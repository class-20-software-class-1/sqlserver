use master
go

create database bbs
go

use bbs
go
create table bbsUsers--用户信息表
(
	UID01 int primary key identity(1,1),
	uName varchar(10) unique not null,
	uSex  varchar(2) check(uSex in('女','男')) not null,
	uAge  int check(uAge>=15 and uAge<=60) not null,
	uPoint int check(uPoint>=0) not null
)
insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
alter table bbsUsers add telephone varchar(20) check(len(telephone)=11)
update bbsUsers set telephone='12345678911' where uName='小雨点'
update bbsUsers set telephone='12345678912' where uName='七年级生'
alter table bbsUsers add constraint UK_bbsUsers_telephone unique(telephone)
alter table bbsUsers add constraint CK_bbsUsers_telephone check(len(telephone)=11)
alter table bbsUsers drop constraint CK__bbsUsers__uPoint__1367E606
update bbsUsers set uName='小雪' where uName='小雨点'

create table bbsUsers2--用户信息表
(
	UID01 int primary key identity(1,1),
	uName varchar(10) unique not null,
	uSex  varchar(2) check(uSex in('女','男')) not null,
	uAge  int check(uAge>=15 and uAge<=60) not null,
	uPoint int check(uPoint>=0) not null,
	telephone varchar(20) check(len(telephone)=11)
)
insert into bbsUsers2 select uName,uSex,uAge,uPoint,telephone from bbsUsers

truncate table bbsUsers2
delete from bbsUsers2

select * from bbsUsers
select * from bbsUsers2



create table bbsSection--版块表


(
	sID01 int primary key identity(1,1),
	sName varchar(10) not null,
	sUid int references bbsUsers(UID01)
)
alter table bbsSection add bbzName varchar(10)
insert into bbsSection(sName,bbzName) values('技术交流','小雨点'),('读书世界','七年级生'),('生活百科','小雨点'),('八卦区','七年级生')
select * from bbsSection

create table bbsTopic--主贴表
(
	tID int primary key identity(1,1),
	tUID int references bbsUsers(UID01),
	tSID int references bbsSection(sID01),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)
alter table bbsTopic add fatieName varchar(10)
alter table bbsTopic add bbzName varchar(10)

insert into bbsTopic values(null,null,'范跑跑!','谁是范跑跑',2008-7-8,1,'逍遥','八卦区'),(null,null,'.NET','与JAVA的区别是什么呀？',2008-9-1,2,'七年级生','技术交流'),(null,null,'今年夏天最流行什么',' 有谁知道今年夏天最流行什么呀?',2008-9-10,1,'小雨点','生活百科')
select * from bbsTopic

create table bbsReply--回帖表
(
	rID int primary key identity(1,1),
	rUID int references bbsUsers(UID01),
	rTID int references bbsTopic(tID),
	rMsg text not null,
	rTime  datetime 
)
alter table bbsReply add huitieName varchar(10) not null
insert into bbsReply values(null,null,'范跑跑',2008-7-8 ,'1'),(null,null,'JAVA',2008-9-1 ,'2'),(null,null,'夏天',2008-9-10 ,'3')
alter table bbsReply alter column rMsg varchar(200) 

select * from bbsReply

create table bbsPoint
(
	UID01 int primary key identity(1,1),
	uName varchar(10) unique not null,
	uPoint  int check(uPoint>=0) not null
)
insert into bbsPoint select uName,uPoint from bbsUsers




select * from bbsUsers
delete from bbsUsers where uName='逍遥'
update bbsUsers set uPoint=10 where uName='小雨点'

select * from bbsSection
delete from bbsSection where sName='生活百科'

select * from bbsReply
truncate table bbsReply



--1.在主贴表中统计每个版块的发帖总数
select bbzName 版块,count(tID)发帖总数 from bbsTopic group by bbzName

--2.在回帖表中统计每个主贴的回帖总数量
select  huitieName,count(rID)回帖总数量 from bbsReply group by huitieName

--3.在主贴表中统计每个用户的发的主帖的总数
select fatieName,count(tID)总数 from bbsTopic group by fatieName

--4.在主贴表中统计每个用户发的主贴的回复数量总和
select fatieName,sum(tCount)回复总数 from bbsTopic group by fatieName 

--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量
select bbzName,avg(tCount)平均回复数量 from bbsTopic group by bbzName,tCount having avg(tCount)>3

--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
select uName,uSex,uAge,uPoint from bbsUsers where uPoint=(select top 1 uPoint from bbsUsers order by uPoint desc)

--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select * from bbsTopic where tTitle like '%快乐%'

--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select * from bbsUsers where uAge>=15 and uAge<=20 and uPoint>10





