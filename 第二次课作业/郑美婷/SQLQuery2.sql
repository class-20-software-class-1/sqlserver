﻿use master
go

if exists(select * from sys.databases where name='Students')
drop database Students

create database Students
on
(
name='Students',
filename='D:\SQLcunchu\Students.mdf',
size=5MB,
maxsize=100MB,
filegrowth=10MB
)
log on
(
name='Students_log',
filename='D:\SQLcunchu\Students.ldf',
size=5MB,
maxsize=100MB,
filegrowth=10MB
)
go
use Students
go
create table StuInfo
(
StuID int primary key identity(1,1),
StuNum char(10) not null,
Stuname nvarchar(20) not null,
StuSex char(2)  check(StuSex='男' or StuSex='女') default('男') ,
StuPhone int,
)
create table ClassInfo
(
ClassID int primary key identity(1,1),
ClassNum char(15) not null,
ClassName nvarchar(30) not null,
ClassRemark text,
StuID int
)