create database Rent
on
(
	name='Rent',
	filename='D:\test\Rent.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on 
(
	name='Rent_log',
	filename='D:\test\Rent_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=10%
)

use Rent
go

create table TblUser
(
	UserId int primary key identity(1,1),
	UserName varchar(20),
	UserTel int not null
)

create table HouseType
(
	TypeId int primary key identity(1,1),
	TypName varchar(20)
)

create table TblQx
(
	QxId int primary key identity(1,1),
	QxName varchar(20)
)

create table HouseInfo
(
	 Id int primary key identity(1,1),
	 HouseDesc text,
	 UserId int references TblUser(UserId),
	 ZJ int not null ,
	 Shi int ,
	 Ting int ,
	 TypeId int references HouseType(TypeId),
	 QxId int references TblQx(QxId),
)