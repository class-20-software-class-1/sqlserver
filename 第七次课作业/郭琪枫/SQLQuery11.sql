use master
go
create database Bank01
on
(
 name='Bank01',
 filename='D:\Bank01.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=15%
)
log on
(
 name='Bank01_log',
 filename='D:\Bank01_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=15%
)

go
use Bank01
go

create table userInfo
(
	customerID int primary key identity(1,1),
	customerName nvarchar(10) not null,
	PID  varchar(20) check(len(PID)=15 or len(PID)=18) unique not null ,
	telephone varchar(20) check(len(telephone)=13 and telephone like'____-________') not null, 
	address nvarchar(200)
)

create table cardInfo
(
	cardID varchar(20) primary key check(cardID like'1010 3576 ____ ___' ) not null,
	curType varchar(20) default('RMB') not null,
	savingType varchar(20) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
	openDate datetime default( getdate())not null ,
	balance int check(balance>=1)not null ,
	pass char(6) default(888888)not null ,
	IsReportLoss varchar(20) default('否') check(IsReportLoss='是' or IsReportLoss='否') not null ,  
	customerID  int references userInfo(customerID)
)

create table transInfo
(
	transId int primary key identity(1,1) not null,
	transDate datetime default(getdate()) not null ,
	cardID  varchar(20) references cardInfo(cardID) not null,
	transType varchar(20) check(transType='存入' or transType='支取') not null,
	transMoney int check(transMoney>0) not null,
	remark text
)

select * from userInfo
select * from cardInfo
--C. 根据下列条件插入和更新测试数据
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：101035761234567

--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：101035761212117

--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：101035761212113

insert into userInfo values('孙悟空',123456789012345,'0716-78989783','北京海淀'),
('沙和尚',421345678912345678,'0478-44223333',''),
('唐僧',321245678912345678,'0478-44443333','')

--update cardInfo set cardID='1010 3576 1234 567',savingType='活期',balance=1000 where customerID=1

insert into cardInfo values('1010 3576 1234 567','','活期',getdate(),1000,'','否',1),
('1010 3576 1212 117','','定期',getdate(),1,'','否',2),
('1010 3576 1212 113','','定期',getdate(),1,'','否',3)

--第二阶段：增、删、改、查

--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)

update cardInfo set pass=611234 where customerID=1

insert into transInfo values('','1010 3576 1234 567','支取',200,'')

update cardInfo set balance=800 where cardID='1010 3576 1234 567'


--3.用同上题一样的方法实现沙和尚存钱的操作(存300)

--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”

--5.查询出2021-03-09到2021-03-19开户的银行卡的信息

insert into transInfo values('','1010 3576 1212 117','存入',300,'')

update cardInfo set balance=301 where cardID='1010 3576 1212 117'

update cardInfo set IsReportLoss='是' where cardID='1010 3576 1234 567'

select * from cardInfo where openDate between '2021-03-09' and '2021-03-25' 
