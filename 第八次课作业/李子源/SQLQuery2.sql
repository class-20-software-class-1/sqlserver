create database Student
on
(
	name='Stuinfo',
	filename='D:\Demo\Stuinfo.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
log on
(
	name='Stuinfo_log',
	filename='D:\Demo\Stuinfo_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
go
use Student


create table Stuinfo
(	
	stuNO varchar(5) unique not null,
	stuName nvarchar(20),
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int identity(1,1) unique not null,
	stuSex nchar(1) check(stuSex='男' or stuSex='女')
)
go

create table Scoreinfo
(
	examNO int primary key identity(1,1),
	stuNO varchar(5) references Stuinfo(stuNO),
	writtenExan int,
	labExam int,
)

go	



insert into Stuinfo values ('s2501','张秋利','20','美国硅谷','女'),('s2502','李斯文',18,'湖北武汉','男'),('s2503','马文才',22,'湖南长沙','女'),
('s2504','欧阳俊雄',21,'湖北武汉','男'),('s2505','梅超风',20,'湖北武汉','女'),('s2506','陈旋风',19,'美国硅谷','女'),('s2507','陈风',20,'美国硅谷','男')

insert into Scoreinfo values('s2501',50,70),('s2502',60,65),('s2503',86,85),('s2504',40,80),('s2505',70,90),('s2506',85,90)


--25.查询每个地方的学生的平均年龄
select stuAddress as 地址,avg(stuAge) as 平均年龄 from Stuinfo group by stuAddress

--26.查询男女生的分别的年龄总和
select stuSex,sum(stuAge) from Stuinfo group by stuSex

--27.查询每个地方的男女生的平均年龄和年龄的总和
select stuAddress as 地址,sum(stuAge)as 年龄总和,avg(stuAge) as 平均年龄 from Stuinfo group by stuAddress

