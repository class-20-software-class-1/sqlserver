create table bbsUsers(
 tid int primary key identity,
 uName varchar(10) not null,
 uSex  varchar(2) not null,
 uAge  int not null,
 uPoint  int  not null,
)
alter table bbsUsers
      add constraint uq_tid unique(uName)
alter table bbsUsers
	  add constraint ck_uSex check(uSex='男' or uSex='女')
alter table bbsUsers
	  add constraint ck_uAge check(uAge>15 or uAge<60)
alter table bbsUsers
	  add constraint ck_uPoint check(uPoint>=0)
	  
create table bbsTopic(
tID  int primary key identity,
tUID  int references bbsUsers(tid),
tSID  int ,
tTitle  varchar(100) not null,
 tMsg  text not null,
 tTime  datetime,
 tCount  int,

)
create table bbsReply(
rID  int primary key identity,
 rUID  int references bbsUsers(tid),
 rTID  int references bbsTopic(tID),
 rMsg  text not null,
 rTime  datetime ,

)
create table bbsSection(
 sID  int ,
  sName  varchar(10) not null,
   sUid   int ,
)

 
alter table bbsSection
add constraint fk_suid 
foreign key(sUid)  references bbsUsers(tid)
DROP TABLE bbsSection
create table bbsSection(
 sID  int  not null,
  sName  varchar(10) not null,
   sUid   int ,
   )
alter table bbsSection
add constraint fk_sID primary key(sID)
insert bbsUsers(uName,uSex,uAge,uPoint) 
select    '小雨点' , '女' , 20 , 0 union
select	 '逍遥'   , '男' , 18  ,4	union
select	 '七年级生',  '男',  19,  2	
select * from bbsUsers
select tid 编号,uName 学号,uSex 姓名,uAge 年龄,uPoint 地址 from  bbsUsers
insert bbsSection( sID,sName, sUid ) 
select 1,'技术交流',3 union
select 2,'读书世界',1 union
select 3,'生活百科',3 union
select 4,'八卦区',1
insert bbsTopic( tUID, tSID,  tTitle, tMsg , tTime, tCount  )
 SELECT 2   , 4,  '  范跑跑'  ,         '      谁是范跑跑   '          , 2008-7-8 ,  1 union
 select 3,1,'   .NET ',' 与JAVA的区别是什么呀？', 2008-9-1 ,  2 union
 select 1,3,'    今年夏天最流行什么 ','    有谁知道今年夏天最流行 ',2008-9-10 , 0
 