use master 
go

create database shopping
on
(
	name='shopping',
	filename='D:\Demo\shopping.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='shopping_log',
	filename='D:\Demo\shopping_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use shopping
go

create table  orders
(
	orderId int primary key identity(1,1) ,
	orderDate datetime default(getdate())
)

create table orderItem
(
	ItemiD	int primary key identity(1,1),
	orderId	int references orders(orderId) not null,
	itemType  nvarchar(4) check(itemType='文具' or itemType='日常用品' or itemType='体育用品' ) not null,
	itemName nvarchar(4) not null,
	theNumber int not null,
	theMoney money not null
)

insert into orders values (default),(default),(default),(default)


insert into orderItem (orderId,itemType,itemName,theNumber,theMoney) values (1 ,'文具',' 笔' ,72, 2)
,(1,'文具','尺',10,1)
,(1,'体育用品','篮球',1,56)
,(2,'文具','笔',36,2)
,(2,'文具','固体胶',20,3)
,(2,'日常用品','透明胶',2,1)
,(2,'体育用品','羽毛球',20,3)
,(3,'文具','订书机',20,3)
,(3,'文具','订书针',10,3)
,(3,'文具','裁纸刀',5,5)
,(4,'文具','笔',20,2)
,(4,'文具','信纸',50,1)
,(4,'日常用品','毛巾',4,5)
,(4,'日常用品','透明胶',30,1)
,(4,'体育用品','羽毛球',20,3)



--1.查询所有订单订购的所有物品数量总和
select  '所有物品综合',sum(theNumber) as 订购数量  from orderItem


--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select  orderId as 订单编号,sum(theNumber) as 总订购数量 ,avg(theMoney) as 均价 from orderItem group by orderId having orderId<3 and  avg(theMoney)<10


--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价

select orderId as 订单编号,avg(theMoney) as 均价 ,sum(theNumber) as 总订购数量 from orderItem group by orderId having avg(theMoney)<10 and sum(theNumber)>50 


--4.查询每种类别的产品分别订购了几次，例如：
--					文具      9
--                   体育用品  3
--                    日常用品  3
select count(itemType) as 文具订购次数 from orderItem where itemType='文具'
select count(itemType) as 体育用品订购次数 from orderItem where itemType='体育用品' 
select count(itemType) as 日常用品订购次数 from orderItem where itemType='日常用品'

select itemType as 类别 ,count(itemType) as 日常用品订购次数 from orderItem group by itemType


--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType as 类别,sum(theNumber) as  总订购数量 ,avg(theMoney) as 均价 from orderItem group by itemType having sum(theNumber)>100



--6.查询每种产品的订购次数，订购总数量和订购的平均单价，例如：

--  产品名称   订购次数  总数量   平均单价 
--    笔           3       120       2

select itemName as 产品名称 ,count(itemName) as 订购次数 ,sum(theNumber) as 总数量,avg(theMoney) as 平均单价 from orderItem group by itemName
