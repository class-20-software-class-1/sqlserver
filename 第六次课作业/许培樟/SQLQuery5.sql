create database bbs
on
(
name=bbs,
filename='D:\bbs.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name=bbs_log,
filename='D:\bbs_log.ldf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)

go
use bbs
go

create table bbsUsers
(
UID01 int primary key identity(1,1),
uName varchar(10) not null,
uSex  varchar(2) not null,
uAge  int ,
uPoint  int 
)
go
create table bbsSection
(
sID1  int primary key identity(1,1),
sName  varchar(10) not null,
sUid   int,
)
go
create table bbsTopic
(
tID int primary key identity(1,1),
tUID  int foreign key references bbsUsers(UID01),
tSID  int foreign key references bbsSection(sID1),
tTitle  varchar(100) not null,
tMsg  text not null,
tTime  datetime,
tCount  int,
)
go
create table bbsReply
(
rID  int primary key identity(1,1),
rUID  int foreign key references bbsUsers(UID01),
rTID  int foreign key references bbsTopic(tID),
rMsg  text not null,
rTime  datetime,
)
go
alter table bbsSection add constraint Fk_bbsSection_sUid foreign key references bbsUsers(UID01)
alter table bbsUsers add constraint Uk_bbsUsers_uSex  check(uSex='男' or uSex='女')
alter table bbsUsers add constraint Uk_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint Uk_bbsUsers_uPoint check(uPoint>=0)
go
insert into bbsUsers(uName,uSex,uAge,uPoint) values( '小雨点','女',20 ,0),( '逍遥','男',18,4),('七年级生','男',19,2)

select uName,uPoint into bbsPoint from bbsUsers

insert into bbsSection(sName,sUid) values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

insert into bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount) 
values ('2','4','范跑跑','谁是范跑跑','2008-7-8','1'),
('3','1','.NET','谁是范跑跑','2008-9-1','2'),
('1','3','.今年夏天最流行什么呀?','有谁知道今年夏天最流行','2008-9-10','0')

insert into bbsReply (rUID,rMsg,rTime)
 values ('1','摩西摩西','2021-3-4'),
 ('2','摩西摩西','2021-4-2'),
 ('3','摩西摩西','2021-1-4')

select * from bbsUsers

delete from bbsUsers where uName='逍遥'

delete from bbsUsers where UID01=2

update bbsPoint set uPoint=(uPoint + 10) where uName='小雨点'

delete from bbsReply where rTID=3
delete from bbsTopic where tSID=4
delete from bbsSection where sID1=4



truncate table bbsReply