use master 
go 
create database dingdan
on
(
name='dingdan',
filename='D:\test\dingdan.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='dingdan_log',
filename='D:\test\dingdan_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use dingdan
go
create table orders
(
orderId int primary key identity,
orderDate datetime
)
create table orderItem
(
ItemiD int primary key identity,
orderId int references orders(orderId),
itemType nvarchar(4) ,
itemName nvarchar(3) ,
theNumber int,
theMoney money

)
insert into orders values('2008-01-12 00:00:00.000'),('2008-01-10 00:00:00.000'),('2008-01-15 00:00:00.000'),('2008-01-10 00:00:00.000')
insert into orderItem values(1,'文具','笔',72,2),(1,'文具','尺',10,1),(1,'体育用品','篮球',1,56),(2,'笔','固体胶',36,2),(2,'文具','固体胶',20,3),(2,'文具','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),(3,'文具','订书机',20,3),(3,'文具','订书针',10,3),(3,'文具','裁纸刀',5,5),(4,'文具','笔',20,2),(4,'文具','信纸',50,1),(4,'日常用品','订毛巾',4,5),
(4,'日常用品','透明胶',30,1),(4,'体育用品','羽毛球',20,3)
select SUM(theNumber)  from dbo.orderItem
select orderId 订单编号 ,SUM(theNumber) 物品的数量和,AVG(theMoney)平均单价 from orderItem where orderId<3 group by orderId   HAVING AVG(theMoney)<10
select orderId 订单编号 ,SUM(theNumber) 物品的数量和,AVG(theMoney)平均单价 from orderItem group by orderId  HAVING SUM(theNumber)>50 and AVG(theMoney)<10
 select   itemType 产品类别, sum(theNumber) 订购数量 from orderItem group by itemType
 select itemType 产品类别,sum(theNumber) 订购总数量,AVG(theMoney)平均单价 from orderItem group by itemType HAVING sum(theNumber) >100
 select itemName 产品名称, count(theNumber) 每种产品的订购次数, sum(theNumber) 订购总数量 ,AVG(theMoney) 平均单价  from orderItem group by itemName