use master
go
if exists(select*from sys.databases where name='Dwu')
drop database Dwu
create database Dwu
on
(
name='Dwu',
filename='D:\SQLcunchu\Dwu.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
log on
(
name='Dwu_log',
filename='D:\SQLcunchu\Dwu_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10mb
)
go
use Dwu
go
create table stuinfo
(
stuNo char(5) primary key not null,
stuName nvarchar(20) not null,
stuAge char(3) not null,
stuAddress nvarchar(200), 
stuSeat int identity(1,1)not null,
stuSex char(1) check(stuSex in (1,0)) default(1) not null
)

insert into stuinfo(stuNo,stuName,stuAge,stuAddress,stuSex) values
('s2501','������',20,'�������',1),
('s2502','��˹��',18,'�����人',0),                 
('s2503','���Ĳ�',22,'���ϳ�ɳ',1),
('s2504','ŷ������',21,'�����人',0),
('s2505','÷����',20,'�����人',1),
('s2506','������',19,'�������',1),
('s2507','�·�',20,'�������',0) 
create table stuexam
(
examNo int primary key identity(1,1),
stuNo char(5) references stuinfo(stuNo) not null,
writtenExam int not null,
labExam int not null
) 
insert stuexam(stuNo,writtenExam,labExam)values
('s2501',50,70),
('s2502',60,65),
('s2503',86,85),
('s2504',40,80),
('s2505',70,90),
('s2506',85,90)
select*from stuinfo
select ѧ��=stuNo ,����=stuName ,����=stuAge ,��ַ=stuAddress ,��λ��=stuSeat ,�Ա�=stuSex from stuinfo
select stuName,stuAge,stuAddress  from stuinfo
select stuNo,writtenExam,labExam  from stuexam
select stuNo ѧ��,writtenExam ����,labexam ���� from stuexam
select stuNo as ѧ��,writtenExam as ����,labexam as ���� from stuexam
select ѧ��=stuNo, ����=writtenExam, ����=labExam from stuexam
select stuNo ѧ��,stuName ����, stuAddress ��ַ ,stuName+'@'+stuAddress ���� from stuinfo
select stuNo ѧ��,writtenExam ����,labexam ����,labexam+writtenExam �ܷ�  from stuexam
select distinct stuAddress from stuinfo
select distinct stuAge as �������� from stuinfo
select top 3 * from stuinfo
select top 4 stuName,stuSeat from stuinfo
select top 50 percent * from stuinfo
select * from stuinfo where stuAddress='�����人' and stuAge=20
select * from stuexam where labexam>60 and labexam<80 order by labexam desc
select * from stuinfo where stuAddress='�����人' OR stuAddress='���ϳ�ɳ'
select * from stuinfo where (stuAddress in('�����人','���ϳ�ɳ'))
select * from stuexam where writtenExam like '[^7,8,9]%' order by writtenExam asc
select * from stuinfo where stuAge is null or stuAge=''
select * from stuinfo where stuAge like '%'
select * from stuinfo where stuName like '��%'
select * from stuinfo where stuAddress like '��%'
select * from stuinfo where stuName like '��_'
select * from stuinfo where stuName like '__��%'
select * from stuinfo  order by stuAge desc
select * from stuinfo  order by stuAge desc,stuSeat asc
select top 1 * from stuexam order by writtenExam desc
select top 1 * from stuexam order by writtenExam 