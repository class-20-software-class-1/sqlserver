create database Student
on
(
	name='Student',
	filename='D:\Student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='D:\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student
go

create table Class
(
	Classid int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)

insert into Class (ClassName) values ('1��'),('2��'),('3��'),('4��'),('5��')
,('6��'),('7��'),('8��'),('9��'),('10��')

update Class set ClassName='1��' where Classid=1

delete from Class where Classid=10

select *from Class



create table Student
(
	StuId int primary key identity(1,1),
	Classid int references Class(Classid),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('��') check(StuSex='��' or StuSex='Ů'),
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200),
)
insert into Student(Classid,StuName,StuSex,StuPhone) values ('1','��','��',1),
('2','��','Ů',2),
('3','��','��',3),
('4','��','Ů',4),
('5','�ż�','��',5),
('6','����','Ů',6),
('7','�Ա�','��',7),
('8','�¶�','Ů',8),
('9','С��','��',9),
('10','С��','Ů',10),
('11','С��','��',11),
('12','С��','Ů',12),
('13','���','��',13),
('14','����','Ů',14),
('15','���','��',15),
('16','��','Ů',16),
('17','¬��ΰ','��',17),
('18','��˾��','��',18),
('19','��ĳ','��',19),
('20','��Ħ','��',20)

alter table Student add CreateDate datetime default(getdate())

update Student set ClassID=getdate()

delete from Student where Classid='2'

select *from Student


create table Course
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default('1') check(CourseCredit>1 and CourseCredit<5) not null,
	CourseType nvarchar(10)
)	

insert into Course(CourseName,CourseName) values ('����','��ѧ'),
('Ӣ��','��ѧ'),
('רҵ��','����'),
('������','רҵ��'),
('��ѧ','ְ�ؿ�'),
('רҵ��','Ӣ��')

update Course set CourseCredit=2 where CourseName='��ѧ'


select *from Course



create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int references Student(StuId),
	CourseId int references Course(CourseId),
	Score decimal(5,2) unique not null

)

insert into Score(Score,CourseId,StuId) values (99,6,13),
(89,8,14),(70,7,3),(96,6,4),(86,11,5),(89,4,6),(83,7,7),
(69,9,8),(47,8,9),(80,7,10),(88,10,11),(78,8,12)

delete from Score where StuId='1'
delete from Score where CourseId='1'

alter table Score add constraint CK_Score_Score check(Score>0 and Score<100)
alter table Score add constraint DK_Score_Score default('0') for Score

select *from Score
