use master
go

create database ATM
on
(
	name=ATM,
	filename='D:\bank\ATM.mdf',
	size=10MB,
	maxsize=50mb,
	filegrowth=15%
)
log on
(
	name=ATM_log,
	filename='D:\bank\ATM_log.ldf',
	size=10MB,
	maxsize=50mb,
	filegrowth=15%
)
go

use ATM
go

create table userInfo
(
	customerID int primary key identity,
	customerName varchar(10) not null,
	PID varchar(20) not null unique check(len(PID)=18 or  len(PID)=15),
	telephone int not null check(telephone=13 and telephone='____-________'),
	addresss varchar(20)
)

create table cardInfo
(
	cardID varchar(20) not null primary key check(cardID='1010 3576 ____ ___'),
	curType varchar(10) not null default('RMB'),
	savingType varchar(10) check(savingType='活期' and savingType='定活两便' and savingType='定期'),
	openDate datetime not null,
	balance int not null check(balance>1),
	pass int not null check(pass=6) default('888888'),
	IsReportLoss varchar(10) not null check(IsReportLoss='是' and IsReportLoss='否') default('否'),
	customerID int not null references userInfo(customerID)
)

create table transInfo
(
	transId int primary key identity,
	transDate datetime not null,
	cardID varchar(20) not null references cardInfo(cardID),
	transType varchar(10) not null check(transType='存入' and transType='支取'),
	transMoey int not null check(transMoey>0),
	remark varchar(20)
)


insert into userInfo values('孙悟空','123456789012345','0716-78989783','北京海淀')
select * from userInfo
insert into cardInfo values('1010 3576 1234 567','RMB','活期',getdate(),'1000',default,default)
select * from cardInfo
insert into userInfo values('沙和尚','421345678912345678','0478-44223333')
select * from userInfo
insert into cardInfo values('1010 3576 1212 117','RMB','定期',getdate(),'1',default,default)
select * from cardInfo
insert into userInfo values('唐僧','321245678912345678','0478-44443333')
select * from userInfo
insert into cardInfo values('1010 3576 1212 113','RMB',getdate(),'1',default,default)
select * from cardInfo
update cardInfo set pass=611234 where cardID='1010 3576 1234 567'

insert into transInfo values(1,getdate(),'1010 3576 1234 567','支出','200',null)

insert into transInfo values(2,getdate(),'1010 3576 1212 117','存入','300',null)

update cardInfo set IsReportLoss='是' where cardID='1010 3576 1212 113'

select * from cardInfo where openDate like '2021-03-[0-1]9'