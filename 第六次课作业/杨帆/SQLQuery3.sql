create database School
on
(
	name='School',
	filename='D:\test\School.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='School_log',
	filename='D:\test\School_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=10%
)

use School
go

create table ClassInfo
(
	ClassId int primary key identity(1,1),
	ClassName varchar(20) not null unique ,
	ClassTime date ,
	ClassDescribe text
)

create table StudentInfo
(
	StudentId int primary key identity(1,1),
	StudentName varchar(10) unique check(len(StudentName)>2),
	StudentSex varchar(10) default('��') check(StudentSex in('��','Ů')),
	StudentAge int not null check(StudentAge>=15 and StudentAge<=40),
	StudentAddress varchar(20) default('�����人'),
	ClassId int references ClassInfo(ClassId)
)

create table CourseInfo
(
	CourseId int primary key identity(1,1),
	CourseName varchar(10) not null unique,
	CourseDescribe text
)

create table PerformanceInfo
(
	PreformanceId int primary key identity(1,1),
	StudentId int references StudentInfo(StudentId) not null,
	CourseId int references CourseInfo(CourseId),
	Preformance int check(Preformance>=0 and Preformance<=100)
)