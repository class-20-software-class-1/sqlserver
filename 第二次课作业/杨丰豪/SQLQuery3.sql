if exists(select * from sysdatabases where name='ClassID')
drop database ClassID
create database ClassID
on
(
name='ClassID',
filename='D:\test\ClassID.mdf',
size=5MB,
maxsize=50MB,
filegrowth=10MB
)
log on
(
name='ClassID_log.ldf',
filename='D:\test\ClassID_log.ldf',
size=5MB,
maxsize=50MB,
filegrowth=10MB
)
go
use ClassID
go
create table ClassID
( 
ClassID int primary key identity(1,1) not null,
ClassNum char(15) not null,
ClassName varchar(30) not null,
ClassRemark text,
StuID char,
)