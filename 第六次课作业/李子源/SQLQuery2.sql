--create database 学生管理系统
--on
--(
--	name='学生管理系统',
--	filename='D:\学生管理系统.mdf',
--	size=20,
--	maxsize=300,
--	filegrowth=50
--)
--log on
--(
--	name='学生管理系统_log',
--	filename='D:\学生管理系统_log.ldf',
--	size=20,
--	maxsize=300,
--	filegrowth=50
--)
--use   学生管理系统
go
create table class(
	classid int primary key identity(1,1),
	classname varchar(10) not null unique ,
	classtime int not null,
	classdescription text
)
create table student (
	studentno int primary key identity(1,1),
	studentname  varchar(10) check(studentname>2) not null,
	studentsex varchar(1) check(studentsex='男'or studentsex='女') default('男') not null,
	studentage int check(studentage>=15 and studentage<=40) not null,
	studentaddress text default('湖北武汉'),
	classid int
)
create table lession(
	lessionId int primary key identity(1,1),
	course varchar(10) not null unique,
	lessionDescription text
)
create table grade(
	gradeid int primary key identity(1,1) not null,
	grade01 int references student(studentno) not null,
	grade02 int references lession(lessionId) not null,
	score int check(score>0 and score<=100)
)