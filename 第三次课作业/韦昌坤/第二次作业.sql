create database Student
on
(
name='Student',
filename='E:\新建文件夹.mdf',
size=5MB,
filegrowth=2MB,
maxsize=100MB
)
log on
(
name='Student-db',
filename='E:\新建文件夹.ldf',
size=5MB,
filegrowth=2MB,
maxsize=20MB
)

create table Class
(
ClassID int primary key identity,
ClassName nvarchar(20) not null unique,
)
create table Students
(
StuId int primary key identity(1,1) not null,
ClassID int foreign key (ClassID) references Class (ClassID) not null,
StuName nvarchar(20) not null,
StuSex nvarchar(1) default('男')check(StuSex='男'or StuSex='女') not null,
StuBrithday smalldatetime,
StuPhone nvarchar(11) unique,
StuAddress nvarchar(200),
)
create table Course
(
CourseID int primary key identity not null,
CourseName nvarchar(50) unique not null,
CourseCredit int not null default('1') check(CourseCredit>=1 and CourseCredit<=5)
)
create table Score
	(
	ScoreID int identity(1,1),
	StuID int not null,
	CourseID int not null,
	Score decimal(5,2) unique not null
	)
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Students(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)
alter table Score add consrraint UK_Score_Score unique(Score)

