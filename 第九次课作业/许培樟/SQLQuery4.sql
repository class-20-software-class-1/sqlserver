--一、先创建数据库和表以及约束

--	1.创建一个数据库用来存放某论坛的用户和发帖信息，数据库的名称为bbs，包含1个数据文件1个日志
--	文件，数据文件和日志文件全部存放在E盘中，初始大小，增长和最大大小自己设定
	use master
	go

	create database bbs
	on
	(
		name='bbs',
		filename='D:\bbs.mdf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	log on
	(
		name='bbs_log',
		filename='D:\bbs_log.ldf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	go

	use bbs
	go

--	2.创建表

--	注意以下4张表的创建顺序，要求在创建bbsUser和bbsSection表时不添加约束，创建完后单独添加约束，其它的表创建时添加约束

--	用户信息表（bbsUsers）
--	用户编号  UID int 主键  标识列
--	用户名    uName varchar(10)  唯一约束 不能为空
--	性别      uSex  varchar(2)  不能为空 只能是男或女
--	年龄      uAge  int  不能为空 范围15-60
--	积分      uPoint  int 不能为空  范围 >= 0
	create table bbsUser
	(
		UID int identity,
		uName varchar(10) not null,
		uSex  varchar(2) not null,
		uAge  int not null,
		uPoint  int not null
	)

	alter table bbsUser add constraint PK_bbsUser_UID primary key(UID)
	alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex in('男','女'))
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge between 15 and 60)
	alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)


--+	主贴表（bbsTopic）
--	主贴编号  tID  int 主键  标识列，
--	发帖人编号  tUID  int 外键  引用用户信息表的用户编号
--	版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
--	贴子的标题  tTitle  varchar(100) 不能为空
--	帖子的内容  tMsg  text  不能为空
--	发帖时间    tTime  datetime  
--	回复数量    tCount  int
	create table bbsTopic
	(
		tID  int primary key identity,
		tUID  int references bbsUser(UID),
		tSID  int references bbsSection(sID),
		tTitle  varchar(100) not null,
		tMsg  text not null,
		tTime  datetime,
		tCount  int
	)


--+	回帖表（bbsReply）
--	回贴编号  rID  int 主键  标识列，
--	回帖人编号  rUID  int 外键  引用用户信息表的用户编号
--	对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
--	回帖的内容  rMsg  text  不能为空
--	回帖时间    rTime  datetime 
	create table bbsReply
	(
		rID  int primary key identity,
		rUID  int references bbsUser(UID),
		rTID  int references bbsTopic(tID),
		rMsg  text not null,
		rTime  datetime 
	)
	

--	版块表（bbsSection）
--	版块编号  sID  int 标识列 主键
--	版块名称  sName  varchar(10)  不能为空
--	版主编号  sUid   int 外键  引用用户信息表的用户编号
	create table bbsSection
	(
		sID  int identity,
		sName  varchar(10) not null,
		sUid   int 
	)
	alter table bbsSection add constraint PK_bbsSection_sID primary key(sID) 
	alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(UID)

--二、在上面的数据库、表的基础上完成下列题目：

--	1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--	  小雨点  女  20  0
--	  逍遥    男  18  4
--	  七年级生  男  19  2
	insert into bbsUser values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
	select * from bbsUser

--	2.将bbsUser表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
	select uName,uPoint into bbsPoint from bbsUser --不要先创建好表，查询数据的时候直接生成表
	insert into bbsPoint select uName,uPoint from bbsUser --必须要先创建好目标表

--	3.给论坛开设4个板块
--	  名称        版主名
--	  技术交流    小雨点
--	  读书世界    七年级生
--	  生活百科    小雨点
--	  八卦区      七年级生
	insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

	select * from bbsSection

--	4.向主贴和回帖表中添加几条记录
	  
--	   主贴：

--	  发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
--	  逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
--	  七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
--	  小雨点   生活百科    今年夏天最流行什么     有谁知道今年夏天最流行  2008-9-10  0
--						      什么呀？
	insert into bbsTopic values (2,5,'范跑跑!','谁是范跑跑','2008-7-8',1)
	insert into bbsTopic values (3,2,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,4,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)

	select * from bbsTopic

	   
--	   回帖：
--	   分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定
	insert into bbsReply values (1,2,'范跑跑是谁我也不知道',GETDATE()),(1,2,'JAVA是什么？？？',GETDATE()),
	(1,2,'范跑跑是谁我也不知道22222',GETDATE())

	select * from bbsReply

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
	delete from bbsReply where rTID=1	
	delete from bbsReply where rUID=2	
	delete from bbsTopic where tUID=2
	delete from bbsUser where UID=2
	
	select * from bbsUser
	select * from bbsReply where rTID=1
	select * from bbsTopic where tUID=2
	select * from bbsUser where UID=2

--	6.因为小雨点发帖较多，将其积分增加10分
	update bbsUser set uPoint= uPoint+10+10 where UID=1

--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
	

--	8.因回帖积累太多，现需要将所有的回帖删除

delete from bbsReply --删除了整个表的数据，表还在，没有释放空间，自增继续。
truncate table bbsReply --删除整个表的数据，表还在，释放了空间，自增从0开始

drop table bbsReply --删除整个表，表都没了


--1.在主贴表中统计每个版块的发帖总数
select  tSID   ,count(  tID )发帖总数 from bbsTopic group by  tSID  

--2.在回帖表中统计每个主贴的回帖总数量
select  rTID ,count(rID)回帖总数量 from bbsReply group by rTID
select * from bbsReply
--3.在主贴表中统计每个用户的发的主帖的总数
select tUID, count( tID )主帖的总数 from bbsTopic group by tUID
--4.在主贴表中统计每个用户发的主贴的回复数量总和
select tID, SUM(tCount) 回复数量总和 from bbsTopic group by tID

--5.在主贴表中查询 每个版块的主贴的 平均回复数量大于3的  版块的平均回复数量
 select tSID, AVG(tCount) from bbsTopic group by tSID having AVG(tCount)>3
--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
select top 1 * from bbsUser order by uPoint desc
--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select *from bbsTopic where tTitle like '%快乐%' or tMsg like '%区别%'
--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select * from bbsUser where uAge>=15 and uAge<=20 and uPoint > 10

--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUser where uName like '小%' and uName like '__大%'

--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
select tTitle '标题',TMsg '类容'  from bbsTopic where tTime  >'2008-9-10 12:00:00' and tCount >10

--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖bb人编号和回复数量查询出来
select tUID ,tCount from bbsTopic where tTitle like '%!'

