use master 
go 

create database Student

use Student 

create table Class
(
	ClassId	int constraint KP_Class_ClassId primary key identity(1,1),
	ClassName nvarchar(20) unique not null, 
)

insert into Class (ClassName)
select'软件一班' union
select'软件二班' union
select'软件三班' union
select'软件四班' union
select'软件五班' union
select'软件六班' union
select'软件七班' union
select'软件八班' union
select'软件九班' union
select'软件十班' 

select * from Class

update Class set ClassName='软件十一班' where ClassId=1

delete from Class where ClassId=10

use Student

create table Student
(
	StuId int primary key identity(1,1),
	ClassId int constraint FK_Student_ClassId foreign key references Class(ClassId),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex in ('男','女')),
	StuBirthday date,
	StuPhone nvarchar(11) unique not null,
	StuAddress nvarchar(200),
)

insert into Student(StuName,StuSex,StuPhone,StuAddress)
select'小狗','男','987456','WIN' union
select'小明','男','789465','WIN' union
select'小天','女','789654','WIN' union
select'小镇','女','965758','WIN' union
select'小子','男','589647','WIN' union
select'小红','女','879645','WIN' union
select'小黄','男','879457','WIN' union
select'小七','男','859476','WIN' union
select'小八','男','669584','WIN' union
select'小九','女','775894','WIN' union
select'小十','男','664987','WIN' union
select'小一','女','998866','WIN' union
select'小二','男','998877','WIN' union
select'小三','男','659874','WIN' union
select'小四','女','448877','WIN' union
select'小五','男','996654','WIN' union
select'小刘','女','568945','WIN' union
select'小气','女','458965','WIN' 

select * from Student

alter table Student add ClassId int references Class(ClassId)

alter table Student add CreateDate datetime

delete from Student where ClassId=1


use Student

create table Course
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int not null default('1') check(CourseCredit in ('1','2','3','4','5')),
	CourseCategory nvarchar(10) check(CourseCategory in ('专业课','公共课')),
)

insert into Course(CourseName,CourseCategory)
select'语文','公共课' union
select'数学','公共课' union
select'Java','专业课' union
select'政治','公共课' union
select'英语','公共课' union
select'HTML','专业课'

select * from Course

update  Course set CourseCredit='5' where CourseCategory='专业课'

create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int foreign key references Student(StuId),
	Courseld int foreign key references Course(CourseId),
	Score decimal(5,2) unique not null
)

insert into Score(Score)
select '10.00' union
select '11.10' union
select '12.20' union
select '13.30' union
select '14.40' union
select '15.50' union
select '16.60' union
select '17.70' union
select '18.80' union
select '19.90' union
select '20.00' union
select '21.10' union
select '22.20' union
select '23.30' union
select '24.40' union
select '25.50' union
select '26.60' union
select '27.70' union
select '28.80' union
select '29.90' 

delete from Score where StuId='1'

delete from Score where Courseld='1'

alter table Score add constraint UK_Score_Score default('0') check(Score>=0 and Score<=100)