use master
go
if exists(select*from sys.databases where name='bbs')
drop database bbs
create database bbs
on
(
name='bbs',
filename='D:\bbs.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='bbs_log',
filename='D:\bbs_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use bbs
go
create table bbsUsers
(
UID int identity(1,1) ,
uName varchar(10) not null,
uSex varchar(2) not null,
uAge int not null,
uPoint int not null
)
alter table bbsUsers add constraint PK_bbsUsers_UID primary key(UID)
create table bbsTopic
(
tID int primary key identity(1,1),
tUID int ,
tSID int ,
tTitle varchar(100) not null,
tMsg text not null,
tTime datetime,
tCount int 
)
create table bbsReply
(
rID int primary key identity(1,1),
rUID int references bbsUsers(UID),
rTID int references bbsTopic(tID),
rMsg text not null,
rTime datetime 
)
create table bbsSection
(
sID int identity(1,1),
sName varchar(10) not null,
sUid int 
)
alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName) 
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in ('男','女'))
alter table bbsUSers add constraint CK_bbsUsers_nSex check(uAge<=20 and uAge>=15)
alter table bbsUSers add constraint CK_bbsUSers_uPoint check(uPoint>=0)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsDection_sUid foreign key(sUid)references bbsUsers(UID)
insert into bbsUsers(uName,uSex,uAge,uPoint)values
( '小雨点','女',20, 0),
('逍遥' ,'男',18 ,4),
('七年级生','男',19 ,2)
--select 列名1，列名2 from 表名
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection(sName)values
(' 技术交流'),('读书世界'),('生活百科'),('八卦区')
alter table bbsSection add bName nvarchar(20) not null
insert into bbsSection(bName,sUid) values
('技术交流','小雨点'),
('读书世界','七年级生'),
('生活百科','小雨点'),
('八卦区','七年级生')
insert into bbsTopic(tUID,tSID,tTitle,tMsg, tTime, tCount)values
('逍遥','八卦区 ','范跑跑 ','谁是范跑跑',2008-7-8,1),
('七年级生','技术交流','.NET','与JAVA的区别是什么呀？',2008-9-1,2),
('小雨点','生活百科','今年夏天最流行什么','有谁知道今年夏天最流行什么呀？',2008-9-10,0)
insert into bbsReply(rUID,rTID,rMsg,rTime)values
('殷殷','八卦区',1,'你猜',2008-7-8),
('giaogiao','技术交流',2,'java更头秃',2008-7-8),
('小雨伞','生活百科',3,'BF风',2008-7-8)
delete from bbsReply where rID=1
delete from bbsTopic where tID=1
insert into bbsUsers(UID,uPoint) values(3,10)
delete from bbsUsers where UID=2
delete from bbsSection where sID=2
delete from bbsReply