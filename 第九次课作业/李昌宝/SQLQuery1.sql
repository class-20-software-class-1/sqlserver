create database bbss
on
(
name = 'bbss',
size = 5,
maxsize = 500,
filename = 'D:\新建文件夹1.mdf',
filegrowth = 10%
)
log on
(
name = 'bbss_log',
size = 5,
maxsize = 500,
filename = 'D:\新建文件夹1.ldf',
filegrowth = 10%
)
use bbss
go
create table bbsUsers   --用户信息表
(
UID int identity (1,1),    --用户编号
uName varchar(10) not null ,  --用户名
uSex  varchar(2) not null,  --性别
uAge  int not null,  --年龄
uPoint  int not null,  --积分
)
go
create table bbsSection		--板块表
(
 sID  int identity,		--板块编号
 sName  varchar(10) not null,--板块名称
 sUid   int,  --版主编号
)
go
alter table bbsUsers add constraint PK_UID primary key(UID);
alter table bbsUsers add constraint UQ_uName unique(uName);
alter table bbsUsers add constraint CK_uSex check(uSex = '男' or uSex = '女');
alter table bbsUsers add constraint CK_uAge check(uAge>=15 and uAge<=60);
alter table bbsUsers add constraint CK_uPoint check(uPoint>=0);
alter table bbsSection add constraint CK_sID primary key (sID);
alter table bbsSection add constraint FK_sUid foreign key(sUid) references bbsUsers(UID)
go
create table bbsTopic           --主贴表
(
tID  int primary key identity,		--主贴编号
tUID  int references bbsUsers(UID),--发帖人编号
tSID  int references bbsSection(sID),--板块编号
tTitle  varchar(100) not null,--贴纸标题
tMsg  text not null,--帖子内容
tTime  datetime  ,--发帖时间
tCount  int--回复数量
)
go
create table bbsReply		--回贴表
(
rID  int primary key identity,--回帖编号
 rUID  int references bbsUsers(UID),--回帖人编号
 rTID  int references bbsTopic(tID),--对应主贴编号
 rMsg  text not null,--回帖内容
  rTime  datetime --回帖时间
)

go
insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection (sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2 ,4,'范跑跑','谁是范跑跑 ',2008-7-8,1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(3 ,1,'.NET','与JAVA的区别是什么呀？ ',2008-9-1,2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(1 ,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀 ',2008-9-10,0)
insert into bbsTopic select  tUID,tSID,tTitle,tMsg,tTime,tCount from  bbsTopic
insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,1,'不认识',2008-7-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,2,'没有区别',2008-9-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(2 ,2,'请百度',2008-9-12)
insert into bbsReply select rUID,rTID,rMsg,rTime from  bbsReply
--在论坛数据库中完成以下题目
select * from bbsUsers
select * from bbsTopic
select * from bbsReply
--1.在主贴表中统计每个版块的发帖总数
select tSID,count(tUID) 发帖总数 from bbsTopic group by tSID
--2.在回帖表中统计每个主贴的回帖总数量
select  RTID,count(rUID)回帖总数 from bbsReply group by rTID
--3.在主贴表中统计每个用户的发的主帖的总数
select  tUID,count(tCount)该用户发主帖总数 from bbsTopic GROUP BY tUID
--4.在主贴表中统计每个用户发的主贴的回复数量总和
select tUID,sum(tCount) 发帖主贴数量总数 from bbsTopic group by tUID 
--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量
select tSID,avg(tCount) 平均回复数量 from bbsTopic group by tSID having  avg(tCount)>0
--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
select top 1 uName,uSex,uAge,uPoint from bbsUsers order by uPoint desc
select uName,uSex,uAge,uPoint from bbsUsers where uPoint= (select max(uPoint)最高积分 from bbsUsers)
--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
Select * from bbsTopic where tTitle like '%快%乐%' OR tMsg LIKE '%快%乐%'
--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select * from bbsUsers where uAge between 15 and 20  and uPoint<10
select uName,uSex,uAge,uPoint  from bbsUsers group by uName,uSex,uAge,uPoint having uPoint<10
--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUsers where uName like '小%大'
--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
select tTitle,tMsg from bbsTopic where tTime>'2008-9-10 12:00:00 ' and tCount>10
--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select tUID,tCount from bbsTopic where tTitle like '%!'
