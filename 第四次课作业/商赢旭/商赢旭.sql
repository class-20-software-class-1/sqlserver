create database Student
on
(
	name='Student',
	filename='Z:\Student.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='Student_log',
	filename='Z:\Student_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go

use Student
go

create table Class
(
	Classid int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
)

insert into Class (ClassName) values ('1班'),('2班'),('3班'),('4班'),('5班')
,('6班'),('7班'),('8班'),('9班'),('10班')

update Class set ClassName='1班' where Classid=1

delete from Class where Classid=10

select *from Class



create table Student
(
	StuId int primary key identity(1,1),
	Classid int references Class(Classid),
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check(StuSex='男' or StuSex='女'),
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200),
)
insert into Student(Classid,StuName,StuSex,StuPhone) values ('1','甲','男',1),
('2','甲','女',2),
('3','乙','男',3),
('4','丙','女',4),
('5','张','男',5),
('6','李','女',6),
('7','牢记','男',7),
('8','王磊','女',8),
('9','朱振义','男',9),
('10','梁志超奶奶','女',10),
('11','梁志超','男',11),
('12','马','女',12),
('13','彩虹小马','男',13),
('14','大乙神针','女',14),
('15','伞兵一号','男',15),
('16','马飞飞','女',16),
('17','卢本伟','男',17),
('18','大司马','男',18),
('19','刘毅','男',19),
('20','周某','男',20)

alter table Student add CreateDate datetime default(getdate())

update Student set ClassID=getdate()

delete from Student where Classid='2'

select *from Student


create table Course
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default('1') check(CourseCredit>1 and CourseCredit<5) not null,
	CourseType nvarchar(10)
)	

insert into Course(CourseName,CourseName) values ('保加利亚语','化学'),
('语音','、化学'),
('专业课','哲学'),
('体育课','政治'),
('保加利亚语','下饭'),
('专业课','保加利亚语')

update Course set CourseCredit=2 where CourseName='化学'


select *from Course



create table Score
(
	ScoreId int primary key identity(1,1),
	StuId int references Student(StuId),
	CourseId int references Course(CourseId),
	Score decimal(5,2) unique not null

)

insert into Score(Score,CourseId,StuId) values (99,6,13),
(89,8,14),(70,7,3),(96,6,4),(86,11,5),(89,4,6),(83,7,7),
(69,9,8),(47,8,9),(80,7,10),(88,10,11),(78,8,12)

delete from Score where StuId='1'
delete from Score where CourseId='1'

alter table Score add constraint CK_Score_Score check(Score>0 and Score<100)
alter table Score add constraint DK_Score_Score default('0') for Score

select *from Score