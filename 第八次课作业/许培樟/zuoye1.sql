create database orderform
on
(
 name='orderform',
 filename='D:\orderform.mdf',
 size=10,
 maxsize=100,
 filegrowth=50%
)
log on
(
 name='orderform_log',
 filename='D:\orderform_log.ldf',
 size=10,
 maxsize=100,
 filegrowth=50%
)
go
use orderform
go

create table orders
(
 orderId int primary key identity,
 orderDate datetime not null
)
create table orderItem
(
 ItemiD int primary key identity,
 orderId int not null,
 itemType nvarchar(10),
 itemName nvarchar(10) not null, 
 theNumber int not null,
 theMoney money
)
insert into orders values(2008-01-12),(2008-02-10),(2008-02-15),(2008-03-10)
insert into orderItem values
(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),
(2,'日常用品','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),
(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),
(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)
select * from orderItem
select * from  orders

select SUM(theNumber) from dbo.orderItem
select orderId, SUM(theNumber)数量和, AVG (theMoney) 平均单价  from orderItem group by orderId HAVING AVG (theMoney)<10 and orderID<3
select orderID, SUM(theNumber)数量和, AVG (theMoney) 平均单价  from orderItem where theNumber>50  group by orderId HAVING AVG (theMoney)<10 
select itemType,count(itemType)类别 from dbo.orderItem group by itemType
select itemType, SUM(theNumber)数量和, AVG (theMoney) 平均单价 from orderItem group by itemType HAVING SUM(theNumber)>100
select count(orderId) 定购次数,sum(theNumber) 订单数量,AVG(theMoney) 平均单价 from orderItem group by itemName

