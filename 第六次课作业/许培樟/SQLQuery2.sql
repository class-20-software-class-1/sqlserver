create database company
on
(
 name='company',
 filename='D:\company.mdf',
 size=10,
 maxsize=100,
 filegrowth=10%
)
log on
(
 name='company_log',
 filename='D:\company_log.ldf',
 size=10,
 maxsize=100,
 filegrowth=10%
)

use company
go

create table sectionInfo
(
 sectionID int primary key identity(1,1),
 sectionName  varchar(10) not null
)
create table userInfo
(
 userNo  int primary key identity(1,1) not null,
 userName  varchar(10) unique not null check(len(userName)>=4),
 userSex   varchar(2) not null check(userSex in('��','Ů')),
 userAge int not null, check(userAge >0 and userAge<=100 ),
 userAddress  varchar(50) default('����'),
 userSection  int references sectionInfo(sectionID)
)
create table workInfo
(
 workId int primary key identity(1,1) not null,
 userId  int references userInfo(userNo),
 workTime datetime  not null,
 workDescription  varchar(40) not null check( workDescription in('�ٵ�','����','����','����','�¼�'))
)