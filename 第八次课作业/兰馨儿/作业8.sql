	use master
	go

	create database DingDan
	on
	(
	name = 'DingDan' ,
	filename = 'D:\DingDan.mdf',
	size = 5MB,
	MAXsize = 50MB,
	filegrowth = 10%
	)
	log	on
	(
	name = 'DingDan_log' ,
	filename = 'D:\DingDan_log.ldf',
	size = 5MB,
	MAXsize = 50MB,
	filegrowth = 10%
	)
	go

	use DingDan
	go

	create table OrdersIofo
	(
	orderId int primary key identity(1,1),
	orderDate datetime
	)

	create table OrderItemInfo
	(
	ItemiD int primary key identity(1,1),
	orderId  int references  OrdersIofo(orderId),
	itemType  varchar(10) not null,
	itemName varchar(10)not null,
	theNumber int,
	theMoney money
	)

	insert into OrdersIofo values('2008-01-12'),
							 ('2008-02-10'),
							 ('2008-02-15'),
							 ('2008-03-10')
	select * from OrdersIofo

	insert into OrderItemInfo values(1,'文具','笔',72,2),
									(1,'文具','尺',10,1),
									(1,'体育用品','篮球',1,56),
									(2,'文具','笔',36,2),
									(2,'文具','固体胶',20,3),
									(2,'日常用品','透明胶',2,1),
									(2,'体育用品','羽毛球',20,3),
									(3,'文具','订书机',20,3),
									(3,'文具','订书机',10,3),
									(3,'文具','裁纸刀',5,5),
									(4,'文具','笔',20,2),
									(4,'文具','信纸',50,1),
									(4,'日常用品','毛巾',4,5),
									(4,'日常用品','透明胶',30,1),
									(4,'体育用品','羽毛球',20,3)
	select * from OrderItemInfo

	--1.查询所有订单订购的所有物品数量总和
	select SUM(theNumber) from dbo.OrderItemInfo
	
	--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
	select avg(theMoney)平均单价,sum(theNumber)物品总数   from OrderItemInfo  group by orderId having avg(theMoney)<10 and orderId<3
	
	--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
	select avg(theMoney)平均单价,sum(theNumber)物品总数   from OrderItemInfo  group by orderId having avg(theMoney)<10 and sum(theNumber)>50

	--4.查询每种类别的产品分别订购了几次，例如：
	--文具 9 , 体育用品  3,日常用品 3
	select itemType  产品类别,count(itemType)订购次数 from dbo.OrderItemInfo  group by itemType

	--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
	select itemType  产品类别,sum(theNumber)订购总数 from dbo.OrderItemInfo  group by itemType

	--6.查询每种产品的订购次数，订购总数量和订购的平均单价，例如：

	-- 产品名称   订购次数  总数量   平均单价 
    --笔           3       120       2
	select itemName 产品名称 ,count(itemName)订购次数,sum(theNumber)总数量,avg(theMoney)平均单价 from dbo.OrderItemInfo group by  itemName 
