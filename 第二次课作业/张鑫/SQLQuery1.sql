if exists(select * from sys.databases where name='Students')
 drop database Students
create database Students
on
(
 name='Students',
 filename='D:\SQL作业\Lesson1.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Students_log',
 filename='D:\SQL作业\Lesson1_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use Students
go
create table StuInfo
(
 StuID int primary key identity(1,1),
 StuNum nvarchar(10) not null,
 StuName nvarchar(20) not null,
 StuSex char(2) default('男') check(StuSex='男' or StuSex='女'), 
 StuPhone nvarchar(11) 
 )
use Students
go
create table ClassInfo
(
 ClassID int primary key identity(1,1),
 ClassNum nvarchar(15) not null,
 ClassName nvarchar(30) not null,
 ClassRemark ntext
 foreign key (ClassID) references StuInfo (StuID)
)