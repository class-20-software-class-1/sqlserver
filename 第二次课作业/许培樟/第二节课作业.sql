create database Homework
on
(
 name='Homework',
 filename='D:\Homework.mdf',
 maxsize=50MB,
 filegrowth=10MB
)
log on
(
 name='Homework_log.kdf',
 filename='D:\Homework_log.mdf',
 size=5MB,
 filegrowth=10MB
)
go 
use Homework
go
create table studented
(
 StuID int primary key identity(1,1) not null,
 StuNum char(10) not null,
 StuName nvarchar(20) not null,
 StuSex char(1) not null,
 StuPhone varchar(20)
)
create table classmate
(classID int primary key identity(1,1),
classnum char(10) not null,
classname nchar(20) not null,
classRemark  text not null,
stuID int not null,
)