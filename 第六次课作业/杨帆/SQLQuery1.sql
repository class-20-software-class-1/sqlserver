use master
go

create database TestDB
on 
(
	name='TestDB.mdf',
	filename='D:\test\TestDB.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='TestDB_log.mdf',
	filename='D:\test\TestDB_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
use TestDB
go

create table TypeInfo
(
	Typeld int primary key identity(1,1),
	TypeName varchar(10) not null
)

create table LoginInfo 
(
	Loginld int primary key identity(1,1),
	LoginName nvarchar(20) not null unique,
	LoginPwd varchar(20) not null default(123456),
	Sex varchar(10),
	Birthday date,
	VipType varchar(10)
)