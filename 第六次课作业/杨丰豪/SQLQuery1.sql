create database TestDB
on
(
name=TestDB,
filename='D:\test\TestDB.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name=TestDB_log,
filename='D:\test\TestDB_log.ldf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)

go
use TestDB
go

create table typeInfo
(
typeId int primary key identity(1,1),
typeName varchar(10) not null
)

go
create table loginInfo
(
LoginId int primary key identity(1,1),
LoginName varchar(10) unique not null,
LoginPwd varchar(20) default'123456' not null,
Sexi nchar(1) check(Sexi='��'OR Sexi='Ů'),
Birthday datetime,
Menber varchar(10),
) 