use master

create database company
on
(
	name='company_mdf',
	filename='D:\sql\company.mdf',
	size=5,
	maxsize=20,
	filegrowth=10%
)
log on
(
	name='company_ldf',
	filename='D:\sql\company.ldf',
	size=4,
	maxsize=10,
	filegrowth=10%
)
go
use company
go

create table sectionInfo
(
	sectionID int primary key identity(1,1),
	sectionName varchar(10) not null
)

create table userInfo
(
	userNo int primary key identity(1,1),
	userName nvarchar(10) unique not null  check( len(userName)>4),
	userSex char(2) unique check( userSex in ('��','Ů')),
	userAge int not null check (userAge>0 and userAge<101),
	userAddress  varchar(50) default('�����人'),
	userSection int foreign key references sectionInfo(sectionID) 
)
create table workInfo
(
	workId int primary key identity(1,1) not null,
	userId  int foreign key references userInfo(userNo),
	workTime datetime not null,
	workDescription  varchar(40) check(workDescription in('�ٵ�','����','����','����','�¼�') )
)



