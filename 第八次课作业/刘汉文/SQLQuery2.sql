create database sell
on
(
name='sell_data',
 filename='D:\新建文件夹(2).mdf',
 size=5mb,
 filegrowth=10%
)
go
use sell
go
create table orders
(
orderId int primary key ,
orderDate datetime 
)
go
create table orderItem
(
ItemiD int,
orderId int,
itemType varchar(20),
itemName char(20),
theNumber int,
theMoney int,
)
go
insert into orders(orderId ,orderDate) values(1,'2008-01-12'),(2,'2008-02-10'),(3,'2008-02-15'),(4,'2008-03-10')
go
insert into orderItem(ItemiD,orderId,itemType,itemName,theNumber,theMoney  ) values
(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),
(2,'日常用品','透明胶',2,1),
(2,'文具','羽毛球',20,3),
(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),
(4,'文具','信纸',50,1),
(4,'文具','笔',4,5),
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3),

select SUM(theNumber) 数量总和 from orderItem

select orderId,SUM(theNumber),AVG(theMoney) from orderItem group by orderId 
having(AVG(theMoney)<10 and orderId <3)

select orderId,SUM(theNumber),AVG(theMoney) from orderItem group by orderId 
having(AVG(theMoney)<10 and SUM(theNumber)>50)

select itemType,count(theNumber) from orderItem group by itemType

select itemType,SUM(theNumber),AVG(theMoney) from orderItem group by itemType
having(SUM(theNumber)>100 and AVG(theMoney)>100)

select itemName 产品名称,count(orderId) 订购次数,SUM(theNumber) 总数,AVG(theMoney) 平均单价 from orderItem group by itemName