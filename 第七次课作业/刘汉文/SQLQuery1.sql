use master 
go
create database bank
on
(
	name='bank',
	filename= 'D:\bank.mdf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
log on
(
	name='bank_log',
	filename= 'D:\bank_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
go
use bank
go
create table userInfo 
(
	customerID int primary key identity,
	customerName varchar(10) not null,
	PID  varchar(18) not null unique check(len(PID)=15 or len(PID)=18),
	telephone varchar(20) not null check(len(telephone)=13 or telephone like '____-________'),
	address varchar(100) 
)

create table cardInfo
(
	cardID varchar(30) not null primary key check(cardID like '10103576_______'),
	curType varchar(10)  not null default('RMB'),
	savingType varchar(5) check(savingType in ('活期两便','活期','定期')) ,
	openDate datetime not null default(getdate()),
	balance money not null check(balance>=1),
	pass int  not null check(len(pass)=6) default(888888),
	IsReportLoss varchar(2) not null default('否') check(IsReportLoss in ('否','是')),
	customerID int not null references userInfo(customerID)
	 
)
create table transInfo 
(
	transId int primary key identity,
	transDate datetime  not null default(getdate()),
	cardID varchar(30) not null references cardInfo(cardID),
	transType nvarchar(2) not null check(transType in ('存入','取出')),
	transMoney money check(transMoney>0) not null,
	remark nvarchar(100) 
	)
insert into userInfo(customerName,PID,telephone,address) values ('孙悟空',123456789012345,'0716-78989783','北京海淀 '),('唐僧',321245678912345678,'0478-44443333','null'),
('沙和尚',421345678912345678,'0478-44223333','null')
insert into cardInfo(balance,savingType,cardID,customerID,IsReportLoss)values ('1000','活期','101035761234567','1','否'), ('1','定期','101035761212117','2','否'), ('1','定期','101035761212113','3','否')
update cardInfo set pass=611234 where customerID=1
select * from cardInfo
select * from transInfo
select * from userInfo
insert into transInfo(cardID,transType,transMoney,remark) values ('101035761234567','取出','200','孙悟空要取200块钱')
update cardInfo set balance=(balance -200) where customerID=1
select cardID from cardInfo where customerID=3
insert into transInfo(cardID,transType,transMoney,remark) values ('101035761212113','存入','300','沙和尚存入300块钱')
update cardInfo set balance=(balance +300) where customerID=3
update cardInfo set IsReportLoss='是' where customerID=2 

select * from cardInfo where openDate between 2021-03-09 and 2021-03-21

use bbs
go
select *  from bbsUsers
alter table bbsTopic drop column tCount  

alter table bbsUsers add telephone varchar(20)  check(len(telephone)=11)
update bbsUsers set telephone=(12345678912) where UIDD=1
update bbsUsers set telephone=(01234567899) where UIDD=2
update bbsUsers set telephone=(12345678998) where UIDD=3
alter table bbsUsers add constraint CK_bbsUsers_telephone unique(telephone)		
alter  table bbsReply alter column rMsg varchar(200)
alter table bbsUsers drop constraint CK_bbsUsers_uPoint
update bbsUsers set uName ='小雪' where  UIDD =1
update bbsUsers set uPoint=(uPoint+100)
select *  into bbsUser2 from bbsUsers

select * from bbsUser2
delete from bbsUser2
truncate table bbsUser2