create database NOE

go

create table tblHouseInfo
(
	HouseID int foreign key references tblUser(userId),
	Housedesc text not null,
	zj money not null,
	shi int not null,
	ting int not null,
	typeld varchar(18),
	qxld varchar(20) 
)

create table tblUser 
(
	userId int primary key identity(1,1),
	userName nvarchar(20),
	userTel varchar(11) not null
)

create table tblHouseType
(
	 typeld varchar(18) check(typeld in('别墅','平房','普通住宅','地下室')),
	 tyname nvarchar(20)
)
create table tblQx
(
	qxld varchar(20) check(qxld in ('武昌','汉阳','汉口')),
	qxname nvarchar(20)
)