create database TestDB
on primary
 (
 name='TestDB_data',
 filename='D:\sql.mdf',

 size=10mb,
 filegrowth=10%

 )
 go
use  TestDB
go
create table typeInfo 
(
typeId int  primary key identity,
typeName varchar(10) not null,

)
go
create table loginInfo
(
LoginId  int  primary key not null,

LoginName char(10) unique not null ,
LoginPwd  char(20) not null default(123456),
sex text ,

birthday  int,
leibie int

) 