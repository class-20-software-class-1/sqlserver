


use master
go


create database Stundents

on
(
 name='Stundents',
 filename='D:\Program Files\Stundents.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Stundents_log',
 filename='D:\Program Files\Stundents_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go

use Stundents
go

create table StuInfo
(
 StuID int primary key identity(1,1) not null,
 StuNum nvarchar(10) not null,
 StuName nvarchar(20) not null,
 StuSex char(2) default('��') check(StuSex='��' or StuSex='Ů') not null,
 StuPhone char(11) not null,
 )

use Stundents
go

create table ClassInfo
(
 ClassID int primary key identity(1,1) not null,
 ClassNum nvarchar(15) not null,
 ClassName nvarchar(30) not null,
 ClassRemark text
 foreign key (ClassID) references StuInfo (StuID)
)