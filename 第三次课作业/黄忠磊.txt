use master
go
create database Student
on(
	name='Student',
	filename='D:\Student.mdf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)

log on
(
   name='Student_log',
	filename='D:\Student_log.ldf',
	size=10MB,
	maxsize=100MB,
	filegrowth=10MB
)
go
use Student
go
create table Class
(ClassID int primary key identity(1,1),
ClassName nvarchar(20) unique not null,
)
create table Student
(stuID int primary key identity(1,1),
ClassID int references class(ClassID),
stusex nvarchar(1)  check (stusex='男'or stusex='女'),
StuBirthday datetime ,
StuPhone nvarchar (11)
)
create table Course
(CourseID int primary key identity(1,1),
CourseName nvarchar(50) not null,
CourseCredit int check(CourseCredit between 1and 5)default(1) not null
)
create table Score
(
ScoreID int not null ,
StuID int ,
CourseID int ,
Score decimal(5,2) unique not null
)
alter table Student  add StuAddress nvarchar(50) 
alter table  Score add constraint PK_Score_ScoreID  primary key(ScoreID)
alter table  Score add constraint FK_Score_StuID foreign key (StuID) references Student(StuID)
alter table  Score add constraint FK_Score_CourseID foreign key (CourseID)references Course(CourseID)