if exists(select*from sys.databases where name='practice02')
	drop database practice02
create database practice02
on
(
	name='practice02',
	filename='D:\text\practice02.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=20MB
)

log on
(
	name='practice02_log',
	filename='D:\text\practice02.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=20MB
)
go

use practice02
go
create table Teschers
(
	eatherID int primary key identity(1,1) not null,
	TeatherName nvarchar(20) not null,
	TeatherSalary money,
	TeatherSex char(2) not null,
	TeatherBrith date,
)

