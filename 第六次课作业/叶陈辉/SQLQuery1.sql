use master 
go 
create database TestDB 
on
(
	name='TestDB',
	filename='D:\TestDB.mdf'
)
log on
(
	name='TestDB_log',
	filename='D:\TestDB_log.ldf'
)

use TestDB
go
create table typeInfo
(
	typeId int primary key identity(1,1),
	typeName varchar(10) not null
)


use TestDB
go 
create table loginInfo
(
	LoginId int primary key identity(1,1),
	LoginName varchar(10) not null unique ,
	LoginPwd varchar(20) not null default('123456'),
	LoginSex nvarchar(1) not null check(LoginSex='��' or LoginSex='Ů'),
	LoginBir varchar(10),
	LoginVIP nvarchar(5)

)



use master
go 
create database company
on
(
	name='company',
	filename='E:\company.mdf'
)
log on
(
	name='company_log',
	filename='D:\company_log.ldf'
)

use company 
go
create table sectionInfo
(
	sectionID INT PRIMARY KEY identity(1,1),
	sectionName varchar(10) not null
)

create table userInfo
(
	userNo  int primary key  identity(1,1) not null,
	userName  varchar(10) unique not null check(len(userName)>4),
	userSex   varchar(2) not null check(userSex='��' or userSex='Ů'),
	userAge   int not null check(userAge>=1 and userAge<=100),
	userAddress  varchar(50) default'����',
	userSection  int foreign key references sectionInfo(sectionID)
)

create table workInfo
(
	workId int identity(1,1) primary key not null,
	userId  int foreign key references userInfo(userNo) not null,
	workTime datetime not null,
	workDescription  varchar(40) not null check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�' )
)

use master
go
create database StuInfo
on
(
	name='StuInfo',
	filename='D:\StuInfo.mdf'
)
log on 
(
	name='StuInfo_ldf',
	filename='D:\StuInfo_log.ldf'
)

use StuInfo

create table classInfo
(
	classid int primary key identity(1,1),
	classname varchar(10) unique not null,
	classtime datetime not null,
	classdes nvarchar(50) 
)

create table stuInfo
(
	stuid int primary key identity(1,1),
	stuname nvarchar(10) check(len(stuname)>2) unique,
	stusex nvarchar(1) check(stusex='��' or stusex='Ů') default'��' not null,
	stuage int check(stuage>15 and stuage<40) not null,
	stusite nvarchar(50) default'�����人',
	classid int ,
)

create table courseInfo
(
	courseid int primary key identity(1,1),
	coursename nvarchar(10) not null unique ,
	coursesite nvarchar(50)
)

create table scoreInfo
(
	scoreid int primary key identity(1,1),
	stuid int not null,
	courseid int not null,
	score int check(score>=0 and score<=100)
)




use master
go

create database house
go 
use house

create table tblUser
(
	userid int not null primary key identity(1,1),
	userName nvarchar(10) not null,
	userTel varchar(13) not null unique
)

create table tblHouseType
(
	typeid int identity(1,1) primary key,
	typeName nvarchar(10) not null, 
)

create table tblQx
(
	qxid int identity(1,1) primary key ,
	qxName nvarchar(10) not null ,
)

create table tblHouseInfo
(
	id int primary key identity(1,1),
	fwms nvarchar(50) ,
	userid int ,
	zj int not null ,
	shi varchar(5),
	ting varchar(5),
	typeid int ,
	qxid int
)