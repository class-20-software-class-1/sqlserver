use master
go

create database Studen
on
(
	name=Studen,
	filename='D:\zuoye\Studen.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=Studen_log,
	filename='D:\zuoye\Studen_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)


use Studen
go

create table stuinfo
(
	stuNO varchar(10) unique not null,
	stuName nvarchar(5),
	stuAge int,
	stuAddres nvarchar(5),
	stuSeat int identity(1,1) unique not null,
	stuSex nchar(1) check(stuSex='0' or stuSex='1')
)

go
insert into Stuinfo values ('s2501','张秋利',20,'美国硅谷','1'),('s2502','李斯文',18,'湖北武汉','0'),('s2503','马文才',22,'湖南长沙','1'),
('s2504','欧阳俊雄',21,'湖北武汉','0'),('s2505','梅超风',20,'湖北武汉','1'),('s2506','陈旋风',19,'美国硅谷','1'),('s2507','陈风',20,'美国硅谷','0')

select * from stuinfo


use Studen
go

create table chengji
(
	examNO int identity(1,1) unique not null,
	stuNO varchar(10) unique not null,
	writtenExam int,
	labExam int,
)

go
insert into chengji values ('s2501',50,70),('s2502',60,65),('s2503',86,85),
('s2504',40,80),('s2505',70,90),('s2506',70,90)

select * from chengji

select 学号=stuNO,stuName as 姓名,stuAge as 年龄,stuAddres as 地址,stuSeat as 座位号,stuSex as 性别  from Stuinfo
select stuName,stuAge,stuAddres from Stuinfo
select 学号=examNO,writtenExam as 笔试,labExam 机试 from chengji 
select stuNO,邮箱=stuName+'@'+stuAddres from Stuinfo
select examNO,stuNO,总分=writtenExam+labExam from chengji
select stuAddres from Stuinfo
select distinct 所有年龄=stuAge from Stuinfo
select * from Stuinfo where stuSeat<=3
select stuName,stuSeat  from Stuinfo where stuSeat<=4
select top 50 percent * from Stuinfo 
select * from Stuinfo where stuAddres='湖北武汉' and stuAge=20
select labExam from chengji where labExam>=60 and labExam<=80 order by labExam desc
select * from Stuinfo where stuAddres='湖北武汉' or stuAddres='湖南长沙'
select * from Stuinfo where stuAddres in('湖北武汉' , '湖南长沙')
select writtenExam from chengji where not labExam>=70 and labExam<=90
select * from Stuinfo where stuAge is null
select * from Stuinfo where stuAge is not null
select * from Stuinfo where stuName like '张%'
select * from Stuinfo where stuAddres like '湖%'
select * from Stuinfo where stuName like '张_'
select * from Stuinfo where stuName like '__俊_'
select * from Stuinfo order by stuAge desc
select * from Stuinfo order by stuAge desc,stuSeat asc
select top 1* from chengji order by writtenExam desc
select top 1* from chengji order by labExam