create database bbs
on

(
	name='bbs',
	filename='E:\bbs.mdf',
	size=10mb,
	maxisize=100mb,
	filegrowth=10mb
)

log on

(
	name='bbs_log',
	filename='E:\bbs_log.ldf',
	size=10mb,
	maxisize=100mb,
	filegrowth=10mb
)

go
use bbs
go

create table bbsUsers
(
	UID int primary key identity(1,1),
	uName varchar(10) unique not null,
	uSex varchar(2) check(uSex=' ' or uSex='1') not null,
	uAge int check(uAge>15 and uAge<60) not null,
	uPoint int check(uPoint>=0) not null
)

go
use bbs
go 

create table bbsSection
(
	sID int primary key identity(1,1),
	sName varchar(10) not null,
	sUid int foreign key references  bbsUsers(UID)

)

go
use bbs
go

create table bbsTopic
(
	tID  int primary key identity(1,1),
	tUID int foreign key references bbsUsers(UID),
	tSID int foreign key references bbsSection (sID),
	tTitle varchar(100) not null,
	tTime datetime,
	tCount  int
)


go
use bbs
go

create table bbsReply
(
	rId int primary key identity(1,1),
	rUID int foreign key references bbsUsers(UID),
	rTID int foreign key references bbsTopic(tID),
	rMsg  text not null,
	rTime  datetime 
)