use master
go

create database bank
on
(
	name='bank',
	filename='D:\bank\bank.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=15%
)
log on
(
	name='bank_log',
	filename='D:\bank\bank_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=15%
)
go 

use bank
go


create table userInfo 
(
	customerID int primary key identity(1,1),
	customerName varchar(10) not null,
	PID nvarchar(20) unique check(len(PID)=18 or len(PID)=15) not null,
	telephone varchar(13) check(telephone like '____-________' and len(telephone)=13) not null,
	address text
)
go

create table cardInfo
(
	cardID char(18) primary key check(cardID like '1010 3576 [0-9][0-9][0-9][0-9] [0-9][0-9][0-9]' ) not null, 
	curType nvarchar(10) default('RMB') not null,
	savingType nvarchar(4) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
	openDate datetime default(getdate()) not null,
	balance money check(balance>=1) not null,
	pass int check(len(pass)=6) default(888888) not null,
	IsReportLoss nvarchar(4) default('否') check(IsReportLoss='是' or IsReportLoss='否') not null,
	customerID int references userInfo(customerID) not null
)
go

create table transInfo 
(
	transId	 int primary key identity(1,1),
	transDate datetime default(getdate()) not null,
	cardID	char(18) references cardInfo(cardID) not null,
	transType nvarchar(2) check(transType='存入' or transType='支取') not null,
	transMoney money check(transMoney>0) not null,
	remark text
)

insert into userInfo (customerName,PID,telephone,address) values ('孙悟空',123456789012345,'0716-78989783','北京海淀')
insert into userInfo (customerName,PID,telephone) values('沙和尚',421345678912345678,'0478-44223333')
insert into userInfo (customerName,PID,telephone) values('唐僧',321245678912345678,'0478-44443333')


 

insert into cardInfo (cardID,savingType,balance,customerID) values ('1010 3576 1234 567','活期',1000,1)
insert into cardInfo (cardID,savingType,balance,customerID) values ('1010 3576 1212 117','定期',1,2)
insert into cardInfo (cardID,savingType,balance,customerID) values ('1010 3576 1212 113','定期',1,3)


--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where cardID='1010 3576 1234 567'

--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
insert into transInfo (cardID,transType,transMoney ) values ('1010 3576 1234 567','支取',200)
update cardInfo set balance=balance-200 where customerID=1

--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)

insert into transInfo (cardID,transType,transMoney ) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where customerID=2

--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”

update cardInfo set IsReportLoss='是' where customerID=3

--5.	查询出2021-03-09到2021-03-19开户的银行卡的信息 between and 

select *from cardInfo where openDate between '2021-03-09' and '2021-03-22'


select *from userInfo
select *from cardInfo order by customerID
select *from  transInfo 













