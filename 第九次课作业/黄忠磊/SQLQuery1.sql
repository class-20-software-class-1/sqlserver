use master
go

create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\bbs.ldf',
	size=5,
	maxsize=15,
	filegrowth=10%
)

go
use bbs
go
create table bbsUsers
(
	bbbsUID int	identity(1,1),
	uName varchar(10) not null,
	uSex  varchar(2) not null ,
	uAge  int not null ,
	uPoint  int not null 
)
go
alter table bbsUsers add constraint PK_bbbsUID primary key (bbbsUID)
alter table bbsUsers add constraint UK_uName unique(uName)
alter table bbsUsers add constraint CK_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint CK_uAge check(uAge>14 and uAge<61 )
alter table bbsUsers add constraint CK_uPoint check(len(uPoint)>=0)
go
create table bbsSection
(
	bbssID  int identity(1,1),
	sName  varchar(10) not null,
	sUid   int
)

go
alter table bbsSection add constraint PK_bbssID primary key (bbssID)
alter table bbsSection add constraint FK_sUid foreign key (sUid) references bbsUsers(bbbsUID)
go
create table bbsTopic
(
	tID  int primary key identity(1,1),
	tUID  int foreign key references bbsUsers(bbbsUID),
	tSID  int foreign key references bbsSection(bbssID),
	 tTitle  varchar(100) not null,
	 tMsg  text,
	 tTime  datetime default(getdate()),
	 tCount  int
)

create table bbsReply
(
	rID  int primary key identity(1,1),
	rUID  int foreign key references bbsUsers(bbbsUID),
	rTID  int	foreign key references bbsTopic(tID),
	rMsg  text NOT NULL,
	rTime  datetime default(getdate())
)

go
insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection (sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2 ,4,'范跑跑','谁是范跑跑 ',2008-7-8,1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(3 ,1,'.NET','与JAVA的区别是什么呀？ ',2008-9-1,2)
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(1 ,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀 ',2008-9-10,0)

insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,1,'不认识',2008-7-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(1 ,2,'没有区别',2008-9-11)
insert into bbsReply(rUID,rTID,rMsg,rTime) values(2 ,2,'请百度',2008-9-12)

delete from bbsReply where rTID=1
delete from bbsTopic where tUID=2
delete from bbsReply where rUID=2
delete from bbsUsers where uName='逍遥'

update bbsUsers set uPoint=uPoint+10 where bbbsUID=1


--delete bbsTopic where tID=3
--delete bbsSection where bbssID=3
--select * from bbsUsers
--delete from bbsReply
--select * from bbsTopic
--delete bbsTopic where tCount=1
--alter table bbsUsers add telephone varchar(20)not null
--alter table bbsUsers add constraint  CK_bbsUsers_telephone check(len(telephone)=11)
--alter table  bbsReply alter column rMsg varchar(200)
--alter table bbsUsers drop CK_uPoint  
--UPDATE bbsUserS SET uName='小雪' WHERE bbbsUID =1
--UPDATE   bbsUserS   SET uPoint=uPoint+100 
SELECT * INTO bbsUser2 FROM bbsUserS
SELECT * FROM bbsUser2
--DELETE table bbsUser2
drop  table  bbsUser2
TRUNCate table bbsUser2

--在论坛数据库中完成以下题目
select * from bbsUsers
select * from bbsSection
select * from bbsTopic
select * from bbsReply
--1.在主贴表中统计每个版块的发帖总数
select tSID  板块,count(*) 发帖总数 from bbsTopic group by tSID       
--2.在回帖表中统计每个主贴的回帖总数量
select rTID 回贴,count(*) 回帖总数 from bbsReply group by rTID
--3.在主贴表中统计每个用户的发的主帖的总数
select tUID, count(*) 主贴的总数 from bbsTopic group by tUID  
--4.在主贴表中统计每个用户发的主贴的回复数量总和
select RUid, count (*) from BbsReply group by RUid  
--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量
select tSID , avg(tCount) from bbsTopic group by tSID  having avg(tCount)>3
--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
select bbbsUID , uSex  ,uName 用户 ,uPoint 积分 from bbsUsers where uPoint=(select max(uPoint) from bbsUsers)
--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select  tID  ,tTitle ,tMsg   from bbsTopic where tTitle like '%快乐%' or tMsg like '%快乐%' 
--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select bbbsUID ,uAge,uPoint  from bbsUsers where uAge>=15 and uAge<=20 and uPoint>10
--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select bbbsUID, uName from bbsUsers where uName like '小-大'
--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
select tTime 时间,tCount 回复数量  from bbsTopic where tTime>'2008-9-10 12:00:00' and tCount>10
--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select tCount ,tUID   , tMsg    from bbsTopic where tTitle like '%!' 