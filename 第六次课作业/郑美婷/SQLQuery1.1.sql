use master 
go

if exists(select*from sys.databases where name='TestDB')
drop database TestDB

create database TestDB
on
(
name='TestDB',
filename='D:\SQL\TestDB.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on 
(
name='TestDB_log',
filename='D:\SQL\TestDB_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use TestDB
go
create table typeInfo
(
typeId int primary key identity(1,1),
typeName nvarchar(10) not null
)
create table loginInfo
(
LoginId int primary key identity(1,1),
LoginName nvarchar(10) unique not null,
LoginPwd nvarchar(20) not null default(123456),
Sex char(2) check(Sex in ('��','Ů')),
Birthday date,
vip nvarchar
)