create database House
on
(
name=TestDB,
filename='D:\test\House.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name=TestDB_log,
filename='D:\test\House_log.ldf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
go
use House
go
create table tblUser
(
userId int primary key identity(1,1),
userName nvarchar(10) not null,
userTel varchar(15) not null,
)
go
create table tblHouseType
(
typeId int  primary key identity(1,1) ,
typName nvarchar(10) not null,
)
go
create table tblQx
(
qxId int primary key identity(1,1),
qxName nvarchar(10),
)
go 
create table tblHouseInfo
(
ID int primary key identity(1,1),
ADesc text not null,
zj int not null,
shi nvarchar(10),
ting nvarchar(10),
)