create database bbs
on(
  name=bbs,
  filename='D:\bbs.mdf', 
  maxsize=50mb,
  size=10mb,
  filegrowth=10%
)
log on(
  name=bbs_log,
  filename='D:\bbs_log.mdf', 
  maxsize=50mb,
  size=10mb,
  filegrowth=10%

)
go
create table bbsUser
(
UID int identity (1,1) not null ,
uName varchar(10) not null,
uSex varchar(2) not null,
uAge int not null,
uPoint int not null
)
create table bbsSection
(
sID int identity (1,1) not null,
sName varchar(10) not null,
sUid int
)
--添加约束
-- alter table 表名 add constraint 约束名  约束类型 
alter table bbsUser add constraint PK_bbsUsers_uID primary key(uID)
alter table bbsUser add constraint UK_bbsUsers_uName unique(uName)
alter table bbsUser add constraint CK_bbsUsers_uSex check(uSex='男' or uSex='女')
alter table bbsUser add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(uID)

create table bbsTopic
(
tID int primary key identity (1,1),
tUID int references bbsUser(UID),
tSID int references bbsSection(sID),
tTitle varchar(100) not null,
tMsg text not null,
tTime datetime,
tCount int
)
create table bbsReply
(rID int primary key identity (1,1),
rUID int references bbsUser(UID),
rTID int references bbsTopic(tID),
rMsg text not null,
rTime datetime
)
insert into bbsUser(uName,uSex,uAge,uPoint)values('小雨点','女',20,0)
insert into bbsUser(uName,uSex,uAge,uPoint)values('逍遥','男',18,4)
insert into bbsUser(uName,uSex,uAge,uPoint)values('七年级生','男',19,2)
--将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，
select uName,uPoint into  bbsPoint from bbsUser

insert bbsSection(sName,sUid)
	select '技术交流', 1 union
    select '读书世界', 3 union
	select '生活百科', 1 union
	select '八卦区', 3

	insert  bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount)
		select 2 , 4,'范跑跑','谁是范跑跑','2008-7-8',1 union
		select 3 , 1,'.NET ','与JAVA的区别是什么呀？',' 2008-9-1 ',2 union
		select 1 , 3,'.NET ' , ' 有谁知道今年夏天最流行什么呀？','2008-9-10',0 

		 insert into bbsReply (rUID,rMsg,rTime)
	   select 1,'付款单','2021-3-15' union
	   select 2,'the name deferent','2021-3-15' union
	   select 3,'sex','2021-3-15'

--因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
--删除约束							  约束名称
-- alter table  表明 drop constraint  约束名称 
alter table bbsTopic drop constraint FK__bbsTopic__tUID__1920BF5C
alter table bbsReply drop constraint FK__bbsReply__rUID__1CF15040

--因为小雨点发帖较多，将其积分增加10分
	--更新数据
--update 表名 set 列名和更新后的数据 where 列和该列的的标识
update bbsUser set uPoint=14 where uID=1

--因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
--删除某一行数据
alter table bbsTopic drop constraint FK__bbsTopic__tSID__1A14E395


	--因回帖积累太多，现需要将所有的回帖删除
	--清空表数据
	truncate table bbsReply
	
--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select * from bbsTopic  where  tMsg like '%谁是%'   or tTitle  like '%谁是%'
 
--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
-- like % _ [] [^]
select * from bbsUser
select UID,uName 优秀用户  from bbsUser where uAge>15and uAge<20 and uPoint>=3

--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUser
select * from bbsUser where uName like '%小%' and uName like '%点%'
--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
--where 时间字段>'2008-9-10 12:00:00'
select * from bbsTopic
select tTitle,tMsg from bbsTopic where tTime>'2008-7-05 12:00:00' and tCount >=10
--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select * from bbsTopic
select tID,tCount from bbsTopic where tMsg like '%?%' 


--在论坛数据库中完成以下题目
select * from bbsUser --用户表
select * from bbsTopic  --主贴表
select * from bbsSection --板块表
select * from bbsReply --回帖表

--1.在 主贴表 中统计 每个版块 的 发帖总数
select tSID 版块,COUNT(*)发帖总数 from dbo.bbsTopic group by tSID
--2.在 回帖表 中统计 每个主贴 的 回帖总数量
select rTID 主贴,COUNT(*)回帖总数量  from dbo.bbsReply group by rTID
--3.在 主贴表 中统计 每个用户 的 发的主帖的总数
select tUID 用户,COUNT (*)主帖的总数 from dbo.bbsTopic group by tUID 
--4.在 主贴表 中统计 每个用户 发的 主贴的回复数量总和
select tUID 用户,SUM(tCount)主贴的回复数量 from dbo.bbsTopic group by tUID
--5.在 主贴表 中查询 每个版块 的 主贴的平均回复数量大于2的版块的平均回复数量
select tSID 版块,AVG(tCount)平均回复数量大于2 from dbo.bbsTopic group by tSID having AVG(tCount)>=2
--6.在 用户信息表 中查询出 积分最高的用户的用户名，性别，年龄和积分
    --select MAx(uPoint)最高积分 from bbsUser
select uName,uSex,uAge,uPoint from dbo.bbsUser where uPoint=(select MAx(uPoint)最高积分 from bbsUser)
--7.在 主贴表中（bbsTopic） 中将 帖子的内容或标题 中有 “快乐”两字的记录查询出来
select * from dbo.bbsTopic where tMsg like '快乐' or tTitle like '快乐'
--8.在  用户信息表（bbsUsers）中将 用户年龄在15-20之间 并且 积分在10分以上 的优秀用户查询出来（用多种方法实现）
select * from dbo.bbsUser where uAge>=15 and uAge<=20 and uPoint>=10
--9.在 用户信息表（bbsUsers） 中将 用户名 的 第一个字为“小”，第三字为“大”的用户信息查询出来
select * from dbo.bbsUser where uName like '小' and uName like '大'
--10.在 主贴表（bbsTopic） 中将 在2008-9-10 12:00:00 以后发的 并且 回复数量在10以上的帖子
   --的标题和内容查询出来，并且为列取上对应的中文列名
select tMsg 内容,tTitle 标题 from dbo.bbsTopic where tTime>'2008-9-10 12:00:00' and tCount>10
--11.在 主贴表（bbsTopic） 中将帖子 的标题是以‘！’结尾 的 帖子的发帖人编号和回复数量查询出来
select tUID 发帖人编号,tCount 回复数量 from dbo.bbsTopic where tTitle like '!'