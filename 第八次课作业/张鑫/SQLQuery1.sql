use master
go

create database sbzhb
on
(
	name=sbzhb,
	filename='D:\sbzhb.mdf',
	size=10mb,
	maxsize=50mb,
	filegrowth=15%
)
log on
(
	name=sbzhb_log,
	filename='D:\sbzhb_log.mdf',
	size=10mb,
	maxsize=50mb,
	filegrowth=15%
)
go

use sbzhb
go

create table orderItem
(
	itemID int references orders(orderld),
	orderid varchar(10),
	itemType varchar(10),
	itemName varchar(10),
	theNumber int,
	theMoney int
)

create table orders
(
	orderld int primary key identity,
	orderDate datetime
)

insert into orders values('2008-01-12 00:00:00.000')

insert into orderItem values('1','文具','笔','72','2'),('1','文具','尺','10','1'),('1','体育用品','篮球','1','56'),('2','文具','笔','36','2'),('2','文具','固体胶','20','3'),('2','日常用品','羽毛球','20','3'),('3','文具','订书机','20','3'),('3','文具','订书针','10','3'),('3','文具','裁纸刀','5','5'),('4','文具','笔','20','2'),('4','文具','信纸','50','1'),('4','日常用品','毛巾','4','5'),('4','日常用品','透明胶','30','1'),('4','体育用品','羽毛球','20','3')

select sum(theNumber)总和 from orderItem

select orderid,avg(theMoney) from orderItem where orderid<3 group by orderid having avg(theMoney)<10

select sum(theNumber) 总和,avg(theMoney) 平均单价 from orderItem group by orderId,theMoney having orderId<3 and theMoney<10
select * from orderItem

select sum(theNumber) 总和,avg(theMoney) 平均单价 from orderItem group by orderId,theNumber having sum(theNumber)>50 and avg(theMoney)<10


select itemType,count(itemType) from orderItem group by itemType 
select itemType,sum(theNumber),avg(theMoney) from orderItem group by itemType,theMoney having sum(theNumber)>100

select itemName,count(itemName),sum(theNumber),avg(theMoney) from orderItem group by itemName,theMoney