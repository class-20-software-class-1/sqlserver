use master
go
create database Student
on
(
name='Student',
filename='D:\SQLcunchu\Student.mdf',
size=5mb,
=100mb,maxsize
filegrowth=10%
)
log on
(
name='Student_log',
filename='D:\SQLcunchu\Student_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use Student
go
create table class
(
 classid int primary key identity(1,1),
 classname nvarchar(10) not null unique,
 classtime datetime not null,
 classsth ntext
)
create table student
(
stuid int primary key identity(1,1),
stuname nvarchar(20)  unique check(stuname>=2),
stusex varchar(2) check(stusex in ('��','Ů')) default('��') not null,
stuage int not null check(stuage>=15 and stuage<=40),
studz nvarchar(200) default('�����人'),
classid int references class(classid)
)
create table classxx
(
classesId int primary key identity(1,1),
classesName nvarchar(10) not null unique,
classesMs ntext
)
create table score
(
scoreid int primary key identity(1,1),
stuid int references student(stuid) not null,
classid int references class(classid) not null,
score int check(score>=0 and score<=100)
)