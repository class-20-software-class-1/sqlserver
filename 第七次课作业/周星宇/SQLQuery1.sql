use master
go

create database bank
on
(
    name='bank',
	filename='D:\bank.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=15%
) 

log on
(
    name='bank_log',
	filename='D:\bank_log.dlf',
	size=5MB,
	maxsize=50MB,
	filegrowth=15%
) 
go

use master 
go

create table userInfo 
(
    customerID int primary key identity(1,1),
	customerName varchar(10) not null,
	PID nvarchar(20) not null check(len(PID)=15 or len(PID)=18) unique,
	telephone varchar(20) check(len(telephone)=13 and telephone like '____-________'),
	address nvarchar(20)
)

use master 
go

create table cardInfo
(
    cardID nvarchar(20) check(len(cardID)=15 and cardID like'1010 3576 ____ ___'),
	curType nvarchar(10) default('RMB'),
    savingType varchar(10) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
	openDate datetime not null default(getdate()),
	balance int not null check(balance>=1),
	pass int not null check(len(pass)=6) default(888888),
	IsReportLoss varchar(2) check(IsReportLoss='是' or IsReportLoss='否') default('否'),
	customerID  int not null references userInfo(customerID)
)



use master 
go

create table transInfo
(
    transId int primary key identity(1,1),
	transDate date not null,
	cardID int not null references userInfo(customerID),
	transType varchar(2) not null check(transType='存入' or transType='支取') ,
	transMoney int not null check(transMoney>0),
	remark nvarchar(10),
)

insert into userInfo(customerName,PID,telephone,address)
select'孙悟空','123456789012345','0716-78989783','北京海淀' union
select'沙和尚','421345678912345678','0478-44223333','北京海淀' union
select'唐僧','3212456789123456789','0478-44443333','北京海淀'

insert into cardInfo(cardID,curType,savingType)
select'1010 3576 1234 567','活期','1000' union
select'1010 3576 10103571212117','定期','1' union
select'1010 3576 1234 567','定期','1000' 

UPDATE cardInfo SET pass='611234' where customerID=1
select * from cardInfo 


insert into cardInfo(cardID,balance,pass,IsReportLoss)
select'1010 3576 1234 567','balance=balance-200','611234','否'  
UPDATE cardInfo SET balance=balance-200 where customerID=1
select * from cardInfo

insert into cardInfo(cardID,balance,pass,IsReportLoss)
select'1010 3576 10103571212117','balance=balance+300','888888','否'  
UPDATE cardInfo SET balance=balance+300 where customerID=2
select * from cardInfo

insert into cardInfo(IsReportLoss)VALUES('是')

select * from cardInfo where openDate between 2021-02-09 and 2021-03-19