create database schoolInfo
go
create table classInfo
(	
	classid int primary key identity(1,1),
	classname nvarchar(10) not null unique,	
	classtime datetime not null,
	miaoshu text 
)

create table stuinfo
(
	stuno int primary key identity(1,1),
	stuname char(200) check (len(stuname)>2),
	stuage int check(stuage>14 and stuage<41),
	stuaddress nvarchar(100) default('�����人'),
	classid int foreign key references classInfo(classid)
)

create table kinfo
(
	kno int primary key identity(1,1),
	kname nvarchar(100) unique not null,
	miaoshu text
)

create table cinfo
(
	cno int primary key identity(1,1),
	cstu int foreign key references stuinfo(stuno),
	ck int foreign key references kinfo(kno),
	cj int check(cj>0 and cj<101)
)