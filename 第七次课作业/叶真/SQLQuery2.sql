use master 
go

create database Bank
on
(
	name='Bank',
	filename='D:\bank\Bank.mdf'
)
log on
(
	name='Bank_log',
	filename='D:\bank\Bank_log.ldf'
)
go

use Bank
go

create table userInfo
(
	customerID int identity(1,1) primary key,
	customerName nvarchar(5) not null,
	PID varchar(20) not null unique check(len(PID)=15 or len(PID)=18),
	telephone varchar(13) not null check(len(telephone)=13 and telephone like '____-________'),
	address nvarchar(20)
)

create table cardInfo
(
	cardId varchar(16) not null primary key,
	curType nvarchar(5) not null default('RMB'),
	savingType nvarchar(4) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
	openDate datetime not null default(getdate()),
	balance int not null check(balance>=1),
	pass char(6) not null default('888888'),
	IsReportLoss nvarchar(1) check(IsReportLoss='是' or IsReportLoss='否') default('否'),
	customerID int foreign key references userInfo(customerID)
)

create table transInfo
(
	transId int identity(1,1) primary key,
	transDate datetime default(getdate()) not null,
	cardID varchar(16) not null unique,
	transType nvarchar(2) not null check(transType='存入' or transType='支取'),
	transMoney int not null check(transMoney>0),
	remark nvarchar(50),
)

insert into userInfo (customerName,PID,telephone,address)
select '孙悟空','123456789012345','0716-78989783','北京海淀' union
select '沙和尚','421345678912345678','0478-44223333','' union
select '唐僧','321245678912345678','0478-44443333',''


insert into cardInfo(balance,savingType,cardId,customerID)
select '1000','活期','101035761234567','2' union
select '1','定期','101035761212117','1' union
select '1','定期','101035761212113','3' 


--1.	将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where customerID=2


--2.	用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额


insert into transInfo(cardID,transType,transMoney)
select '123456789012345','支取','200'

update cardInfo set balance=(balance-200) where customerID=2


--3.	用同上题一样的方法实现沙和尚存钱的操作(存300)

insert into transInfo(cardID,transType,transMoney)
select '101035761212117','存入','300'

update cardInfo set balance=(balance+300) where customerID=1


--4.	唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”

update cardInfo set IsReportLoss='是' where customerID=3

--5.	查询出2021-03-09到2021-03-19开户的银行卡的信息

select * from cardInfo where openDate between 2021-03-09 and 2021-03-21