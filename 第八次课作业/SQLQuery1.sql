create database orderskk
on(
        name='orderskk',
		filename='D:\orderskk.mdf',
		size=10,
		maxsize=100,
		filegrowth=10
		

)
log on
(             
        name='orderskk_log',
		filename='D:\orderskk_log.ldf',
		size=10,
		maxsize=100,
		filegrowth=10
      


)
go
use orderskk
go
create table orders
( orderId int primary key identity ,
  orderDate datetime 
)
go
create table orderItem
( 
    ItemiD int primary key identity ,
	orderId int ,
	itemType nvarchar(10),
	itemName nvarchar(10),
	theNumber money  ,
	theMoney money ,
)
insert  orders values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
insert  orderItem values
(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3), 
(2,'日常用品','透明胶',2,4), 
(2,'体育用品','羽毛球',20,3), 
(3,'文具','订书机',20,3), 
(3,'文具','订书针',10,3), 
(3,'文具','裁纸刀',5,5), 
(4,'文具','笔',20,2), 
(4,'文具','信纸',50,1), 
(4,'日常用品','毛巾',4,5), 
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3) 

select * from orders
select * from orderItem
select SUM (theNumber) as 总数 from dbo.orderItem
select orderId ,sum(theNumber), AVG(theMoney) from orderItem where orderId<3  group by orderId HAVING avg(theMoney)<10 
select orderId ,sum(theNumber), AVG(theMoney) from orderItem   where theNumber>50 group by orderId HAVING avg(theMoney)<10 
select itemType,COUNT(itemType)人数 from dbo.orderItem group by itemType
select itemType ,sum(theNumber), AVG(theMoney) from orderItem    group by itemType HAVING sum(theNumber)>100 
select itemName ,COUNT(itemName),SUM(theNumber),AVG(theMoney) from orderItem group by itemName