
	use master
	go

	create database Bbs
	on
	(
		name='Bbs',
		filename='D:\DATA\bbs.mdf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	log on
	(
		name='Bbs_log',
		filename='D:\DATA\bbs_log.ldf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	go

	use Bbs
	go

	create table bbsUser
	(
		UID int identity,
		uName varchar(10) not null,
		uSex  varchar(2) not null,
		uAge  int not null,
		uPoint  int not null
	)

	alter table bbsUser add constraint PK_bbsUser_UID primary key(UID)
	alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex in('男','女'))
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge between 15 and 60)
	alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)


	create table bbsTopic
		(
		tID  int primary key identity,
		tUID  int references bbsUser(UID),
		tSID  int references bbsSection(sID),
		tTitle  varchar(100) not null,
		tMsg  varchar(100) not null,
		tTime  datetime,
		tCount  int
	)

	create table bbsReply
	(
		rID  int primary key identity,
		rUID  int references bbsUser(UID),
		rTID  int references bbsTopic(tID),
		rMsg  text not null,
		rTime  datetime 
	)
	
	create table bbsSection
	(
		sID  int identity,
		sName  varchar(10) not null,
		sUid   int 
	)
	alter table bbsSection add constraint PK_bbsSection_sID primary key(sID) 
	alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(UID)

	select * from bbsUser
	select * from bbsSection
	select * from bbsTopic
	select * from bbsReply  

	insert into bbsUser values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
	insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
	insert into bbsTopic values (2,4,'范跑跑','谁是范跑跑','2008-7-8',1),
								(3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),
								(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)
	
	  -- 回帖：
	  --分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

	insert into bbsReply ( rMsg,  rTime, rUID )values ('范跑跑是',GETDATE(),1),
													  ('.NET与JAVA的区别是',GETDATE(),2),
													  ('今年夏天最流行是',GETDATE(),3)

	--1.在主贴表中统计每个版块的发帖总数
	select  tSID 板块编号 ,count(tID)发帖总数 from bbsTopic group by tSID
	--2.在回帖表中统计每个主贴的回帖总数量
	select rTID 对应主贴编号 , count(rID)回帖总数量  from bbsReply group by  rTID 
	--3.在主贴表中统计每个用户的发的主帖的总数
	select tUID 发帖人编号 ,count(tID)发帖总数 from bbsTopic group by tUID
	--4.在主贴表中统计每个用户发的主贴的回复数量总和
	select tUID 发帖人编号  ,sum(tCount)发帖总数 from bbsTopic group by tUID
	--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量
	select  tSID 版块编号, avg(tCount) 平均回复数量 from bbsTopic group by  tSID having avg(tCount)>3
	--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
	select uPoint 积分最高 ,uName 用户名,uSex 性别 , uAge 年龄 from bbsUser where uPoint =(select max(uPoint) from bbsUser)
	select  top 1 * from bbsUser  order by uPoint desc
	--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
	select * from bbsTopic where tTitle like '%快乐%' or  tMsg like '%快乐%'
	--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
	select * from bbsUser where uAge between 15 and 20  and uPoint>10 
	select * from bbsUser where  uAge>=15 and uAge<=20  and uPoint>10 
	--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
	select * from bbsUser where uName  like '小_大'
	--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
	select  tTime 发帖时间, tCount 回复数量 from bbsTopic where  tTime > '2008-09-10 12:00:00' and tCount>10 
	--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
	select tTitle 贴子的标题 , tUID 发帖人编号 , tCount 回复数量  from bbsTopic where tTitle = '%!'