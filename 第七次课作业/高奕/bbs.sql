use master
create database bbs
on
(
	name='bbs',
	filename='D:\bank\bbs.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\bank\bbs_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use bbs
create table bbsUser
(
	uID int identity(1,1) not null,
	uName varchar(10)  not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null
)

create table bbsSection
(
	sID  int identity(1,1) not null , 
	sName  varchar(10) not null,
	sUid   int

)
--添加约束
-- alter table 表名 add constraint 约束名  约束类型 
alter table bbsUser add constraint PK_bbsUser_uID primary key(uID)
alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(uID)


create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int references bbsUser(uID),
	tSID int references bbsSection(sID),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)
create table bbsReply
(
	 rID  int identity(1,1) primary key,
	 rUID  int references  bbsUser(uID),
	 rTID  int references bbsTopic(tID),
	 rMsg text not null,
	 rTime datetime
)

insert into bbsUser (uName,uSex,uAge,uPoint) values ('小雨点','女','20','0')
,('逍遥','男','18','4'),('七年级生','男','19','2')

select uName,uPoint into bbsPoint from bbsUser

insert into bbsSection (sName,sUid) values ('技术交流',1),
('读书世界',3),('生活百科',1),('八卦区',3)

insert into bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount) values ('2','4','范跑跑','谁是范跑跑','2008-7-8','1'),
('3','1','.NET','谁是范跑跑','2008-9-1','2'),('1','3','.今年夏天最流行什么呀?','有谁知道今年夏天最流行','2008-9-10','0')

insert into bbsReply (rUID,rMsg,rTime) values ('1','范跑跑是赵家俊小名','2021-1-1'),
('2','名字不一样','2021-1-2'),('3','EMO','2021-1-3')

--删除约束							  约束名称
-- alter table  表明 drop constraint  约束名称 
alter table bbsTopic drop constraint  FK__bbsTopic__tUID__1920BF5C
alter table bbsReply drop constraint FK__bbsReply__rUID__1CF15040

--删除某一行数据
--delete from 表名 where 列名 数据
delete from bbsUser where uName='逍遥'

--更新数据
--update 表名 set 列名和更新后的数据 where 列和该列的的标识
update bbsUser set uPoint=14 where uID=1

--删除某一行数据
alter table bbsTopic drop constraint FK__bbsTopic__tSID__1A14E395

delete from bbsSection where sName='生活百科'

--清空表数据
truncate table bbsReply

--2、新增字段：用户表新增“手机号码”字段，并为该字段添加唯一约束，且长度为11，必填
alter table bbsTopic add telephone int 
alter table bbsTopic add constraint UK_bbsTopic_telephone unique(telephone)
alter table bbsTopic add constraint CK_bbsTopic_telephone check(len(telephone)=11)

--3、修改字段：修改回帖表的“回帖消息”字段，数据类型改成varchar(200)
--alter table 表名 alter column
alter table bbsReply alter column rMsg varchar(200)

--4、删除约束：删除用户表的“积分”字段的检查约束
--alter table 表名 drop constraint
alter table bbsUser drop constraint CK_bbsUser_uPoint

--5、修改数据：修改用户“小雨点”的名字改成“小雪”，将所有用户的积分都增加100
update  bbsUser set uName='小雪' where uID=1
update  bbsUser set uPoint=uPoint+100 

--6、复制用户表的数据到新表bbsUser2
--select 指定列或* into 新表 from  
select * into bbsUser2 from bbsUser

--7、删除bbsUser2的所有数据，写出两种方式的语句
--delete from 删除表数据
delete from bbsUser2
--truncate table 清空表数据 释放空间
truncate table bbsUser2










