CREATE DATABASE userInfo
on
 (
 name='ATMsystem_data',
 filename='D:\bank\ATMsystem_data.mdf',
 size=5,
 maxxize=50,
 filegrowth=15%
 )
 log on
 (
 name='ATMsystem1_data',
 filename='D:\bank\ATMsystem1_data.mdf',
 size=1,
 filegrowth=5
 )
 go


 create table userInfo
 (
 customerID int primary key identity(1,1),
 customerName  nvarchar(10) not NULL,
 PID  VARCHAR(30) check (len(PID) = 18 or len(PID)=15) not NULL,
 telephone  varchar(20) check(len(telephone)=13 or telephone like'____-________'),
 address varchar(20)
 )
 create table cardInfo
 (
 cardID varchar(30)primary key check(cardID like'1010 3576 ____ ____'),
 curType varchar(20) default ('RMB') not NULL,
 savingTypev nvarchar(20)check(savingTypev in ('活期,定期，定活两期')),
 openDate datetime default(getdate()) not NULL,
 balance varchar(20) check(len(balance)>1) not null,
 pass varchar(6)  check(len(pass)=6)default('8888888'),
 IsReportLoss varchar(2) check(IsReportLoss in('是,否'))default('否') not NULL,
 customerID int references userInfo(customerID) not NULL,
 )
 create table trabsInfo
 (
 trasID int primary key identity(1,1),
 trasnDate datetime default(getdate()) not NULL,
 cardID varchar(20) ,
 transTypes varchar(2) check(transTypes in('存入,支出')),
 transMoney money check(transMoney>0)not NULL,
 remark text
 )
 select * from userInfo
 select * from cardInfo
 insert into userInfo values('孙悟空',123456789012345,'0716-78989783','北京海淀,')
 insert into userInfo(customerName,PID,telephone) values('沙和尚',421345678912345678,'0478-44223333'),('唐僧',321245678912345678,'0478-44443333')
 insert into cardInfo(cardID,savingTypev,balance,customerID) values('1010 3576 1234 567','活期',1000,1),('1010 3576 1212 117','定期',1,2),('1010 3576 1212 113','定期',1,3)


 --1.将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234 where cardID='0716-78989783'
--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支出',200)
update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'
--3.用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)
update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'
--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是' where customerID=3
--5.查询出2021-03-09到2021-03-19开户的银行卡的信息
select * from cardInfo where openDate like '2021-03-[0-1]9'