create database ATM on(
	name='ATM',
	filename='D:\bank\ATM.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=15%
)
log on(
	name='ATM_log',
	filename='D:\bank\ATM.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=15%
)
go

use ATM
go

--用户信息表：userInfo ：
create table userInfo(
--customerID	顾客编号	自动编号（标识列），从1开始，主键
	customerID int primary key identity(1,1),
--customerName	开户名	必填
	customerName varchar(10) not null,
--PID	身份证号	必填，只能是18位或15位，身份证号唯一约束
	PID varchar(18) not null unique check(len(PID)=18 or len(PID)=15),
--telephone	联系电话	必填，格式为xxxx-xxxxxxxx或手机号13位
	telephone char(13) not null check(telephone like '____-________' or len(telephone)=13),
--address	居住地址	可选输入
	address varchar(50)
)

--银行卡信息表：cardInfo
create table cardInfo(
--cardID	卡号	必填，主健，银行的卡号规则和电话号码一样，一般前8位代表特殊含义，如某总行某支行等。
--假定该行要求其营业厅的卡号格式为：1010 3576 xxxx xxx开始,每4位号码后有空格， 
	cardID char(20) not null primary key check(cardID like'1010 3576 ____ ___'),
--curType	货币种类	必填，默认为RMB
	curType nchar(10) default('RMB'),
--savingType	存款类型	活期/定活两便/定期
	savingType nchar(10) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
--openDate	开户日期	必填，默认为系统当前日期
	openDate datetime not null default(getdate()),
--balance	余额	必填，不低于1元 
	balance varchar(100) not null check(balance>1),
--pass	密码	必填，6位数字，开户时默认为6个“8”
	pass int not null check(len(pass)=6) default(888888),
--IsReportLoss	是否挂失	必填，是/否值，默认为”否”
	IsReportLoss char(2) not null check(IsReportLoss='是' or IsReportLoss='否') default('否'),
--customerID	顾客编号	外键，必填，表示该卡对应的顾客编号，一位顾客允许办理多张卡号
	customerID int not null references userInfo (customerID)
)

--交易信息表：transInfo 
create table transInfo(
--transId	交易编号	标识列、主键
	transId int primary key identity(1,1),
--transDate	交易日期	必填，默认为系统当前日期
	transDate datetime not null default(getdate()),
--cardID	卡号	必填，外健 
	cardID char(20) not null references cardInfo(cardID),
--transType 	交易类型	必填，只能是存入/支取
	transType varchar(10) not null check(transType='存入' or transType='支取'),
--transMoney	交易金额	必填，大于0
	transMoney nchar(1000) not null check(transMoney>0),
--remark	备注	可选输入，其他说明
	remark text
)
--孙悟空开户，身份证：123456789012345，电话：0716-78989783，地址：北京海淀 
--   开户金额：1000 活期   卡号：1010 3576 1234 567
insert into userInfo 
select '孙悟空',123456789012345,'0716-78989783','北京海淀 '
insert into  cardInfo values('1010 3576 1234 567','RMB','活期',getdate(),1000,666666,'否',7)
--沙和尚开户，身份证：421345678912345678，电话：0478-44223333，
--   开户金额： 1  定期 卡号：1010 3576 1212 117
insert into userInfo values('沙和尚',421345678912345678,'0478-44223333','河南')
insert into cardInfo values('1010 3576 1212 117','RMB','定期',getdate(),2,888888,'否',9)
--唐僧开户，身份证：321245678912345678，电话：0478-44443333，
--   开户金额： 1  定期 卡号：1010 3576 1212 113
insert into userInfo values('唐僧',321245678912345678,'0478-44223333','北京')
insert into cardInfo values('1010 3576 1212 113','RMB','定期',getdate(),2,888888,'否',10)
--1.将用户“孙悟空”开卡时的初始密码更改为“611234”
update cardInfo set pass=611234
--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，
--然后在孙悟空账上的余额减200注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，
--再根据银行卡号来插入交易记录和修改账上余额
insert into transInfo values(getdate(),'1010 3576 1234 567','支取',200 ,'取钱' )
--3.用同上题一样的方法实现沙和尚存钱的操作(存300)
insert into transInfo values(getdate(),'1010 3576 1212 117','存入',300,'存')
insert into transInfo values(getdate(),'1010 3576 1212 117','存入',300,'存')
--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
update cardInfo set IsReportLoss='是'
