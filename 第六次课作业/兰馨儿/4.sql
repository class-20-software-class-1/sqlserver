	use master 
	go

	create database Bank
	on
	(
	name = 'Bank' ,
	filename = 'D:\Bank_mdf',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 15%
	)
	log	on
	(
	name ='Bank_log',
	filename = 'D:\Bank_ldf',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 15%
	)
	go

	use Bank
	go

	create table UserInfo
	(
	customerID int primary key identity(1,1),
	customerName varchar(10) not null,
	PID varchar(20) not null check(len(PID)=15 or len(PID)=18),
	telephone varchar(20) not null check(len(telephone)=13 and telephone like '____-________'),
	address varchar(20) 
	)

	create table CardInfo
	(
	cardID varchar(20) not null primary key check(cardID like '1010 3576 ____ ___'),
	curType varchar(10) not null default('RMB') ,
	savingType varchar(20) check(savingType in('活期','活两便','定期')),
	openDate  datetime default(GETDATE()) not null,
	balance varchar(20) not null check (balance>=1),
	pass varchar(20) check(len(pass) = 6) not null default ('888888'),
	IsReportLoss varchar(2) not null default ('否') check(IsReportLoss in('是','否')),
	customerID int references userInfo(customerID) not null
	)

	create table TransInfo 
	(
	transId int primary key identity(1,1),
	transDate datetime default(GETDATE()) not null,
	cardID varchar(20)  references CardInfo(cardID),
	transType varchar(10) not null check(transType in('存入','支取')),
	transMoney money not null check(transMoney >0),
	remark text
	)

	insert into UserInfo values('孙悟空',123456789012345,'0716-78989783','北京海淀')
	insert into UserInfo values('沙和尚',421345678912345678,'0716-78989783',''),
							   ('唐僧',321245678912345678,'0478-44443333','')
	

	insert into CardInfo( balance ,savingType,cardID ,customerID)values (1000, '活期','1010 3576 1234 567' ,1) ,
															 (1,'定期', '1010 3576 1212 117',2 ),
															 (1 , '定期','1010 3576 1212 113',3)

	--1.将用户“孙悟空”开卡时的初始密码更改为“611234”
	update CardInfo set pass = '611234'where customerID =1
	--2.用两条SQL语句实现孙悟空要取钱(取200)的操作，先向交易信息表插入一条取钱的交易记录，然后在孙悟空账上的余额减200
	--注意：先要将用户孙悟空的用户编号找到，再根据用户编号找到卡号，再根据银行卡号来插入交易记录和修改账上余额
	select * from TransInfo where cardID = '1010 3576 1234 567'
	insert into  TransInfo (transType,transMoney,cardID )values ('支取',200,'1010 3576 1234 567')
	update  CardInfo set balance = balance - 200 where customerID = 1
	--3.用同上题一样的方法实现沙和尚存钱的操作(存300)
	insert into TransInfo (transType,transMoney,cardID )values ('存入',300,'1010 3576 1212 117' )
	update CardInfo set balance = balance + 200 where customerID = 2
	--4.唐僧的卡丢了，需要挂失，将唐僧的银行卡的是否挂失字段的值改为“是”
	update CardInfo set IsReportLoss = '是' where customerID = 3
	--5.查询出2021-03-09到2021-03-19开户的银行卡的信息
	select * from  CardInfo where openDate in ('2021-03-09' , '2021-03-19 ')
	 select * from  CardInfo where openDate >= '2021-03-09' and  openDate <= '2021-03-19'

	 select * from UserInfo
	select * from CardInfo
	select * from TransInfo 