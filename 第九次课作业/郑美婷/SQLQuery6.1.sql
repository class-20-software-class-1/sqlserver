use master
go
if exists(select*from sys.databases where name='bbs')
drop database bbs
create database bbs
on
(
name='bbs',
filename='D:\SQLcunchu\bbs.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='bbs_log',
filename='D:\SQLcunchu\bbs_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use bbs
go
create table bbsUsers
(
UID int identity(1,1),
uName varchar(10) not null,
uSex varchar(2) not null,
uAge int not null,
uPoint int not null
)
alter table bbsUsers add constraint PK_bbsUsers_UID primary key(UID)
create table bbsTopic
(
tID int primary key identity(1,1),
tUID int ,
tSID int ,
tTitle varchar(100) not null,
tMsg text not null,
tTime datetime,
tCount int 
)
create table bbsReply
(
rID int primary key identity(1,1),
rUID int references bbsUsers(UID),
rTID int references bbsTopic(tID),
rMsg text not null,
rTime datetime 
)
create table bbsSection
(
sID int identity(1,1),
sName varchar(10) not null,
sUid int 
)
alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName) 
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in ('男','女'))
alter table bbsUSers add constraint CK_bbsUsers_nSex check(uAge<=20 and uAge>=15)
alter table bbsUSers add constraint CK_bbsUSers_uPoint check(uPoint>=0)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid)references bbsUsers(UID)
insert into bbsUsers(uName,uSex,uAge,uPoint)values
( '小雨点','女',20, 0),
('逍遥' ,'男',18 ,4),
('七年级生','男',19 ,2),
( '快乐宝贝','女',19, 10),
('林丹' ,'男',18 ,4),
('罗维','男',19 ,2),
( '可威威','女',20, 0),
('三大' ,'男',18 ,4),
('七年级快乐','男',19 ,2)
--select 列名1，列名2 from 表名
select uName,uPoint into bbsPoints from bbsUsers
insert into bbsSection(sName)values
(' 技术交流'),('读书世界'),('生活百科'),('八卦区')
insert into bbsSection(sName,sUid) values
('技术交流','1'),
('读书世界','3'),
('生活百科','1'),
('八卦区','3')
insert into bbsTopic(tUID,tSID,tTitle,tMsg, tTime, tCount)values
(2,4,'范跑跑！ ','谁是范跑跑','2008-7-8',1),
(3,1,'.NET','与JAVA的区别是什么呀？快乐','2008-9-1',2),
(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀!','2008-9-10',0),
(2,4,'范跑跑！ ','谁是范跑跑','2008-7-8',1),
(3,1,'.NET','与JAVA的区别是什么呀？快乐','2008-9-1',2),
(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀!','2008-9-18',10),
(2,4,'范跑跑 ','谁是范跑跑','2008-7-8',1),
(3,1,'.NET','与JAVA的区别是什么呀？快乐','2008-9-1',2),
(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀!','2008-9-12',5)
insert into bbsReply(rTID,rMsg,rTime)values
(1,'你猜','2008-7-8'),
(2,'java更头秃','2008-12-8'),
(3,'BF风','2008-7-8'),
(1,'你猜','2008-7-8'),
(2,'java更头秃','2008-9-10'),
(3,'BF风','2008-7-8'),
(1,'你猜','2008-7-8'),
(2,'java更头秃','2008-7-8'),
(3,'BF风','2008-9-12')
update bbsUsers set uPoint= uPoint+10 where UID=2
select * from bbsUsers
select * from bbsTopic
select * from bbsReply 

select tSID 版块编号,COUNT(*)发帖总数 from bbsTopic GROUP BY tSID ORDER BY tSID--1.在主贴表中统计每个版块的发帖总数

select rTID 主贴编号,COUNT(*)回帖总数量 from bbsReply GROUP BY rTID--2.在回帖表中统计每个主贴的回帖总数量

select tUID 用户编码,count(tUID) 主贴总数 from bbsTopic group by tUID--3.在主贴表中统计每个用户的发的主帖的总数

select tUID 发帖人编号,sum(tCount)回复数量总和 from bbsTopic group by tUID--4.在主贴表中统计每个用户发的主贴的回复数量总和

select tSID,Avg(tCount)平均回复数量 from bbsTopic group by tSID having Avg(tCount)>3--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量

select uName 用户名,uSex 性别,uAge 年龄,uPoint 积分 from bbsUsers where uPoint=(select max(uPoint)最高积分 from bbsUsers)--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分

select * from bbsTopic where tMsg like '%快乐%' or tTitle like '%快乐%'--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来

select uName 优秀用户 from bbsUsers where uAge>15 and uAge<20 and uPoint>10--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）

select uName 用户名字,uSex 用户性别,uAge 用户年龄 from bbsUsers where uName like '小%' or uName like '%%大'--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来

select  tTitle 贴子的标题,tMsg 帖子的内容 from bbsTopic where tTime > '2008-9-10 12:00:00' and  tCount>=10 --10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名

select  tUID 发帖人编号,tCount 回复数量 from bbsTopic where tTitle like '%!'--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来