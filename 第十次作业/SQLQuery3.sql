	create database Students03
	on
	(
	name = 'Students03',
	filename = 'D:\Students03.mdf',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 10%
	)
	log on
	(
	name = 'Students03_log',
	filename = 'D:\Students03_log.ldf',
	size = 5MB,
	maxsize = 50MB,
	filegrowth = 10%
	)
	go 
	use  Students03
	go

	if exists(select * from sys.objects where name = 'StudentInfo')
	drop table StudentInfo
	--表1-----------------------------------------------------------------------------------------------------------------------------
	create table StudentInfo
	(
	StuNo varchar(10) primary key ,
	StuName varchar(10) check(len(StuName)>=2),
	StuAge int not null,
	StuAddress nvarchar(20),
	StuSeat int ,
	StuSex nvarchar(1) default ('男') check(StuSex='女' or StuSex='男')
	)
	select * from StudentInfo
	insert into StudentInfo values ('s2501','张秋利',20,'美国硅谷',1,'男'),('s2502','李斯文',18,'湖北武汉',2,'女'),
	('s2503','马文才',22,'湖南长沙',3,'男'),('s2504','欧阳俊雄',21,'湖北武汉',4,'女'),
	('s2505','梅超风',20,'湖北武汉',5,'男'),('s2506','陈旋风',19,'美国硅谷',6,'男'),('s2507','陈风',20,'美国硅谷',7,'女')
	--表2-----------------------------------------------------------------------------------------------------------------------------
	create table StuExamInfo
	(
	ExamNo int primary key identity(1,1),
	StuNo varchar(10) references StudentInfo(StuNo),
	WrittenExam int,
	LadExam int
	)
	select * from StuExamInfo
	insert into StuExamInfo values ('s2501',50,70),('s2502',60,65),('s2503',86,85),
	('s2504',40,80),('s2505',70,90),('s2506',85,90)
	--要求-----------------------------------------------------------------------------------------------------------------------------

	--1.查询学生的姓名，年龄，笔试成绩和机试成绩
		select StuName 姓名,StuAge 年龄,WrittenExam 笔试成绩,LadExam 机试成绩 from StudentInfo
		inner join StuExamInfo on StudentInfo.StuNo = StuExamInfo.StuNo
	--2.查询笔试和机试成绩都在60分以上的学生的学号，姓名，笔试成绩和机试成绩
		select StuName 姓名,StuAge 年龄,WrittenExam 笔试成绩,LadExam 机试成绩 from StudentInfo
		inner join StuExamInfo on StudentInfo.StuNo = StuExamInfo.StuNo
		where WrittenExam > 60 and LadExam > 60
	--3.查询所有学生的学号，姓名，笔试成绩，机试成绩，没有参加考试的学生的成绩以NULL值填充
		select StuExamInfo.StuNo 学号,StuName 姓名,WrittenExam 笔试成绩,LadExam 机试成绩 from StudentInfo 
		left join StuExamInfo on StudentInfo.StuNo= StuExamInfo.StuNo
	--4.查询年龄在20以上（包括20）的学生的姓名，年龄，笔试成绩和机试成绩，并按笔试成绩降序排列
		select StuName 姓名,StuAge 年龄,WrittenExam 笔试成绩,LadExam 机试成绩 from StudentInfo
		inner join StuExamInfo on StudentInfo.StuNo = StuExamInfo.StuNo
		where StuAge >= 20
		order by WrittenExam DESC
	--5.查询男女生的机试平均分
		select StuSex 性别 ,avg(LadExam)机试成绩平均分 from StudentInfo
		inner join StuExamInfo on StudentInfo.StuNo = StuExamInfo.StuNo
		group by StuSex
	--6.查询男女生的笔试总分
		select StuSex 性别 ,sum(WrittenExam) 笔试总成绩 from StudentInfo
		inner join StuExamInfo on StudentInfo.StuNo = StuExamInfo.StuNo
		group by StuSex

	select * from StudentInfo
	select * from StuExamInfo
