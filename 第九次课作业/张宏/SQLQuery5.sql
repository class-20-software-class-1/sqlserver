use master 
go

create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)

log on
(
	name='bbs_log',
	filename='D:\bbs_log.mdf',
	size=10mb,
	maxsize=100mb,
	filegrowth=10mb
)
go

use bbs

go

create table bbsUsers
(
	UID int primary key identity(1,1),
	uName varchar(10) unique not null,
    uSex varchar(2) check(uSex='男' or uSex='女')  not null,
	uAge int check(uAge>15 and uAge<60) not null,
	uPoint int check(uPoint>=0) not null
)

create table bbsSection
(
	sID  int primary key identity(1,1),
	sName  varchar(10) not null,
	sUid   int foreign key references bbsUsers(UID),
)

create table bbsTopic
(
	 tID  int primary key identity(1,1),
	 tUID  int references bbsUsers(UID),
	 tSID  int references bbsSection(sID),
	 tTitle  varchar(100) not null,
	 tMsg  text  not null,
	 Time  datetime,
	 tCount  int
)

create table bbsReply
(
	 rID  int primary key identity(1,1),
	 rUID  int references bbsUsers (UID),
	 rTID  int references bbsTopic(tID),
	 rMsg  text not null,
	 rTime  datetime 
)

--1.现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，记录值如下：
--  小雨点  女  20  0
--  逍遥    男  18  4
--	  七年级生  男  19  2
insert into bbsUsers values('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
select * from bbsUsers



--2.将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
select uName,uPoint into  bbsPoint from bbsUsers
insert into bbsPoint select uName,uPoint  from bbsUsers
select * from bbsPoint

--3.给论坛开设4个板块
--	  名称        版主名
--  技术交流    小雨点
--  读书世界    七年级生
--  生活百科     小雨点
--  八卦区       七年级生
insert into bbsSection values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
select * from bbsSection
select * from bbsPoint

--4.向主贴和回帖表中添加几条记录
	  
		--主贴：

		--发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
		--逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
		--七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
		--小雨点   生活百科    今年夏天最流行什么     ’有谁知道今年夏天最流行‘  2008-9-10  0
		--						什么呀？
insert into bbsTopic values(2,4,'范跑跑','谁是范跑跑',' 2008-7-8 ',1),(6,1,'NET','与JAVA的区别是什么呀？','2008-9-1',2)
insert into bbsTopic values(4,3,'今年夏天最流行什么 ','有谁知道今年夏天最流行',' 2008-9-10',0)

--insert into bbsTopic values (2,5,'范跑跑','谁是范跑跑','2008-7-8',1)
--insert into bbsTopic values (3,2,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,4,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)
select * from bbsTopic

 --回帖：
 --  分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定
	insert into bbsReply values(1,2,'我怎么知道？',getdate()),(1,2,'没什么区别',getdate()),(1,2,'今年夏天流行JK',getdate())

	select * from bbsReply

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）

delete from bbsReply where rTID=1
delete from bbsReply where rUID=2	
delete from bbsTopic where tUID=2
delete from bbsUser where UID=2

select * from bbsReply
select * from bbsReply where rTID=1
select * from bbsTopic where tUID=2
select * from bbsUser where UID=2

--	6.因为小雨点发帖较多，将其积分增加10分
update bbsUser set bbsPoint=bbsPoint+10 where UID=1

--	8.因回帖积累太多，现需要将所有的回帖删除

delete from bbsReply
truncate table bbsReply
drop table bbsReply




--  1.在主贴表中统计每个版块的发帖总数
select tSID, count(*) from bbsTopic group by tSID
--2.在回帖表中统计每个主贴的回帖总数量
select rTID, count(*) from bbsReply group by rTID
--3.在主贴表中统计每个用户的发的主帖的总数
select tID ,count(*) from bbsTopic group by tID
--4.在主贴表中统计每个用户发的主贴的回复数量总和
select tID, count(*) from bbsTopic group by tID
--5.在主贴表中查询 每个版块的主贴 的平均回复数量 大于3的版块的平均回复数量
select tSID,avg(tCount) from bbsTopic group by tSID having avg(tCount)>3
--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
--select max(uPoint) from bbsUsers
select UName,USex,Upoint from bbsUsers where uPoint=(select max(uPoint) from bbsUsers)
--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select * from bbsTopic where tTitle='快乐'or tMsg='快乐'
--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select * from  bbsUsers where uAge>15 and uAge<20 and Upoint>10
--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUsers where uName like '小_大%'
--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
不会
--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select tTitle,tID,tCount from bbsTopic group by tTitle 