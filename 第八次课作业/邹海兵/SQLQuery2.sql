use master
go

create database ui
go

use ui
go
create table orders
(
	orderid int primary key identity(1,1),
	orderDate datetime
)

create table orderItem
(
	itemID int primary key identity(1,1),
	orderid int references orders(orderid),
	itemType varchar(10),
	itemName varchar(20),
	theNumber int,
	theMoney decimal(1000,2)
)
insert into orders values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
insert into orderItem values(1,'文具','笔','72','2'),(1,'文具','尺','10','1'),(1,'体育用品','篮球','1','56'),(2,'文具','笔','36','2'),
(2,'文具','固体胶','20','3'),(2,'日常用品','透明胶','2','1'),(2,'体育用品','羽毛球','20','3'),(3,'文具','订书机','20','3'),(3,'文具','订书机','10','3'),
(3,'文具','裁纸刀','5','5'),(4,'文具','笔','20','2'),(4,'文具','信纸','50','1'),(4,'日常用品','毛巾','4','5'),(4,'日常用品','透明球','30','1'),
(4,'体育用品','羽毛球','20','3')

select * from orders
select * from orderItem

--查询所有订单订购的所有物品数量总和
select sum(theNumber) 订购总和 from orderItem

--查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select sum(theNumber) 数量,avg(theMoney) 单价 from orderItem group by itemID having itemID<3 and avg(theMoney)<10

--查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select count(itemID) 数量,avg(theMoney) 单价 from orderItem group by theMoney,theNumber having theMoney<10 and theNumber>50

--查询每种类别的产品分别订购了几次，例如：
--					文具      9
--                    体育用品  3
--                    日常用品  3
select itemType,COUNT(itemType) 订购次数 from orderItem group by itemType

--查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select sum(theNumber) 订单数量,AVG(theMoney) 平均单价 from orderItem group by itemType having sum(theNumber)>100

--查询每种产品的订购次数，订购总数量和订购的平均单价，例如：

--  产品名称   订购次数  总数量   平均单价 
--    笔           3       120       2
select count(orderId) 定购次数,sum(theNumber) 订单数量,AVG(theMoney) 平均单价 from orderItem group by itemName


