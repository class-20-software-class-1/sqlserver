use master
go
create database student
on
(
    name='student',
	filename='D:\student.mdf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)

log on
(
    name='student_log',
	filename='D:\student_log.ldf',
	size=5MB,
	maxsize=50MB,
	filegrowth=10%
)
go

use student
go

create table Classinformation
(
    classid int primary key identity(1,1),
	classname varchar(20) not null unique, 
	Openingtime date not null,
	Classdescription nvarchar(50)
)

create table Studendinfo
(
    Studentnumber int primary key identity(1,1),
	Studentname varchar(10) check(Studentname>=2) not null unique,
	sex varchar(2) check(sex='��' or sex='Ů') default('��'),
	Age int check(Age>=15 and Age<=40) not null,
	Homeaddress varchar(10) default('�����人'),
	Classnumber int
)


create table Courseinfor
(
    number int primary key identity(1,1),
	Coursename varchar(10) not null unique,
	CourseDescription nvarchar(50)
)


create table Achievementinfo
(
    Gradenumber int primary key identity(1,1),
	Studentnumber int references Studendinfo(Studentnumber),
	Coursenumber int references Courseinfor(number),
	achievement int check(achievement>=0 and achievement<=100)
)