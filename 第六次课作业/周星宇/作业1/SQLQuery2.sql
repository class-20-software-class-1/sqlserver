use master
go
create database company
on
(
    name='company',
	filename='D:\company.mdf',
	size=5MB,
    maxsize=50MB,
    filegrowth=10%
)
log on
(
    name='company_log',
	filename='D:\company_log.ldf',
	size=5MB,
    maxsize=50MB,
	filegrowth=10%
)
go

use company
go

create table sectionInfo
(
    sectionID  int primary key identity(1,1),
	sectionName  varchar(10) not null
)

create table userInfo
(
    userNo  int primary key identity(1,1),
	userName varchar(10) unique not null check(userName>4),
	userSex varchar(2) not null check(userSex='��' or userSex='Ů'),
	userAge int not null check(userAge>=1 and userAge<=100),
	userAddress varchar(50) default('����'),
	userSection int references sectionInfo(sectionID)
)

create table workInfo
(    
    workId int primary key identity(1,1) not null,
	userId  int references workInfo(workId),
	workTime datetime not null,
    workDescription  varchar(40) check(workDescription='�ٵ�' or workDescription='����' or workDescription='����'or workDescription='����' or workDescription='�¼�')
)

