use master
go
create database BBS02
on
(
 name='BBS02',
 filename='D:\BBS02.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='BBS02_log',
 filename='D:\BBS02_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use BBS02
go

create table bbsUsers
(
	bbsUID int identity(1,1),
	uName varchar(10) not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null
)

create table bbsSection
(
	sID int identity(1,1),
	sName  varchar(10) not null,
	sUid int
)

create table bbsTopic
(
	tID  int primary key identity(1,1),
	 tUID  int references bbsUsers(bbsUID),
	 tSID int references bbsSection(sID),
	  tTitle  varchar(100) not null,
	  tMsg  text not null,
	  tTime  datetime,
	  tCount  int
)

create table bbsReply
(
	rID int primary key identity(1,1),
	 rUID int references bbsUsers(bbsUID),
	 rTID int references bbsTopic(tID),
	 rMsg  text not null,
	 rTime  datetime 
)

alter table bbsUsers add constraint PK_bbsUsers_bbsUID primary key(bbsUID)
alter table bbsUsers add constraint UQ_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>= 0)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsUsers_bbsUID foreign key(sUid) references bbsUsers(bbsUID) 

select * from bbsReply

insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),
('逍遥','男',18,4),
('七年级生','男',19,2)

select * from bbsUsers

select uName,uPoint into bbsPoint from bbsUsers

select * from bbsPoint

insert into bbsSection(sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

select * from bbsSection

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2,4,'范跑跑','谁是范跑跑','2008-07-08',1),
(3,1,'.NET','与JAVA的区别是什么呀?','2008-09-01',2),
(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀?','2008-09-10',0)

insert into bbsReply(rUID,rTID,rMsg,rTime) values(1,1,'不知道','2008-07-09'),
(1,2,'不知道','2008-09-09'),
(2,2,'不知道','2008-10-09')

select * from bbsReply
select * from bbsTopic
select * from bbsSection

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）

delete from bbsReply where rUID=2
delete from bbsReply where rTID=1
delete from bbsTopic where tUID=2
delete from bbsUsers where bbsUID=2

update bbsUsers set uPoint=10 where bbsUID=1

select uPoint from bbsUsers
--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
delete from bbsTopic where tSID=3
delete from bbsSection where sID=3

truncate table bbsReply


select * from bbsReply
alter table bbsTopic drop column tCount

alter table bbsUsers add telephone char(10) UQ_

alter table bbsReply alter column rMsg varchar(200)

alter table bbsUsers drop constraint CK_bbsUsers_uPoint

update bbsUsers set uName='小雪' where bbsUID=1

update bbsUsers set uPoint=uPoint+100



select * from bbsUsers

select * into bbsUsers2 from bbsUsers

drop table bbsUsers2
truncate table bbsUsers2




 