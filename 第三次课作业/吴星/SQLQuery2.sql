Drop database Student
create database Student 
on 
(
	Name='Student',
	Filename='D:\SQL\新建文件夹\MSSQL12.MSSQLSERVER\MSSQL\DATA/\Student.mdf',
	Size = 5MB ,
	Maxsize= 100MB,
	Filegrowth= 10%
)
log on
(
	Name='Student_log',
	Filename='D:\SQL\新建文件夹\MSSQL12.MSSQLSERVER\MSSQL\DATA/\Student_log.ldf',
	Size = 5mb ,
	Filegrowth= 1MB
)
use Student
create table Class
(
	ClassId int primary key identity(1,1),
	ClassName nvarchar(20) unique not null,
)
create table Student
(
	 stuId int primary key identity(1,1),
	 Stuname nvarchar(20) not null,
	 Stusex nvarchar(1) check(StuSex in('男','女')),
	 StuBirthday date,
	 StuPhone nvarchar(11) unique not null,
	 ClassId int not null,
)
	alter table Student add StuAddress nvarchar(20)
	 alter table Student add constraint Fk_SC foreign key(ClassId) references Class(ClassId)

create table Course 
(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCredit int default('1') check(CourseCredit between 1 and 5)
)
drop table Score
create table Score
(
	ScoreId int  identity(1,1), 
	StuId int not null ,
	CourseId int not null,
	Score decimal(5,2) unique not null
)

alter table Score add constraint pk_ScoreId primary key (ScoeId)
 --alter table 表名 add constraint pk_ScoreId(约束名) primary key（类型） (ScoeId)（列）

alter table Score add constraint Fk_SS foreign key(StuId) references Student(stuId)

 use Student
 alter table Score add constraint Fk_SC foreign key(CourseId) references Course(CourseId)
 --alter table 表名drop constraint 约束名 删除表的约束
 alter table Student drop constraint Fk_SS