use master
go

create database student
on
(
     name='student',
	 filename='E:\新建文件夹\作业\SQL\bbs.mdf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)

log on
(
     name='student_log',
	 filename='E:\新建文件夹\作业\SQL\bbs_log.ldf',
	 size=5MB,
	 maxsize=50MB,
	 filegrowth=10%
)
go



use master 
go

create table studentinfo
(
    stuNO int primary key identity(1,1) not null,
    stuName varchar(10) unique not null,
	stuAge int,
	stuAddress nvarchar(20),
	stuSeat int not null,
	stuSex varchar(2) check(stuSex='0' or stuSex='1') not null
)



use master 
go

create table achievement
(
     examNO int primary key identity(1,1) not null,
	 stuNO int references studentinfo(stuNO),
	 writtenExam int not null,
	 labExam int not null
)



insert into studentinfo(stuNO,stuName,stuAge,stuAddress,stuSeat,stuSex)
select's2501''张秋利','20','美国硅谷','1','1' union
select's2502''李斯文','18','湖北武汉','2','0' union
select's2503''马文才','22','湖南长沙','3','1' union
select's2504''欧阳俊雄','21','湖北武汉','4','0' union
select's2505''梅超风','20','湖北武汉','5','1' union
select's2506''陈旋风','19','美国硅谷','6','1' union
select's2507''梅超风','20','美国硅谷','7','0'
go



select * from studentinfo

insert into achievement
select 's2501', 50, 70 union
select 's2502', 60, 65 union
select 's2503', 86, 85 union
select 's2504', 40, 80 union
select 's2505', 70, 90 union
select 's2506', 85, 90

go



select stuNO "学号", stuName "姓名", stuAge "年龄", stuAddress "地址", stuSeat "座号", stuSex "性别" from studentinfo
select stuName, stuAge, stuAddress from studentinfo
select stuNO "学号", writtenExam "笔试", labExam "机试" from achievement
select stuNO, stuName, stuAddress, stuName+'@'+stuAddress "邮箱" from studentinfo
select stuNO "学号", writtenExam "笔试", labExam "机试", labExam+writtenExam "总分"  from achievement
select distinct stuAddress from studentinfo
select distinct stuAge "所有年龄" from studentinfo
select top 3 * from studentinfo
select top 4 stuName, stuSeat from studentinfo
select top 50 percent * from studentinfo
select * from studentinfo where stuAddress = '湖北武汉' and stuAge = 20
select * from studentinfo "S"

	join achievement "E"

    on "S".stuNO = "E".stuNO
    where labExam between 60 and 80
    order by labExam desc

 

select * from studentinfo
    where stuAddress = '湖北武汉' or stuAddress = '湖南长沙'
select * from studentinfo where stuAddress in ('湖北武汉','湖南长沙')
select * from achievement

	join studentinfo
    on achievement.stuNO = studentinfo.stuNO
    where labExam not between 70 and 90

select * from studentinfo where stuName is null or stuName = ''
select * from studentinfo where stuName is not null or stuName <> ''
select * from studentinfo where stuName like '张%'
select * from studentinfo where stuName like '__俊%'
select * from studentinfo
    order by stuAge desc
select * from achievement
    where writtenExam not in 
(
    select distinct "E1".writtenExam from achievement "E1"

			join achievement "E2"
			on "E1".writtenExam < "E2".writtenExam

)



select top 1 * from achievement
      order by labExam asc
