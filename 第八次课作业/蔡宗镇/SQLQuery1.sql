use master 
go
create database orderr
on
(
	name='orderr',
	filename= 'D:\orderr.mdf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
log on
(
	name='orderr_log',
	filename= 'D:\orderr_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=15%
)
go
use orderr
go
create table orders
(
	orderId int primary key identity,
	orderDate datetime  default(getdate())
)
create table orderItem
(
	ItemiD int primary key identity,
	orderId int references orders(orderId) ,
	itemType nvarchar(10),
	itemName nvarchar(10),
	theNumber  int ,
	theMoney money
)
insert into orders values(default),(default),(default),(default)
select * from orders
insert into orderItem  values ('1','文具','笔','72','2'),('1','文具','尺','10','1'),('1','体育用品','蓝球','1','56'),('2','文具','笔','36','2'),('2','文具','固体胶','20','3'),
('2','日常用品','透明胶','2','1'),('2','体育用品','羽毛球','20','3'),('3','文具','订书机','20','3'),('3','文具','订书针','10','3'),('3','文具','裁纸刀','5','5'),
('4','文具','笔','20','2'),('4','文具','信纸','50','1'),('4','日常用品','毛巾','4','5'),('4','日常用品','透明胶','30','1'),('4','体育用品','羽毛球','20','3')
select * from orderItem

--1.查询所有订单订购的所有物品数量总和
select  sum(theNumber) 物品数量总和  from orderItem 

--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select orderId 订单编号, sum(theNumber) 物品数量总和 , avg(theMoney) 平均单价 from orderItem  where orderId<3 group by orderId having avg(theMoney) <10

--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderId 订单编号, sum(theNumber) 物品数量总和 , avg(theMoney) 平均单价 from orderItem group by orderId having avg(theMoney) <10 and sum(theNumber)>50

-- 4.查询每种类别的产品分别订购了几次，
select itemType 类别, count(orderId) 次数 from orderItem group by itemType

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType 类别, sum(theNumber) 物品数量总和 ,avg(theMoney) 平均单价  from orderItem group by itemType having  sum(theNumber)>100

--6.查询每种产品的订购次数，订购总数量和订购的平均单价
select itemName 产品 ,  count(orderId) 次数, sum(theNumber) 物品数量总和  ,avg(theMoney) 平均单价  from orderItem group by itemName 

use Text
go
select StuProvince 省份,AVG(StuAge)平均年龄 from dbo.StuInfo group by StuProvince
select StuSex 性别,sum(StuAge)年龄总和 from dbo.StuInfo group by StuSex
select StuProvince 省份,AVG(StuAge)平均年龄,sum(StuAge)年龄总和  from dbo.StuInfo group by StuProvince














