use master 
go 
create database bank1
on
( name=' bank1',
  filename='D:\bank\ bank1.mdf',
  size=5mb,
  maxsize=5000mb,
  filegrowth=15%

)
  log on
( name=' bank1_log',
  filename='D:\bank\bank1_log.ldf',
  size=5mb,
  maxsize=5000mb,
  filegrowth=15%

)
go
use bank1
go 
create table userInfo 
(	customerID int primary key identity(1,1),
	customerName nvarchar(5)  not null,
    PID varchar(18) unique check(len(PID)=18 or len(PID)=15),
	telephone char(13) check(telephone like '____-________' or len(telephone)=13),   
	address1     nvarchar(200) 
)

create table cardInfo
(	cardID      char(20)     not null primary key  check(substring(cardID,1,9)='1010 3576' and len(cardID)=18),
	curType     nvarchar(10) not null default('RMB'),
	savingType  nvarchar(4),
	openDate    datetime  not null  default(getdate()),
	balance     bigint    not null  check(balance>1), 
	pass        int       not null  default('888888') check(len(pass)=6),
	IsReportLoss nchar(1) not null  default('否'),
	customerID  int  references userInfo(customerID) not null

)
create table transInfo 
(	transId  int primary key identity(1,1),
	transDate datetime not null default(getdate()),
	cardID    char(20)   references cardInfo(cardID)  not null,
	transType nchar(2)  not null check(transType='存入'or transType='支取' ),
	transMoney bigint  not null check(transMoney>0),
	remark      text  
)

select*from userInfo
select*from cardInfo
select*from transInfo 

  insert into userInfo  values('孙悟空',123456789012345,'0716-78989783','北京海淀 '),
                              ('沙和尚',421345678912345678,'478-044223333','湖南'),
							  ('唐僧',321245678912345678,'0478-44443333','武汉')

  insert into cardInfo values( '1010 3576 1234 567',  default ,'活期',default,1000,default,'是',4),
                             ( '1010 3576 1212 117',  default ,'活期',default,3,default,'是',5),
							 ( '1010 3576 1212 113',  default ,'活期',default,5,default,'是',6)


update cardInfo set pass=611234 where customerID=4


   select*from userInfo where customerName='孙悟空'
   select cardID  from cardInfo where customerID in ( select customerID from userInfo where customerName='孙悟空')
   
   insert into transInfo  values(default,'1010 3576 1234 567','支取',200,'在今天花了200元')
   update  cardInfo set balance=balance-200 where customerID in(select customerID from userInfo where customerName='孙悟空')
   
   select*from userInfo where customerName='沙和尚'
   select cardID  from cardInfo where customerID in ( select customerID from userInfo where customerName='沙和尚')
   insert into transInfo  values(default,'1010 3576 1212 117','存入',300,'在今天存了300元')
   update  cardInfo set balance=balance+300 where customerID in(select customerID from userInfo where customerName='沙和尚')

    update  cardInfo set IsReportLoss='是' where customerID=( select customerID  from userInfo where customerName='唐僧') 

select*from cardInfo  where  openDate   between '2021-03-18'and '2021-03-20'

select*from transInfo where transMoney=(select max(transMoney) from transInfo )

select sum(transMoney) as 总交易金额 from transInfo 