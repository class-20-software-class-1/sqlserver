use master
create database bbs
on
(
	name='bbs',
	filename='D:\test\bbs.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\test\bbs_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%
)
go
use bbs
go
create table bbsUsers
(
	uID int identity(1,1) not null,
	uName varchar(10)  not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null
)

create table bbsSection
(
	sID  int identity(1,1) not null , 
	sName  varchar(10) not null,
	sUid   int

)
--添加约束
-- alter table 表名 add constraint 约束名  约束类型 
alter table bbsUsers add constraint PK_bbsUser_uID primary key(uID)
alter table bbsUsers add constraint UK_bbsUser_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
alter table bbsUsers add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUser_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(uID)


create table bbsTopic
(
	tID int primary key identity(1,1),
	tUID int references bbsUsers(uID),
	tSID int references bbsSection(sID),
	tTitle varchar(100) not null,
	tMsg text not null,
	tTime datetime,
	tCount int
)
create table bbsReply
(
	 rID  int identity(1,1) primary key,
	 rUID  int references  bbsUsers(uID),
	 rTID  int references bbsTopic(tID),
	 rMsg text not null,
	 rTime datetime
)

insert into bbsUsers(uName,uSex,uAge,uPoint) values ('小雨点','女','20','0')
,('逍遥','男','18','4'),('七年级生','男','19','2')


insert into bbsSection (sName,sUid) values ('技术交流',1),
('读书世界',3),('生活百科',1),('八卦区',3)

insert into bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount) values ('2','4','范跑跑','谁是范跑跑','2008-7-8','1'),
('3','1','.NET','谁是范跑跑','2008-9-1','2'),('1','3','.今年夏天最流行什么呀?','有谁知道今年夏天最流行','2008-9-10','0')

insert into bbsReply (rUID,rMsg,rTime) values ('1','范跑跑是赵俊小名','2021-1-1'),
('2','名字不一样','2021-1-2'),('3','EMO','2021-1-3')

update bbsUsers set uPoint=uPoint+15 where UID=1
update bbsTopic set tCount=tCount+10 where tUID=1
update bbsTopic set tTime='2011-11-30' where tUID=2
update bbsTopic set tCount=tCount+10 where tUID=2
update bbsUsers set uName='小雨大' where uID=1
update bbsTopic set tTitle='快乐' where tID=1

select * from bbsReply
select * from bbsTopic
select * from bbsUsers

--在主贴表中 统计 每个版块 的 发帖总数
select tSID, COUNT(*)发帖总数 from bbsTopic group by tSID

--在回帖表中 统计 每个主贴的 回帖总数量
select rTID, COUNT(*)回帖总数量 from bbsReply group by rTID

--在主贴表中 统计 每个用户的  发的主帖的总数
select tUID,COUNT(*)发的主帖的总数 from bbsTopic group by tUID

--在主贴表中 统计 每个用户发的主贴的 回复数量总和
select tUID,SUM(tCount)回复数量总和 from bbsTopic group by tUID

--在主贴表中 查询 每个版块的主贴的 平均回复数量 大于3的版块的平均回复数量
select tSID,AVG(tCount)平均回复数量 from bbsTopic group by tSID HAVING AVG(tCount)>3

--在用户信息表中 查询 出积分最高的用户的用户名 ，性别，年龄和积分
select top 1 * from bbsUsers order by uPoint desc

--在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select * from bbsTopic where tTitle like'%快乐%'or tMsg like'%快乐%'

--在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select UName 名字,USex 性别,uAge 年龄,Upoint 积分 from bbsUsers where UPoint=(select MAX(UPoint)最高积分 from BbsUsers)

--在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUsers where uName like'小_大'

--在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，
--并且为列取上对应的中文列名
select tTitle 帖子标题,tMsg 帖子内容 from bbsTopic where tTime>'2008-9-10 12:00:00' and tCount>10

--.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select  tUID,tCount from bbsTopic where tTitle like'!'