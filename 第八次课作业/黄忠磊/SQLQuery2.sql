use master
go

create database AAA
on
(
	name=AAA,
	filename='D:\AAA.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=Studen_log,
	filename='D:\AAA_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
go

use AAA
go	
--订单表（orders）列为：订单编号（orderId 主键）  订购日期（orderDate）

--订购项目表（orderItem），列为：
--项目编号（ItemiD）订单编号（orderId）产品类别（itemType）
--产品名称（itemName） 订购数量（theNumber）   订购单价（theMoney）
create table orders
(
orderId INT primary key identity(1,1),

orderDate datetime ,

)
create table orderItem
(
ItemiD INT primary key identity(1,1),
orderId int not null,
itemType varchar(10) not null,
itemName varchar(10)not null,
theNumber int ,
theMoney money not null
)
insert into orders values('2008-01-11'),('2008-02-11'),('2008-03-11'),('2008-04-11')
select * from orderItem
insert into orderItem values (1,'文具','笔',72,2),(2,'文具','尺',10,1),(1,'体育用品','篮球',1,56),(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),(2,'日常用品','透明胶',2,1),(2,'体育用品','羽毛球',20,3),(3,'文具','订书机',20,3),(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),(4,'文具','笔',20,2),(4,'文具','信纸',50,1),(4,'日常用品','毛巾',4,5),(4,'日常用品','透明胶',30,1),(4,'体育用品','',20,3)

--1.查询所有订单订购的所有物品数量总和

select sum(theNumber)数量总和 from orderItem

--2.查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价

  select orderId,sum(theNumber) 物品的数量和,avg (theMoney)平均单价 from orderItem where orderId<3  group by orderId
  having  avg(theMoney)<10

--3.查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
 select orderId, sum(theNumber) 物品的数量和,avg (theMoney) 平均单价 from orderItem  group by  orderId having  sum(theNumber)>50 and avg(theMoney)<10

--4.查询每种类别的产品分别订购了几次，例如：
--					文具      9
--                    体育用品  3
--                    日常用品  3

select itemType ,count (itemType)产品类别  from orderItem  group by itemType

--5.查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select  itemType 产品类别 , AVG(theMoney) 平均单价,sum(theNumber) from orderItem group by itemType having sum(theNumber)>100

--6.查询每种产品的订购次数，订购总数量和订购的平均单价，例如：

--  产品名称   订购次数  总数量   平均单价 
--    笔           3       120       2

select itemName 产品名,count(itemName) 订购次数, sum(theNumber)总数量 ,avg(theMoney)平均单价 from orderItem group by  itemName