
	use master
	go

	create database bbs
	on
	(
		name='bbs',
		filename='D:\DATA\bbs.mdf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	log on
	(
		name='bbs_log',
		filename='D:\DATA\bbs_log.ldf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	go

	use bbs
	go

	create table bbsUser
	(
		UID int identity,
		uName varchar(10) not null,
		uSex  varchar(2) not null,
		uAge  int not null,
		uPoint  int not null
	)

	alter table bbsUser add constraint PK_bbsUser_UID primary key(UID)
	alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex in('男','女'))
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge between 15 and 60)
	alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)


	create table bbsTopic
	(
		tID  int primary key identity,
		tUID  int references bbsUser(UID),
		tSID  int references bbsSection(sID),
		tTitle  varchar(100) not null,
		tMsg  text not null,
		tTime  datetime,
		tCount  int
	)

	create table bbsReply
	(
		rID  int primary key identity,
		rUID  int references bbsUser(UID),
		rTID  int references bbsTopic(tID),
		rMsg  text not null,
		rTime  datetime 
	)
	
	create table bbsSection
	(
		sID  int identity,
		sName  varchar(10) not null,
		sUid   int 
	)
	alter table bbsSection add constraint PK_bbsSection_sID primary key(sID) 
	alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(UID)

	insert into bbsUser values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
	insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
	insert into bbsTopic values (2,4,'范跑跑','谁是范跑跑','2008-7-8',1)
	insert into bbsTopic values (3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)


	
	--要求：
	--1.删除字段：删除主贴表的“回帖数量”字段
	alter table bbsTopic drop column tCount 
	--2、新增字段：用户表新增“手机号码”字段，并为该字段添加唯一约束，且长度为11，必填
	alter table bbsUser add Telephone varchar(20) unique check(len (Telephone) =11) not null
	--3、修改字段：修改回帖表的“回帖消息”字段，数据类型改成varchar(200)
	alter table bbsReply alter column rMsg varchar(200)
	--5、修改数据：修改用户“小雨点”的名字改成“小雪”，将所有用户的积分都增加100
	update bbsUser set uPoint= uPoint+100 
	update bbsUser set  uName = '小雪' where UID = 1
	--6、复制用户表的数据到新表bbsUser2
	select * into bbsUser2  from bbsUser 
	--7、删除bbsUser2的所有数据，写出两种方式
	delete from bbsUser2 
	truncate table bbsUser2 

	select * from bbsUser
	select * from bbsSection
	select * from bbsTopic
	 


  