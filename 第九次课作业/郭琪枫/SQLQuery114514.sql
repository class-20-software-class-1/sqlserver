use master
go
create database BBS02
on
(
 name='BBS02',
 filename='D:\BBS02.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='BBS02_log',
 filename='D:\BBS02_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use BBS02
go

create table bbsUsers
(
	bbsUID int identity(1,1),
	uName varchar(10) not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null
)

create table bbsSection
(
	sID int identity(1,1),
	sName  varchar(10) not null,
	sUid int
)
--主贴表（bbsTopic）
--主贴编号  tID  int 主键primary key  标识列，identity
--发帖人编号  tUID  int 外键 renferences 表（列） 引用用户信息表的用户编号
--版块编号    tSID  int 外键 primary key 引用版块表的版块编号    （标明该贴子属于哪个版块）
--贴子的标题  tTitle  varchar(100) 不能为空
--帖子的内容  tMsg  text  不能为空
--发帖时间    tTime  datetime  
--回复数量    tCount  int
create table bbsTopic
(
	tID  int primary key identity(1,1),
	 tUID  int references bbsUsers(bbsUID),
	 tSID int references bbsSection(sID),
	  tTitle  varchar(100) not null,
	  tMsg  text not null,
	  tTime  datetime,
	  tCount  int
)
--回帖表（bbsReply）
--回贴编号  rID  int 主键primary key   标识列，identity
--回帖人编号  rUID  int 外键 renferences 引用用户信息表的用户编号
--对应主贴编号    rTID  int 外键 renferences 引用主贴表的主贴编号（标明该贴子属于哪个主贴）
--回帖的内容  rMsg  text  不能为空
--回帖时间    rTime  datetime 
create table bbsReply
(
	rID int primary key identity(1,1),
	 rUID int references bbsUsers(bbsUID),
	 rTID int references bbsTopic(tID),
	 rMsg  text not null,
	 rTime  datetime 
)

alter table bbsUsers add constraint PK_bbsUsers_bbsUID primary key(bbsUID)
alter table bbsUsers add constraint UQ_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in('男','女'))
alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>= 0)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsUsers_bbsUID foreign key(sUid) references bbsUsers(bbsUID) 

select * from bbsReply

insert into bbsUsers(uName,uSex,uAge,uPoint) values('小雨点','女',20,0),
('逍遥','男',18,4),
('七年级生','男',19,2)

select * from bbsUsers

select uName,uPoint into bbsPoint from bbsUsers

select * from bbsPoint

insert into bbsSection(sName,sUid) values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

select * from bbsSection

insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values(2,4,'范跑跑','谁是范跑跑','2008-07-08',1),
(3,1,'.NET','与JAVA的区别是什么呀?','2008-09-01',2),
(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀?','2008-09-10',0)

insert into bbsReply(rUID,rTID,rMsg,rTime) values(1,1,'不知道','2008-07-09'),
(1,2,'不知道','2008-09-09'),
(2,2,'不知道','2008-10-09')

select * from bbsReply
select * from bbsTopic
select * from bbsSection

--	5.因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）

delete from bbsReply where rUID=2
delete from bbsReply where rTID=1
delete from bbsTopic where tUID=2
delete from bbsUsers where bbsUID=2

update bbsUsers set uPoint=10 where bbsUID=1

select uPoint from bbsUsers
--	7.因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
delete from bbsTopic where tSID=3
delete from bbsSection where sID=3

truncate table bbsReply


select * from bbsReply

alter table bbsTopic drop column tCount

alter table bbsUsers add telephone char(10) 

alter table bbsReply alter column rMsg varchar(200)

alter table bbsUsers drop constraint CK_bbsUsers_uPoint

update bbsUsers set uName='小雪' where bbsUID=1
update bbsUsers set uPoint=uPoint+100


select bbsUID,uName,uSex,uAge,uPoint into bbsUsers2 from bbsUsers

drop table bbsUsers2
truncate table bbsUsers2



--1.在主贴表中统计每个版块的发帖总数

select tSID,COUNT(tID) 发帖总数 from bbsTopic group by tSID

--2.在回帖表中统计每个主贴的回帖总数量

select rTID,COUNT(rID) 回帖总数 from bbsReply group by rTID

--3.在主贴表中统计每个用户的发的主帖的总数

select tUID,COUNT(tID) 主帖的总数 from bbsTopic group by tUID

--4.在主贴表中统计每个用户发的主贴的回复数量总和

select tUID,SUM(tCount) 回复数量总和 from bbsTopic group by tUID

--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量

select tSID,AVG(tCount) 平均回复数量 from bbsTopic group by tSID HAVING AVG(tCount)>3

--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分

select  top  1  * from bbsUsers order by uPoint DESC

--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来

select * from bbsTopic where tTitle like '%快乐%' or tMsg like '%快乐%'

--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）

select * from bbsUsers where uAge between 15 and 20 and uPoint>10
select * from bbsUsers where uAge>=15 and uAge<=20 and uPoint>10

--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来

select * from bbsUsers where uName like '小_大'

--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名

select tTitle 帖子标题,tMsg 帖子内容 from bbsTopic where  tTime>'2008-9-10 12:00:00'  and tCount>10

--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来


select tUID,tCount from bbsTopic where tTitle like '%!'
 

update bbsTopic set tTitle='好!' 
update bbsTopic set tTime='2008-9-11 12:00:00' 
update bbsTopic set tTitle='好快乐!'
update bbsUsers set uName='小中大' where bbsUID=1








references column constraint alter truncate drop table