create database  Dfive
on
(
	name='Dfive',
	filename='D:\Dfive.mdf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
log on
(
	name='Dfive_log',
	filename='D:\Dfive_log.ldf',
	size=10,
	maxsize=100,
	filegrowth=10%
)
use  Dfive
go
create table StudenIfo
(
	stuNo int identity(2501,1) primary key,
	stuName nvarchar(20) not null unique, 
	stuAge int   null ,
	studdRess nvarchar(20) not null,
	stuSeat int  ,
	stuSext int check(stuSext in('1','0')) default('0')
)
insert into StudenIfo(stuName,stuAge,studdRess,stuSeat,stuSext ) values('张三','20','福建泉州','1','1'),('李四','18','湖北武汉','2','0'),('王五','22','湖南长沙','3','1'), 
('小明','21','湖北武汉','4','0'),('小李','20','湖北武汉','5','1'),('小许','20','福建泉州','6','1'),('小俊俊俊','20','福建泉州','7','0') 

create table Class
(
	examNo int identity(1,1) primary key,
	stuNo char(20) null,
	writtenExam int  ,
	labExam int 
)
insert into Class(stuNo,writtenExam,labExam) values ('1001','50','70'),('1002','60','65'),('1003','86','85'),('1004','40','80'),('1005','70','90'),('1006','85','90')

select stuNo 学号, stuName 姓名,stuAge 年龄, studdRess 地址,stuSeat 座位号,stuSext 性别 from StudenIfo 
select stuName 姓名,stuAge 年龄, studdRess 地址 from StudenIfo 
select stuNo as 学号, writtenExam as 笔试成绩,labExam as 机试成绩 from  Class 
select stuNo 学号, writtenExam 笔试成绩,labExam 机试成绩 from  Class 
select 学号=stuNo,笔试成绩=writtenExam ,机试成绩=labExam  from  Class 
select stuNo 学号, stuName 姓名, studdRess 地址,stuName+'@'+ studdRess as 邮箱 from StudenIfo
select stuNo as 学号, writtenExam as 笔试成绩,labExam as 机试成绩,labExam+ writtenExam 总分 from Class
select  studdRess from  StudenIfo
select distinct stuAge as 所有年龄 from StudenIfo
select * from StudenIfo where  stuSeat<=3 
select top 4 stuName 姓名, stuSeat 座位号 from StudenIfo 
select top 50 percent  * from StudenIfo  
select * from  StudenIfo  where studdRess =  '湖北武汉' and stuAge=20 
select * from Class where labExam like '[60-80]%'
select * from  StudenIfo where studdRess in('湖北武汉','湖南长沙')
select * from  StudenIfo where studdRess ='湖北武汉' or studdRess ='湖南长沙'

select * from Class where writtenExam like '[70-90]%' order by writtenExam ASC

select * from  StudenIfo where stuName like '张%'  
select * from  StudenIfo where studdRess like '湖%'  
select * from  StudenIfo where  stuName like '陈_'   
select * from  StudenIfo where stuName like '_俊%' 
select * from  StudenIfo order by   stuAge DESC 
select * from  StudenIfo order by   stuAge DESC ,  stuSeat ASC 
 select top 1 * from Class  order by  writtenExam DESC 
 select top 1 * from Class  order by  labExam ASC    