use master 
go
create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10,
	maxsize=200,
	filegrowth=20
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf ',
	size=10,
	maxsize=200,
	filegrowth=20
)
go
use bbs
  create table bbsUsers
 (
	UIDD  int identity(1,1) not null ,
	uName   nvarchar(10) not null,
	uSex   varchar(2) not null ,
	uAge  int not null,
	uPoint int not null 
 )
 alter table bbsUsers add constraint PK_bbsUsers_UIDD primary key(UIDD )
alter table  bbsUsers add constraint UK_bbsUsers_uName unique(uName)
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in('女','男'))
alter table bbsUsers add constraint CK_bbsUsers_uAge  check(uAge>=15 and uAge<=60)
alter table bbsUsers add constraint CK_bbsUsers_uPoint  check(uPoint>=0)
  create table bbsTopic
 (
	tID   int primary key identity(1,1),
	tUID   int references  bbsUsers(UIDD),
	tSID  int references  bbsSection(sIDD),
	tTitle    nvarchar(100) not null,
	tMsg    text not null,
	tTime   datetime,  
	tCount  int 
 )
 create table bbsReply
 (
	rID     int primary key identity(1,1),
	rUID     int references bbsUsers(UIDD) ,
	rTID     int references  bbsTopic(tID),
	rMsg      text not null,
	rTime     datetime,   
 )
  create table bbsSection
 (
	sIDD    int identity(1,1),
	sName  nvarchar(10),
	sUid  int  , 
 )
 alter table bbsSection add constraint PK_bbsSection_sIDD primary key(sIDD)
 alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(UIDD)
 
 insert into bbsUsers(uName,uSex,uAge,uPoint) values ('小雨点','女','20','0'),('逍遥 ','男','18','4'),('七年级生','男','19','2')

 create table bbsPoint
 (
	ID nvarchar(10) ,
	point int 
 )
 
 --
 insert into bbsPoint(ID,point) select uName,uPoint from bbsUsers


 --
 select uName,uPoint into bbsUsers2 from bbsUsers 

 select * from bbsUsers

 insert into bbsSection(sName,sUid) values ('技术交流  ','1'),('  读书世界 ','3'),('生活百科','1'),('八卦区 ','3')
 insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) values ('2','4','范跑跑  ','谁是范跑跑','2008-7-8 ','1'),
 ('3','1','.NET','与JAVA的区别是什么呀？','2008-9-1','2'),('1','3','今年夏天最流行什么','有谁知道今年夏天最流行什么呀？',' 2008-9-10','0')
 select * from bbsSection
 select * from bbsUsers
 select * from bbsReply
 select * from bbsTopic
 insert into bbsReply(rUID,rMsg, rTime ) values ('1','你是','2020.1.2'),('3','爱狗','2020.2.15'),('2','还是能','2020.5.25')

-- 1.在主贴表中统计每个版块的发帖总数
select  tSID 版块编号  ,count(tID) 发帖总数 from bbsTopic group by tSID

--2.在回帖表中统计每个主贴的回帖总数量
select rTID 主贴编号 , sum(rID) 回帖总数量  from bbsReply group by rTID
--3.在主贴表中统计每个用户的发的主帖的总数
select tUID  主贴编号, count(*) 主帖的总数  from bbsTopic group by tUID  
--4.在主贴表中统计每个用户发的主贴的回复数量总和
select tUID  用户编号  , sum(tCount  ) 回复数量总和  from bbsTopic group by tUID 
--5.在主贴表中查询每个版块的主贴的平均回复数量大于3的版块的平均回复数量
select tSID  版块编号, avg(tCount) 平均回复数量 from bbsTopic group by tSID having avg(tCount)>3
--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
select  top 1 * from bbsUsers  order by uPoint desc
--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select * from bbsTopic where tMsg like '%快乐%' or tTitle  like '%快乐%'
--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select  * from bbsUsers where uAge >=15 and uAge <=20 and  uPoint>2
select  * from bbsUsers where uAge between 15 and 20  and  uPoint>2
--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select  * from bbsUsers where uName like '小_大%'
--10.在主贴表（bbsTopic）中将在2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
select * from bbsTopic  where tTime  > '2008-9-10 12:00:00' and tCount  >10
--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select tUID ,tCount from bbsTopic where tMsg   like '%？'