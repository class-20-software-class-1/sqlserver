use master
go
if exists(select*from sys.databases where name='bbs')
drop database bbs
create database bbs
on
(
name='bbs',
filename='D:\bbs.mdf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
log on
(
name='bbs_log',
filename='D:\bbs_log.ldf',
size=5mb,
maxsize=100mb,
filegrowth=10%
)
go
use bbs
go
create table bbsUsers
(
UID int primary key identity(1,1) ,
uName varchar(10) not null,
uSex varchar(2) not null,
uAge int not null,
uPoint int not null
)
alter table bbsUsers add constraint PK_bbsUsers_UID primary key(UID)
create table bbsTopic
(
tID int primary key identity(1,1),
tUID int ,
tSID int ,
tTitle varchar(100) not null,
tMsg text not null,
tTime datetime,
tCount int 
)
create table bbsReply
(
rID int primary key identity(1,1),
rUID int references bbsUsers(UID),
rTID int references bbsTopic(tID),
rMsg text not null,
rTime datetime 
)
create table bbsSection
(
sID int identity(1,1),
sName varchar(10) not null,
sUid int 
)
alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName) 
alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex in ('男','女'))
alter table bbsUSers add constraint CK_bbsUsers_nSex check(uAge<=20 and uAge>=15)
alter table bbsUSers add constraint CK_bbsUSers_uPoint check(uPoint>=0)
alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsDection_sUid foreign key(sUid)references bbsUsers(UID)
insert into bbsUsers(uName,uSex,uAge,uPoint)values
( '小雨点','女',20, 0),
('逍遥' ,'男',18 ,4),
('七年级生','男',19 ,2)
--select 列名1，列名2 from 表名
select uName,uPoint into bbsPoint from bbsUsers
insert into bbsSection(sName)values
(' 技术交流'),('读书世界'),('生活百科'),('八卦区')
alter table bbsSection add bName nvarchar(20) not null
insert into bbsSection(sName,sUid) values
('技术交流',3),
('读书世界',1),
('生活百科',3),
('八卦区',1)
insert into bbsTopic(tUID,tSID,tTitle,tMsg, tTime, tCount)values
(2,1,'范跑跑 ','谁是范跑跑',2008-7-8,1),
(1,3,'.NET','与JAVA的区别是什么呀？',2008-9-1,2),
(3,4,'今年夏天最流行什么','有谁知道今年夏天最流行什么呀？',2008-9-10,0)
insert into bbsReply(rUID,rTID,rMsg,rTime)values
(1,'八卦区',1,'你猜',2008-7-8),
(2,'技术交流',2,'java更头秃',2008-7-8),
(3,'生活百科',3,'BF风',2008-7-8)
delete from bbsReply where rID=1
delete from bbsTopic where tID=1
insert into bbsUsers(UID,uPoint) values(3,10)
delete from bbsUsers where UID=2
delete from bbsSection where sID=2
delete from bbsReply

alter table bbsTopic drop column tCount --1、删除字段：删除主贴表的“回帖数量”字段
alter table bbsUsers add phonenum varchar(11) unique not null--2、新增字段：用户表新增“手机号码”字段，并为该字段添加唯一约束，且长度为11，必填
alter table bbsReply alter column rMsg varchar(200)--3、修改字段：修改回帖表的“回帖消息”字段，数据类型改成varchar(200)
alter table bbsUsers drop constraint CK_bbsUsers_uPoint--4、删除约束：删除用户表的“积分”字段的检查约束
update bbsUsers set uName='小雪'
update bbsUsers set uPoint=uPoint+1
--5、修改数据：修改用户“小雨点”的名字改成“小雪”，将所有用户的积分都增加100
select UID,uName,uSex,uAge,uPoint into bbsUser2 from bbsUsers--6、复制用户表的数据到新表bbsUser2
truncate table bbsUser2
delete from bbsUser2--7、删除bbsUser2的所有数据，写出两种方式的语句