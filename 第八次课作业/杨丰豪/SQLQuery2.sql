--建数据库
create database Dong
on
(
	name='Dong',
	filename='D:\test\Dong.mdf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
log on
(
	name='Dong_log',
	filename='D:\test\Dong_log.ldf',
	size=5mb,
	maxsize=50mb,
	filegrowth=10%

)
--使用创建出来的数据库
go
use Dong

go
create table orders
(
orderId int primary key identity(1,1),
orderDate datetime not null,
)
--在数据库创建表
go
create table orderItem
(
ItemiD int primary key identity(1,1),
orderId int references orders(orderId),
itemType varchar(20) not null,
itemName varchar(20) not null,
theNumber int not null,
theMoney money not null,
)
--测试查询 orders orderItem 表
go
select * from orders
select * from orderItem
--添加日期
go
insert into orders(orderDate) values('2008-01-12'),('2008-02-10'),('2008-02-15'),('2008-03-10')
--添加数据
go
insert into orderItem(orderId,itemType,itemName,theNumber,theMoney) values
(1,'文具','笔',72,2),
(1,'文具','尺',10,1),
(1,'体育用品','篮球',1,56),
(2,'文具','笔',36,2),
(2,'文具','固体胶',20,3),
(2,'日常用品','透明胶',2,1),
(2,'体育用品','羽毛球',20,3),
(3,'文具','订书机',20,3),
(3,'文具','订书针',10,3),
(3,'文具','裁纸刀',5,5),
(4,'文具','笔',20,2),
(4,'文具','信纸',50,1),
(4,'日常用品','毛巾',4,5),
(4,'日常用品','透明胶',30,1),
(4,'体育用品','羽毛球',20,3)

--查询所有订单订购的所有物品数量总和
select SUM(theNumber)物品数量总和 from orderItem

--查询订单编号小于3的，平均单价小于10 每个订单订购的所有物品的数量和以及平均单价
select orderId,sum(theNumber)物品总数量, AVG(theMoney)平均单价 from dbo.orderItem where orderId<3 group by orderId HAVING AVG(theMoney)<10

--查询平均单价小于10并且总数量大于 50 每个订单订购的所有物品数量和以及平均单价
select orderId,SUM(theNumber)物品总数量,AVG(theMoney)平均单价 from dbo.orderItem group by  orderId HAVING SUM(theNumber)>50 and AVG(theMoney)<10

--查询每种类别的产品分别订购了几次，例如：
--文具      9
--体育用品  3
--日常用品  3
select itemType,COUNT(itemType)订购次数  from dbo.orderItem group by itemType

--查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType,SUM(theNumber)订购总数量,AVG(theMoney)平均单价 from dbo.orderItem group by itemType HAVING SUM(theNumber)>100

--查询每种产品的订购次数，订购总数量和订购的平均单价，例如：
--  产品名称   订购次数  总数量   平均单价 
--    笔           3       120       2
select itemName 产品名称,COUNT(itemName)订购次数,SUM(theNumber)订购总量,AVG(theMoney)平均单价 from orderItem group by itemName