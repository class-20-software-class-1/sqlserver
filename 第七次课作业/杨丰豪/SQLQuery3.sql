create database bbs
on
(
	name='bbs',
	filename='D:\bank\bbs.mdf',
	size=20,
	maxsize=200,
	filegrowth=15%
)

log on
(
	name='bbs_log',
	filename='D:\bank\bbs.ldf',
	size=20,
	maxsize=200,
	filegrowth=15%
)

go
use bbs
go

create table userInfo
(
customerID int primary key identity(1,1),
customerName varchar(10) not null,
PID  varchar(20) check(len(PID)=15 or len(PID)=18) unique,
telephone varchar(13) check(len(telephone)=13 and telephone like '____-________'),
address text
)

create table cardInfo
(
cardID varchar(50) primary key check(cardID like '1010 3576 ____ ___') not null,
curType varchar(50) default('RMB') not null,
savingType varchar(20) check(savingType='活期' or savingType='定活两便' or savingType='定期'),
openDate datetime default(getdate()) not null,
balance int check(balance>=1) not null,
pass varchar(20) check(len(pass)=6) default('888888') not null,
IsReportLoss varchar(5) check(IsReportLoss in('是','否'))default('否') not null,
customerID int references userInfo(customerID) not null,
)

create table transInfo 
(
	transId int identity primary key,
	transDate datetime default(getdate())  not null,
	cardID varchar(50) references cardInfo(cardID),
	transType nvarchar(2) check(transType in('存入','支出')),
	transMoney money check(transMoney>0) not null,
	remark text 
)

go
select * from userInfo
select * from cardInfo
select * from transInfo 

go
insert into userInfo (customerName,PID,address,telephone) values('孙悟空',123456789012345,'北京海淀','0716-78989783')
insert into userInfo (customerName,PID,telephone) values('沙和尚',421345678912345678,'0478-44223333'),
('唐僧',321245678912345678,'0478-44443333')


insert into cardInfo (cardID,savingType,balance,customerID) values('1010 3576 1234 567','活期',1000,1)
insert into cardInfo (cardID,savingType,balance,customerID) values('1010 3576 1212 117','定期',1,2),('1010 3576 1212 113','定期',1,3)

update cardInfo set pass=611234 where cardID='1010 3576 1234 567'

insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1234 567','支出',200)

update cardInfo set balance=balance-200 where cardID='1010 3576 1234 567'

insert into transInfo(cardID,transType,transMoney) values ('1010 3576 1212 117','存入',300)

update cardInfo set balance=balance+300 where cardID='1010 3576 1212 117'

update cardInfo set IsReportLoss='是' where customerID=3

select * from cardInfo where openDate like '2021-03-[0-1]9'