use master
go

create database House
on
(
	name=House,
	filename='D:\zuoye\House.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name=House_log,
	filename='D:\zuoye\House_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)

use House
go

create table tblUser
(
	useld int identity(1,1) unique not null,
	userName varchar(10) unique not null check(username>2),
	userTel int check(userTel=11)
)

use House
go

create table tblHouseType
(
	typeId int identity(1,1) unique not null,
	typName varchar(10) unique not null check (typName='别墅' or typName='普通住宅' or typName='平房' or typName='地下室')
)

use House
go

create table tblQx
(
	qxId int identity(1,1) unique not null,
	qxName varchar(10) unique not null ,
)

use House
go

create table tblHouseInfo
(
	ld int identity(1,1) unique not null,
	housedesc varchar(100) unique not null,
	userId int,
	zi int not null,
	shi varchar(10) not null,
	ting varchar(10) not null,
	typeld int identity(1,1) unique not null,
	qxld int
)
