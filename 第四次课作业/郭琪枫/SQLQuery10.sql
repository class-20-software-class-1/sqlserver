use master
go
create database Students
on
(
 name='Students',
 filename='D:\Program Files\Stundents.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Students_log',
 filename='D:\Program Files\Stundents_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)

use Students
go

create table Class
(
	ClassID int primary key identity,
	ClassName nvarchar(20) unique not null,
)

create table Student
(
	StuID int primary key identity(1,1),
	ClassID int references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nchar(1) default('��') check(StuSex in('��','Ů')), 
	StuBirthday date,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200)
)

create table Course
(
	CourseID int primary key identity,
	CourseName nvarchar(50) unique not null,
	CourseCredit nvarchar(10) default(1) check(CourseCredit<=5 and CourseCredit>=1) not null,
	Coursetypes nvarchar(10) check(Coursetypes='רҵ��' or Coursetypes='������')
)

create table Score
(
	ScoreID int identity,
	StuID int references Student(StuID),
	CourseID  int references Course(CourseID),
	Score decimal(5,2) unique not null
)


insert into Class (ClassID,ClassName) values(1,'����һ��'),(2,'��������'),(3,'��������'),(4,'�����İ�'),(5,'�������'),(6,'��������'),(7,'�����߰�'),(8,'�����˰�'),(9,'�����Ű�'),(10,'����ʮ��')
update Class set ClassName='����ʮһ��' where ClassID=1
delete from Class where ClassID=10

insert into Student values(1,1,'����','��',1999-03-01,12345678901,'�����ϵ�'),
(2,1,'����','��',1999-03-02,12345678911,'�����ϵ�'),
(3,2,'������','Ů',1999-03-03,12345678921,'�����ϵ�'),
(4,2,'����','Ů',1999-03-04,12345678931,'�����ϵ�'),
(5,3,'����','��',1999-03-05,12345678941,'�����ϵ�'),
(6,3,'Ƥ��','Ů',1999-03-06,12345678951,'�����ϵ�'),
(7,4,'����','��',1999-03-07,12345678961,'�����ϵ�'),
(8,4,'������','��',1999-03-08,12345678971,'�����ϵ�'),
(9,5,'����','��',1999-03-09,12345678981,'�����ϵ�'),
(10,5,'���','Ů',1999-03-10,12345678991,'�����ϵ�'),
(11,6,'ɯ��','Ů',1999-03-11,12345678901,'�����ϵ�'),
(12,6,'��','��',1999-03-21,12345678902,'�����ϵ�'),
(13,7,'���','��',1999-03-12,12345678903,'�����ϵ�'),
(14,7,'�����','��',1999-03-13,12345678904,'�����ϵ�'),
(15,8,'�����','��',1999-03-14,12345678905,'�����ϵ�'),
(16,8,'ϣ˹','Ů',1999-03-15,12345678906,'�����ϵ�'),
(17,9,'���׶�','Ů',1999-03-16,12345678907,'�����ϵ�'),
(18,9,'������','��',1999-03-17,12345678908,'�����ϵ�'),
(19,10,'����','Ů',1999-03-18,12345678909,'�����ϵ�'),
(20,10,'������','��',1999-03-19,12345678890,'�����ϵ�')

alter table Student add CreateDate datetime default(getdate())
update Student set CreateDate=CreateDate datetime

insert into Course(CourseName) values('������'),('�����'),('��ѧ��'),('Ӣ���'),('���Ŀ�'),('��ѧ��')
select * from Course
update Course set CourseCredit=2 where CourseName='�����'


insert into Score values(1,11,111,80),
(2,22,222,81),
(3,33,333,82),
(4,44,444,83),
(5,55,555,84),
(6,66,666,85),
(7,77,777,86),
(8,88,888,87),
(9,99,999,88),
(10,45,185,89),
(11,47,857,90),
(12,46,181,91),
(13,85,191,92),
(14,75,911,93),
(15,36,811,94),
(16,95,221,95),
(17,98,211,96),
(18,56,985,97),
(19,69,753,98),
(20,89,159,99)
update Score set Score=100 where CourseID=333
delete from Score where StuID=1
delete from Score where CourseID=111
alter table Score add constraint DK_Score_Score check(Score<=100 and Score>=0)
alter table Score add constraint DK_Score_Score default(0) for Score