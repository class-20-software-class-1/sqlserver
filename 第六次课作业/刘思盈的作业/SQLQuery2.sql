use master
go
create database ATM
on
(
name='ATM',
filename='D:\bank\ATM.mdf',
size=5,
maxsize=100,
filegrowth=15%

)
log on
(
name='ATM_log',
filename='D:\bank\ATM_log.ldf',
size=5,
maxsize=100,
filegrowth=15%


)
go
use ATM
go
create table userInfo
(
customerID int primary key identity,
customerName nvarchar(10) not null,
PID varchar(20) unique check(len(PID)=15 or len(PID)<=18) not null,
telephone varchar(13)check(len(telephone)=13 or telephone like'____-________'),
address nvarchar(20)
)
create table cardInfo
(
cardID varchar(20) primary key check(cardID  like '1010 3576 ____ ___') not null ,
curType nvarchar(10) not null default('RMB'),
savingType nvarchar(8) check(savingType in('活期','定活两便','定期')),
openDate datetime default(GETDATE()) not null,
balance int check(balance>1),
pass int default(888888) not null check(len(pass)=6),
IsReportLoss  char(2) default('否') check(IsReportLoss='是' or IsReportLoss='否' ),
customerID int not null references userInfo(customerID)
)
create table transInfo
(
transId int primary key identity  ,
transDate datetime default(GETDATE()) NOT NULL,
cardID varchar(20) references cardInfo(cardID) not null,
transType nvarchar(5) check(transType in('存入','支取')) not null,
transMoney int not null check(transMoney>0),
remark text
)
insert into userInfo values('孙悟空','123456789012345','0716-78989783','北京海淀 '),('沙和尚','421345678912345678','0478-44223333',''),('唐僧','321245678912345678','0478-44443333','')
insert into cardInfo(balance,savingType,cardID,customerID) values(1000,'活期','1010 3576 1234 567',1),(1000,'定期','1010 3576 1212 117',2),(1000,'定期','1010 3576 1212 113',3)
update cardInfo set pass=611234 where customerID=1
insert into transInfo values('2002-8-07','1010 3576 1234 567','支取','200','餐费'),('2002-9-07','1010 3576 1212 117','存入','300','餐费')
update cardInfo set balance=balance-200 where customerID=1
update cardInfo set IsReportLoss='是' where customerID=3
select * from cardInfo where openDate>=2021-03-09 and openDate<=2021-03-19
