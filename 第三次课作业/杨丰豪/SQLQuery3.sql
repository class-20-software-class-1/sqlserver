use master
go

create database Student
on
(
name='student',
filename='D:\test\student.mdf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
log on
(
name='Student_log',
filename='D:\test\student_log.ldf',
size=5MB,
maxsize=50MB,
filegrowth=10%
)
go

use master
go

create table Class
(
ClassID int primary key identity(1,1),
ClassName nvarchar(20) unique not null
)
create table Student
(
StuID int primary key identity(1,1),
ClassID int references Class(classID),
StuName nvarchar(20) not null,
Stusex nvarchar(1) check(Stusex='��'or Stusex='Ů'),
StuBirthday date,
StuPhone nvarchar(11) unique
)
create table Course
(
CourseID int primary key identity(1,1),
CourseName nvarchar(50) unique not null,
CourseCredit int default(1) check(courseCredit>'1' or CourseCredit<'5')not null
)
create table Score
(
ScoreID int not null,
StuID int,
CourseID int,
Score decimal(5,2) unique not null
)
alter table Student add StuAddress nvarchar(200)
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
alter table Score add constraint Fk_Score_StuID foreign key(StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)
alter table Score add constraint UK_Score_Score unique(score)






