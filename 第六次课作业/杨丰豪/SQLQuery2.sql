create database company
on
(
name=company,
filename='D:\test\company.mdf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
log on
(
name=TestDB_log,
filename='D:\test\company_log.ldf',
size=10mb,
maxsize=50mb,
filegrowth=10%
)
go
use company
go
create table sectionInfo
(
sectionID int primary key identity(1,1),
sectionName  varchar(10) not null,
)
go
create table userInfo
(
userNo  int primary key identity(1,1),
userName  varchar(10) check(userName>4),
userSex   varchar(2) check(userSex='��'or userSex='Ů') not null,
userAge   int check(userAge>=1 and userAge<=100),
userAddress  varchar(50) default'����',
userSection  int,
)
go
create table workInfo
(
workId int primary key identity(1,1),
userId  int not null,
workTime datetime not null,
workDescription  varchar(40) check(workDescription='�ٵ�' or workDescription='����' or workDescription='����' or workDescription='����' or workDescription='�¼�')
)
alter table userInfo add constraint Pk_userInfo_userSection foreign key(userSection) references sectionInfo(sectionID)
alter table workInfo add constraint Uk_userId_userId foreign key(userId) references userInfo(userNo)