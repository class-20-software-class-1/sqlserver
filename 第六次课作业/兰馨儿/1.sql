--1-------------------------------------------------------------
use master
go

if exists(select*from sys.objects where name ='TestDB' )
	drop database TestDB
create database TestDB
on
(
name = 'TestDB',
filename = 'D:\DATA\TestDB.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)
log on
(
name = 'TestDB_log',
filename = 'D:\DATA\TestDB_log.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)

go
use  TestDB
create table TypeInfo
(
 TypeId int primary key identity(1,1),
 TypeName varchar(10) not null,
 )

 create table LoginInfo
(
LoginId int primary key identity(1,1),
LoginName varchar(10)  not null unique,
LoginPwd varchar(20) default  (123456) not null,
LoginSex varchar(2) default('��')  check (LoginSex in('��','Ů') ),
LoginBrithday date,
LoginType varchar(10)
 )

 --2-----------------------------------------------------------------------------
 use master
go

create database Company
on
(
name = ' Company',
filename = 'D:\DATA\Company.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)

log on
(
name = ' Company_log',
filename = 'D:\DATA\Company_log.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)

go
use Company
create table SectionInfo
(
  SectionId int primary key identity(1,1),
  SectionName varchar(10) not null,
 )

 create table UserInfo
(
UserNo int primary key identity(1,1) not null,
UserName varchar(10) not null unique check (len(UserName)> 4),
UserSex varchar(2) default('��')  check (UserSex in('��','Ů') ) not null,
UserAge int not null check(UserAge >= 1 and UserAge <= 100),
UserAddress varchar(50) default('����'),
UserSection int references SectionInfo(SectionId)
 )

 create table WorkInfo
(
  WorkId int primary key identity(1,1) not null,
  UserId int  references  UserInfo(UserNo) not null,
  WorkTime datetime not null,
  WorkDescription varchar(40) not null check( WorkDescription in('�ٵ�','����','����','����','�¼�'))
 )

 --3----------------------------------------------------------------------------------------------------------

 use master
go

create database Students02
on
(
name = ' Students02',
filename = 'D:\DATA\Students02.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)

log on
(
name = 'Students02_log',
filename = 'D:\DATA\Students02_log.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)

create table ClassInfo01
(
 ClassId int primary key identity(1,1),
 ClassName varchar(10) not null unique,
 OpenTime date not null,
 ClassDescription  text
 )

 create table StudentsInfo
(
  StudentsId int primary key identity(1,1),
  StudentsName varchar(10) check (len(StudentsName)> 2) unique ,
  StudentsSex varchar(2) default('��')  check (StudentsSex in('��','Ů') ) not null,
  StudentsAge int check( StudentsAge >= 15 and  StudentsAge <= 40) not null ,
  HouseAddress varchar(50) default('����'),
  ClassId int references  ClassInfo01 (ClassId)
 )
 
 create table CourseInfo
 (
  CourseId int primary key identity(1,1),
  CourseName  varchar(10) not null unique,
  CourseDescription text
 )

 create table ScoreInfo
 (
  ScoreId  int primary key identity(1,1),
  StudentsId int references StudentsInfo(StudentsId),
   CourseId int references  CourseInfo( CourseId) ,
   Score int  check(Score >0 and Score  <= 100)
 )

--4-------------------------------------------------------------------------------------------------------

use master
go

if exists(select*from sys.objects where name ='TblHouse' )
	drop database TblHouse
create database TblHouse
on
(
name = ' TblHouse',
filename = 'D:\DATA\TbHlouse.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)

log on
(
name = 'TbHlouse_log',
filename = 'D:\DATA\TbHlouse_log.mdf',
size = 5MB,
maxsize = 50MB,
filegrowth = 10%
)

if exists(select*from sys.objects where name ='TblUserInfo' )
	drop table  TblUserInfo
create table TblUserInfo
(
UserId int primary key identity(1,1),
UserName varchar(10) not null unique,
UserTel varchar(20) unique not null
)

if exists(select*from sys.objects where name ='TblHouseTypeInfo' )
	drop table  TblHouseTypeInfo
create table TblHouseTypeInfo
(
TypeId int primary key identity(1,1),
TypeName varchar(10) not null  check(TypeName in('����','��ͨסլ','ƽ��','������'))
)

create table TblQxInfo
(
QxId int primary key identity(1,1),
QxName varchar(10) not null check(QxName in('���','����','����')),
)


create table TblHouseInfo
(
HouseId int primary key identity(1,1),
HouseDesc text,
UserId int references TblUserInfo(UserId ),
HouseZj money not null,
HousesShi int,
HouseTing int,
QxId  int references  TblQxInfo(QxId ),
TypeId int references  TblHouseTypeInfo(TypeId )
)
