
use master
go
create database TestDB--第一题
on
(
name='TestDB',
fileName='D:\test\TestDB.mdf',
size=5MB,
maxsize=50MB,
filegrowth=10MB
)
log on
(
name='TestDB_log',
fileName='D:\test\TestDB_log.ldf',
size=5MB,
maxsize=100MB,
filegrowth=20MB
)



create table typeInfo
(
typeld int primary key identity(1,1),
typeName varchar(10) not null
)
create table loginInfo 
(
Loginld  int primary key identity(1,1),
LoginName nvarchar(10) not null unique,
LoginPwd   nvarchar(20) not null default('123456'),
LoginSex nvarchar(3) not null,
LoginBirthday char not null,
Loginleixing nvarchar(2) not null,

)


create database company 
go
create table sectionInfo--第二题
(
sectionID int primary key identity,
sectionName nvarchar(10) not null
)


create table userInfo 
(
userNo int primary key identity(1,1) not null,
userName varchar(10) unique not null ,
userSex varchar(2) not null check(userSex='男'or userSex='女'),
userAge int not null check(userAge>1 and userAge<100),
userAdress varchar(50) default('湖北'),
userSection int foreign key references  sectionInfo(sectionID)
)



create table workInfo
(
workld int primary key identity(1,1) not null,
userld int foreign key references userInfo (userNo) not null,
WorkTime datetime not null,
workDescription varchar(40) not null check(workDescription='迟到'or workDescription='早退')
)



create database Studentlnfo--第三题
go
create table Classin 
(
classid int primary key identity(1,1),
className nvarchar(10) not null unique ,
classtime int not null,
classtext nvarchar(30) not null
)


create table Student 
(
studentid int primary key identity(1,1),
studentName nvarchar(10)  unique default('男') check(studentName='男' or studentName='女'),
studentAge  int not null check(studentAge>=15 and studentAge  <=40 ),
studentAdress nvarchar(30) default('湖北武汉'),
studentbianhao  int not null,
)



create table kecheng
(
kechengid int primary key identity(1,1),
kechengName nvarchar(10) unique  not null,
kechengtext nvarchar(30) not null
)



create table grade
(
gradeid int primary key identity(1,1),
grademath int check(grademath>0 and grademath<100)
)



create database roombuy--第四题
go
create table tblUser
(
userld int  ,
userName nvarchar(3),
usertel char(11),
)
insert into  tblUser values('123','张三','12312434231')


create table roomtype 
(
typeld nvarchar(20) not null,
typName nvarchar(3) not null,
)
insert into  roomtype values('别墅','皇家别墅'),('普通住宅','皇家酒店'),('平房','皇家旅馆'),('地下室','皇家密室')


create table TblQX
(
qxld varchar(10) not null,
qxName nvarchar(5) not null,
)
insert into  TblQX values('159','武昌'),('158','汉阳'),('157','汉口')



create table tblHouseInfo 
(
Houseid int not null,
Housedesc nvarchar(10)  not null ,
HouseZJ nvarchar  not null,
Houseshi int not null,
Houseting  int not null,
insert into tblHouseInfo values(666,'最顶尖的别墅','500000/天',10,10),(555,'最顶尖的普通住宅','50000/天',3,5),(333,'最顶尖的平房','10000/天',2,2),(333,'最顶尖的平房','10000/天',2,2)