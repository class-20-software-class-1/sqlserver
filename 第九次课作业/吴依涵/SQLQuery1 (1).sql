use master
go
create database bbs
on
(
 name='bbs',
 filename='D:\2.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='bbs_log',
 filename='D:\2_1og.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
go
use bbs
go
create table bbsUsers
(
 bbUID int identity,
 uName varchar(10) not null,
 uSex varchar(2) not null,
 uAge int not null,
 uPoint int not null
 )
go
use bbs
go
create table bbsSection
(
 bbsID int identity,
 sName varchar(10) not null,
 sUid int references bbsUsers(bbUID)
 )
go
use bbs
go
create table bbsTopic
(
 tID int primary key identity,
 tUID int references bbsUsers(bbUID),
 tSID int references bbsSection(bbsID),
 tTitle varchar(100) not null,
 tMsg text not null,
 tTime datetime,
 tCount int  
 )
go
use bbs
go
create table bbsReply
(
 rID int primary key identity,
 rUID int references bbsUsers(bbUID),
 rTID int references bbsTopic(tID),
 rMsg text not null,
 rTime datetime 
 )
 --添加约束
alter table bbsUsers add constraint PK_bbsUsers_bbUID primary key(bbUID)

alter table bbsUsers add constraint UK_bbsUsers_uName unique(uName) 

alter table bbsUsers add constraint CK_bbsUsers_uSex check(uSex='男'or uSex='女')

alter table bbsUsers add constraint CK_bbsUsers_uAge check(uAge>=15 and uAge<=60) 

alter table bbsUsers add constraint CK_bbsUsers_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_bbsID primary key(bbsID )

alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUsers(bbUID)

insert into bbsUsers(uName,uSex,uAge,uPoint) 
values('小雨点','女',20,0),
      ('逍遥','男',18,4),
      ('七年级生','男',19,2)
--二2.
select uName,uPoint into bbsPoint from bbsUsers

--二3.
insert into bbsSection(sName,sUid) 
values('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)

--二4.
--主贴表（bbsTopic）
--主贴编号  tID  int 主键primary key  标识列，identity
--发帖人编号  tUID  int 外键 renferences 表（列） 引用用户信息表的用户编号
--版块编号    tSID  int 外键 primary key 引用版块表的版块编号    （标明该贴子属于哪个版块）
--贴子的标题  tTitle  varchar(100) 不能为空
--帖子的内容  tMsg  text  不能为空
--发帖时间    tTime  datetime  
--回复数量    tCount  int
insert into bbsTopic(tUID,tSID,tTitle,tMsg,tTime,tCount) 
values(2,4,'范跑跑','谁是范跑跑','2008-7-8',1),
      (3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),
      (1,3,'今年夏天最流行什么',' 有谁知道今年夏天最流行什么呀？','2008-9-10',0)

--回帖：分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定

--回帖表（bbsReply）
--回贴编号  rID  int 主键primary key   标识列，identity
--回帖人编号  rUID  int 外键 renferences 引用用户信息表的用户编号
--对应主贴编号    rTID  int 外键 renferences 引用主贴表的主贴编号（标明该贴子属于哪个主贴）
--回帖的内容  rMsg  text  不能为空
--回帖时间    rTime  datetime 

insert into bbsReply(rUID,rTID,rMsg,rTime) 
values(1,3,'地震跑路人',2020-1-1),
(1,2,'区别很大',2020-2-1),
(2,2,'流行敲代码',2020-3-1)
update bbsUsers set uPoint=10 where uName='小雨点'

--1.在主贴表中统计每个版块的发帖总数
select tSID 板块编号,count(tID)发帖总数 from bbsTopic group by tSID 

--2.在回帖表中统计每个主贴的回帖总数量
select rTID 主贴编号,count(rID)回帖总数量 from bbsReply group by rTID 

--3.在主贴表中统计每个用户的发的主帖的总数
select tUID 用户,count(tID)主帖总数 from bbsTopic group by tUID

--4.在主贴表中统计每个用户发的主贴的回复数量总和
select tUID 用户,sum(tCount)回复数量总和 from bbsTopic group by tUID
 
--5.在主贴表中查询每个版块的 主贴的平均回复数量大于3的 版块的平均回复数量
select tSID 用户,avg(tCount)版块平均回复数量 from bbsTopic group by tSID having avg(tCount)>3

--6.在用户信息表中查询出积分最高的用户的用户名，性别，年龄和积分
select top 1 uName,uSex,uAge,uPoint from bbsUsers order by uPoint DESC
select uName,uSex,uAge,uPoint from bbsUsers where uPoint=(select max(uPoint)积分最高 from bbsUsers)

--7.在主贴表中（bbsTopic）中将帖子的内容或标题中有“快乐”两字的记录查询出来
select * from bbsTopic where tMsg like '%快乐%' or tTitle like '%快乐%' 

--8.在用户信息表（bbsUsers）中将用户年龄在15-20之间并且积分在10分以上的优秀用户查询出来（用多种方法实现）
select uName 优秀用户 from bbsUsers where uAge>=15 and uAge<=20 and uPoint>=10 
select uName 优秀用户 from bbsUsers where uAge between 15 and 20 and uPoint>=10

--9.在用户信息表（bbsUsers）中将用户名的第一个字为“小”，第三字为“大”的用户信息查询出来
select * from bbsUsers where uName like'小%' or uName like '__大%'

--10.在主贴表（bbsTopic）中将在 2008-9-10 12:00:00 以后发的并且回复数量在10以上的帖子的标题和内容查询出来，并且为列取上对应的中文列名
select tTitle 标题,tMsg 内容 from bbsTopic where tTime>'2008-9-10 12:00:00' and tCount>=10

--11.在主贴表（bbsTopic）中将帖子的标题是以‘！’结尾的帖子的发帖人编号和回复数量查询出来
select tUID 发帖人编号,tCount 回复数量 from bbsTopic where tTitle like '%!'


select * from bbsReply  