use master
	go

	create database bbs
	on
	(
		name='bbs',
		filename='D:\ADTA\bbs.mdf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	log on
	(
		name='bbs_log',
		filename='D:\ADTA\bbs_log.ldf',
		size=5MB,
		filegrowth=10MB,
		maxsize=100MB	
	)
	go

	use bbs
	go

	create table bbsUser
	(
		UID int identity,
		uName varchar(10) not null,
		uSex  varchar(2) not null,
		uAge  int not null,
		uPoint  int not null
	)

	alter table bbsUser add constraint PK_bbsUser_UID primary key(UID)
	alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
	alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
	
	alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)

	alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)


	create table bbsTopic
	(
		tID  int primary key identity,
		tUID  int references bbsUser(UID),
		tSID  int references bbsSection(sID),
		tTitle  varchar(100) not null,
		tMsg  varchar(200) not null,
		tTime  datetime,
		tCount  int
	)

	create table bbsReply
	(
		rID  int primary key identity,
		rUID  int references bbsUser(UID),
		rTID  int references bbsTopic(tID),
		rMsg  text not null,
		rTime  datetime 
	)
	
	create table bbsSection
	(
		sID  int identity,
		sName  varchar(10) not null,
		sUid   int 
	)
	alter table bbsSection add constraint PK_bbsSection_sID primary key(sID) 
	alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(UID)

	insert into bbsUser values ('小雨点','女',20,0),('逍遥','男',18,4),('七年级生','男',19,2)
	insert into bbsSection values ('技术交流',1),('读书世界',3),('生活百科',1),('八卦区',3)
	insert into bbsTopic values (2,4,'范跑跑','谁是范跑跑','2008-7-8',1)
	insert into bbsTopic values (3,1,'.NET','与JAVA的区别是什么呀？','2008-9-1',2),(1,3,'今年夏天最流行什么','有谁知道今年夏天最流行','2008-9-10',0)
	insert into bbsReply( rTID, rMsg, rTime,rUID) values (2,'与JAVA的区别...','2008-7-8',2),(3,'今年夏天最流行什么....','2008-8-8',1),(1,'范跑跑..shi..','2008-9-8',3)
	select tSID 版块编号  , count(tUID)发帖总数 from bbsTopic group by tSID 
	select  rTID 对应主贴编号, count(  rID )回帖总数量  from bbsReply group by  rTID
	select tUID 发帖人编号 , count(tID)主帖总数  from bbsTopic group by tUID 
	select tUID 发帖人编号, sum(tCount)回复数量总和  from bbsTopic group by tUID
	select  tSID,avg(tCount)主贴平均回复数量  from bbsTopic group by tSID having avg(tCount)>3
	select  top 1 *from bbsUser order by uPoint  desc
    select UName,USex,Upoint from BbsUser where UPoint=(select MAX(UPoint)最高积分 from BbsUser)
	select*from bbsTopic where tTitle like'快乐'or tMsg like'快乐'
	select uName from bbsUser where uPoint>10 and (uAge>=15 and uAge<=20)
	select uName , uPoint, uAge from bbsUser where  uPoint>10 and (uAge>=15 and uAge<=20) 
	select * from bbsUser where uName='小_大%' 
	select tTitle, tMsg   from bbsTopic where tTime >'2008-9-10 12:00:00 'and tCount>10
	select  tUID,tCount from bbsTopic  where tTitle='%!'