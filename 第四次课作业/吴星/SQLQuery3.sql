create database Student1
on
(
	Name='Student1',
	Filename='D:\SQL\新建文件夹\MSSQL12.MSSQLSERVER\MSSQL\DATA/\Student1.mdf',
	Size=5MB,
	Maxsize=100MB,
	Filegrowth=10%
)
log on
(
	Name= 'Student1_log',
	Filename='D:\SQL\新建文件夹\MSSQL12.MSSQLSERVER\MSSQL\DATA/\Student1_log.ldf',
	Size=5MB,
	Filegrowth=1MB
)
use Student1
create table Class
(
	ClassId int primary key identity(1,1),
	ClassName nvarchar(20) unique not null,
)
	insert into Class(ClassName)
		 Select '一班' union
		 Select '二班'union 
		 Select '三班'union 
		 Select '四班'union
		Select '五班'union 
		Select '六班'union 
		Select '七班'union 
		Select '八班'union
		Select '九班'union 
		Select '十班' 
	
	drop table Class
	select * from Class

	update Class set ClassName='十一班' where ClassId='1'
	delete from Class where ClassId='10'

	drop table Student
	create table Student
	(
		StuId int primary key identity(1,1) ,
		ClassId int foreign key (ClassId) references Class(ClassId) ,
		StuName nvarchar(20) not null,
		StuSex nvarchar(2) check(StuSex in('男','女')),
		StuBirthday date ,
		StuPhone nvarchar(11) unique,
		StuAddress nvarchar(200),
		
	)
	select * from Student

	insert into Student(ClassId,StuName,StuSex,StuBirthday,StuPhone,StuAddress)
	Select 1,'张三','男','2000-12-5',21563214568,'中国' union
	Select 2,'李四','男','2000-12-5',21563214658,'中国' union
	Select 3,'张三','男','2000-12-5',21563114568,'中国' union
	Select 4,'张三','男','2000-12-5',21566564568,'中国' union
	Select 5,'张三','男','2000-12-5',21544214568,'中国' union
	Select 6,'张三','男','2000-12-5',21545895568,'中国' union
	Select 7,'张三','男','2000-12-5',21563845468,'中国' union
	Select 8,'张三','男','2000-12-5',21566564368,'中国' union
	Select 9,'张三','男','2000-12-5',21565214568,'中国' union
	Select 1,'张三','女','2000-12-5',02156326568,'中国' union
	Select 1,'张三','男','2000-12-5',58085214568,'中国' union
	Select 2,'张三','男','2000-12-5',21023214568,'中国' union
	Select 3,'张三','男','2000-12-5',20563214568,'中国' union
	Select 4,'张三','男','2000-12-5',21123214568,'中国' union
	Select 5,'张三','男','2000-12-5',21523145638,'中国' union
	Select 6,'张三','男','2000-12-5',21563212348,'中国' union
	Select 7,'张三','男','2000-12-5',21534114568,'中国' union
	Select 8,'张三','男','2000-12-5',13433214568,'中国' union
	Select 9,'张三','男','2000-12-5',21321434568,'中国' union
	Select 8,'张三','男','2000-12-5',21512342368,'中国' 

	alter table Student add CreateDate datetime default getdate()
		update Student set CreateDate=getdate()
	delete from Student where ClassId='8' and StuPhone='13433214568'
	
	drop table Course
	create table Course
	(
		CourseId int primary key identity(1,1),
		CourseName nvarchar(50) unique not null,
		CourseCredit int default('1') check(CourseCredit>'0' and CourseCredit<'6'),
		CourseCreditt nvarchar(10) check(CourseCreditt in ('专业课','公共课'))
	)

	insert into Course(CourseName,CourseCreditt)
	Select '如何让富婆爱上我','专业课' union
	Select '富婆通讯录','专业课' union
	Select '渣男语录','专业课' union
	Select '如何吃软饭','专业课' union
	Select '霸道总裁爱上我','专业课' union
	Select '啥也不是','专业课' 

	select * from Course

	update Course set CourseCredit=5 where CourseId=4

	create table Score
	(
		ScoreId int primary key identity(1,1),
		StuId int foreign key(StuId) references Student(StuId),
		CourseId int foreign key(CourseId) references Course(CourseId),
		Score decimal(5,2) unique not null,
	)
	select * from Score
	drop table Score
	insert into Score(StuId,CourseId,Score)
	Select '1','3','6' union
	select '2','4','6.8' union
	Select '1','3','56' union
	select '2','4','56.8' union
	Select '1','3','77' union
	select '2','4','86.8' union
	Select '1','3','96' union
	select '2','4','16.8' union
	Select '1','3','62' union
	select '2','4','66.8' union
	Select '1','3','36' union
	select '2','4','62.8' union
	Select '1','3','61' union
	select '2','4','64.8' union
	Select '1','3','64' union
	select '2','4','65.8' union
	Select '1','3','69' union
	select '2','4','69.8' union
	Select '1','3','68' union
	select '2','4','67.8' 

	update Course set CourseCredit='4' where CourseName='渣男语录'

	delete from Score where StuId='1'
	delete from Score where CourseId='1'
	
	alter table Score add constraint DF_SCORE  Default('0') (Scorez)
	alter table Score add constraint CK_score check(Score>-1 and Score<101)