create database bbs
on
(
	name='bbs',
	filename='D:\bbs.mdf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
log on
(
	name='bbs_log',
	filename='D:\bbs_log.ldf',
	size=10MB,
	maxsize=50MB,
	filegrowth=10%
)
go

create table bbsUser
(
	uID int identity(1,1) not null,
	uName varchar(10)  not null,
	uSex  varchar(2) not null,
	uAge  int not null,
	uPoint  int not null
)

create table bbsSection
(
	sID  int identity(1,1) not null , 
	sName  varchar(10) not null,
	sUid   int

)
--添加约束
-- alter table 表名 add constraint 约束名  约束类型 
alter table bbsUser add constraint PK_bbsUser_uID primary key(uID)
alter table bbsUser add constraint UK_bbsUser_uName unique(uName)
alter table bbsUser add constraint CK_bbsUser_uSex check(uSex='男' or uSex='女')
alter table bbsUser add constraint CK_bbsUser_uAge check(uAge>=15 and uAge<=60)
alter table bbsUser add constraint CK_bbsUser_uPoint check(uPoint>=0)

alter table bbsSection add constraint PK_bbsSection_sID primary key(sID)
alter table bbsSection add constraint FK_bbsSection_sUid foreign key(sUid) references bbsUser(uID)

	--主贴表（bbsTopic）
   create table bbsTopic
   (
	--主贴编号  tID  int 主键  标识列，
	tID int primary key identity(1,1),
	--发帖人编号  tUID  int 外键  引用用户信息表的用户编号
	tUID int references bbsUser(uID),
	--版块编号    tSID  int 外键  引用版块表的版块编号    （标明该贴子属于哪个版块）
	tSID int references bbsSection(sID),
	--贴子的标题  tTitle  varchar(100) 不能为空
	tTitle nvarchar(100)not null,

	--帖子的内容  tMsg  text  不能为空
	tMsg text not null,
	--发帖时间    tTime  datetime  
	tTime datetime,
	--回复数量    tCount  int
	tCount int
	)
	--回帖表（bbsReply）
    create table bbsReply
	(
	--回贴编号  rID  int 主键  标识列，
	rID int primary key identity(1,1),
	--回帖人编号  rUID  int 外键  引用用户信息表的用户编号
	rUID int references bbsUser(uID) ,
	--对应主贴编号    rTID  int 外键  引用主贴表的主贴编号    （标明该贴子属于哪个主贴）
	rTID int references bbsTopic(tID),
	--回帖的内容  rMsg  text  不能为空
	rMsg text not null,
	--回帖时间    rTime  datetime 
	rTime datetime
	)

	--现在有3个会员注册成功，请用一次插入多行数据的方法向bbsUsers表种插入3行记录，
insert  bbsUser(uName,uSex,uAge,uPoint)
	select '小雨点' , '女',  20 , 0 union
	select '逍遥 ' , '男' , 18 , 4 union
	select '七年级生'  ,'男' , 19,  2 


	
    --将bbsUsers表中的用户名和积分两列备份到新表bbsPoint表中，提示查询部分列:select 列名1，列名2 from 表名
	select uName,uPoint into  bbsPoint from bbsUser

	--给论坛开设4个板块
	 --名称        版主名
	  --技术交流    小雨点
	  --读书世界    七年级生
	  --生活百科     小雨点
	  --八卦区       七年级生
	  --insert into bbsSection (sName,sUid) values ('技术交流',1),
--('读书世界',3),('生活百科',1),('八卦区',3)
    insert bbsSection(sName,sUid)
	select '技术交流', 1 union
    select '读书世界', 3 union
	select '生活百科', 1 union
	select '八卦区', 3

	--向主贴和回帖表中添加几条记录
	   --主贴：
	  --发帖人    板块名    帖子标题                帖子内容                发帖时间   回复数量
	  --逍遥      八卦区     范跑跑                 谁是范跑跑              2008-7-8   1
	  --七年级生  技术交流   .NET                   与JAVA的区别是什么呀？  2008-9-1   2
	  --小雨点   生活百科    今年夏天最流行什么     有谁知道今年夏天最流行什么呀？  2008-9-10  0
		insert  bbsTopic (tUID,tSID,tTitle,tMsg,tTime,tCount)
		select 2 , 4,'范跑跑','谁是范跑跑','2008-7-8',1 union
		select 3 , 1,'.NET ','与JAVA的区别是什么呀？',' 2008-9-1 ',2 union
		select 1 , 3,'.NET ' , ' 有谁知道今年夏天最流行什么呀？','2008-9-10',0 
	   
	   --回帖：
	   --分别给上面三个主贴添加对应的回帖，回帖的内容，时间，回帖人自定
	   insert into bbsReply (rUID,rMsg,rTime)
	   select 1,'高奕','2021-3-15' union
	   select 2,'the name deferent','2021-3-15' union
	   select 3,'sex','2021-3-15'

	--因为会员“逍遥”发表了非法帖子，现将其从论坛中除掉，即删除该用户，请用语句实现（注意主外键，要删除主键，先要将引用了该主键的外键数据行删除）
	--删除约束							  约束名称
-- alter table  表明 drop constraint  约束名称 
alter table bbsTopic drop constraint FK__bbsTopic__tUID__1920BF5C
alter table bbsReply drop constraint FK__bbsReply__rUID__1CF15040


	--因为小雨点发帖较多，将其积分增加10分
	--更新数据
--update 表名 set 列名和更新后的数据 where 列和该列的的标识
update bbsUser set uPoint=14 where uID=1


	--因为板块“生活百科”灌水的人太少，现决定取消该板块，即删除（注意主外键）
--删除某一行数据
alter table bbsTopic drop constraint FK__bbsTopic__tSID__1A14E395
	--因回帖积累太多，现需要将所有的回帖删除
	--清空表数据
	truncate table bbsReply