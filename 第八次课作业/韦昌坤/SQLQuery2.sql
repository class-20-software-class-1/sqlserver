create database orders
on
(
	name = 'order',
	size = 5,
	maxsize = 500,
	filename = 'D:\新建文件夹.mdf',
	filegrowth = 10%
)
log on
(
	name = 'order_LOG',
	size = 5,
	maxsize = 500,
	filename = 'D:\新建文件夹.ldf',
	filegrowth = 10%
)
use orders
go
create table orders			--订单表
(
	orderId int primary key,  --订单编号
	orderDate smalldatetime		--订购日期
)
go
create table orderItem		--订购项目表
(
	itemName nvarchar(10) not null,		--产品名称
	theNumber  int not null,		--订购数量
	theMoney money not null,		--订购单价
	itemType nvarchar(4)  not null,--产品类别
	orderId	int references	orders(orderId)	not null,		--订单编号
	ItemiD int identity(1,1) primary key,		--项目编号
)
insert into orders(orderId,orderDate) values(1,'2008-01-12'),(2,'2008-02-10'),(3,'2008-02-15'),(4,'2008-03-10')
insert into orderItem(orderId,itemType,itemName,theNumber,theMoney)
select 1,'文具','笔',72,2 union
select 1,'文具','尺',10,1 union
select 1, '体育用品','篮球',1,56 union
select 2,'文具','笔',36,2 union
select 2,'文具','固体胶',20,3 union
select 2,'日常用品','透明胶',2,1 union
select 2,'体育用品','羽毛球',20,3 union
select 3,'文具','订书机',20,3 union
select 3,'文具','订书机',10,3 union
select 3,'文具','裁缝刀',5,5 union
select 4,'文具','笔',20,2 union
select 4,'文具','信纸',50,1 union
select 4,'日常用品','毛巾',4,5 union
select 4,'日常用品','透明胶',30,1 union
select 4,'体育用品','羽毛球',20,3 
--查询所有订单订购的所有物品数量总和
select sum(theNumber) as 所有物品数量 from orderItem 
select theNumber,avg(theMoney) as 平均值 from orderItem where orderId<3 group by theNumber having avg(theMoney)<10
select theNumber as 所有物品数量,avg(theMoney) as 平均单价 from orderItem where theNumber>50 group by theNumber having avg(theMoney)<10
--查询每种类别的产品分别订购了几次
select itemType,sum(theNumber) as 订购数量 from orderItem group by itemType
--查询每种类别的产品的订购总数量在100以上的订购总数量和平均单价
select itemType,sum(theNumber) as 订购数量 from orderItem group by itemType having sum(theNumber)>100
--查询每种产品的订购次数，订购总数量和订购的平均单价
select itemName as 产品名称,sum(theNumber) as 订购总数量,avg(theMoney)as 平均单价, count(itemName) as 订购数量 from orderItem group by itemName