create database Students
	
	--班级信息表
	use Students
	create table ClassInfo
	(
	ClassId int primary key identity(1,1),
	ClassName nvarchar(20) unique not null
	)

	select *from  ClassInfo
	insert into ClassInfo (ClassName) values ('软1'),('软2'),('软3'),('软4'),('软5'),('软6'),('软7'),('软8'),('软9'),('软10')
	update ClassInfo set ClassName = '软1' where ClassId = 1
	delete from ClassInfo where ClassId = 10

	--学生信息表
	if exists(select * from sys.objects where name = 'StudentInfo')
	drop table StudentInfo
	create table StudentInfo
	(
	StuId int primary key identity(1,1),
	ClassId int references  ClassInfo(ClassId) ,
	StuName nvarchar(20) not null,
	StuSex nvarchar(1) default('男') check (StuSex = '男' or StuSex = '女'),
	StuBirthday smalldatetime,
	StuPhone nvarchar(11) unique,
	StuAddress nvarchar(200)
	)

	--insert into StudentInfo ( ClassId, StuName ,StuSex ,StuBirthday ,StuPhone ,StuAddress )

	select * from StudentInfo 
	insert into StudentInfo ( ClassId, StuName ,StuSex ,StuBirthday ,StuPhone ,StuAddress ) values
	(1,'李','男','1992-7-4','1346897423','中国'),(2,'张三','男','1992-3-4','1343894323','中国'),
	(3,'吴','男','1992-2-4','1346894343','中国'),(4,'李三','男','1994-3-23','1346694323','中国'),
	(5,'张','男','1992-1-4','1346894353','中国'),(6,'老李','男','1992-3-9','1346794323','中国'),
	(7,'红','女','1992-5-4','1346894363','中国'),(8,'老吴','男','1998-12-4','1346894323','中国'),
	(9,'王','男','1992-6-4','1346894373','中国'),(10,'李四','男','1992-3-4','1344894323','中国'),
	(11,'老王','男','1992-7-4','1346894383','中国'),(12,'张四','男','1992-11-4','1347894323','中国'),
	(13,'老张','男','1992-8-4','1346894393','中国'),(14,'王老五','男','1997-3-4','1346874323','中国'),
	(15,'李子','男','1992-9-4','1346894328','中国'),(16,'刘','男','1999-3-4','1346864323','中国'),
	(17,'陈','女','1992-3-5','1346894327','中国'),(18,'郑','男','1995-8-4','1346854323','中国'),
	(19,'花','女','1992-3-3','1346894326','中国'),(20,'白','男','1996-3-4','1346494323','中国')
	
	alter table  StudentInfo add CreateDate  datetime default(getdate())
	update StudentInfo set CreateDate = getdate()
	delete from StudentInfo where ClassId= 1

	--课程信息表
	if exists(select * from sys.objects where name = 'CourseInfo')
	drop table CourseInfo
	create table CourseInfo
	(
	CourseId int primary key identity(1,1),
	CourseName nvarchar(50) unique not null,
	CourseCridit int default(1) check(CourseCridit >=1  and CourseCridit <=5) not null,
	CourseType nvarchar(10) check(CourseType = '专业课' or CourseType = '公共课')
	)

	select * from CourseInfo
	insert into CourseInfo ( CourseName,CourseType) values ('java','专业课'),('html','专业课'),('语','公共课'),
	('C++','专业课'),('数','公共课'),('英','公共课')
	select * from CourseInfo
	update CourseInfo set CourseCridit = 4 where CourseName = '英'

	--成绩信息表
	if exists(select * from sys.objects where name = 'ScoreInfo')
	drop table ScoreInfo
	create table ScoreInfo
	(
	ScoreId int primary key identity(1,1),
	StuId int references  StudentInfo(StuID) ,
	CourseId int references CourseInfo(CourseID) ,
	Score decimal(5,2) unique not null
	)

	select * from  ScoreInfo 
	insert into ScoreInfo  ( StuId,CourseId,Score)values (5,1,77),(6,2,57),(7,3,67),(8,4,88),(9,5,78),(10,6,76),(11,7,79),
	(12,9,87),(13,10,87),(14,11,80),(15,12,89),(16,13,87),(17,14,67),(18,15,87),(19,16,67),(20,27,97),(1,18,88),(2,19,89),(3,20,87),(4,1,87)
	update CourseInfo set CourseCridit = 3 where CourseName = 'java'
	delete from StudentInfo where StuId= 1
	delete from ScoreInfo where ScoreId = 1
	alter table ScoreInfo add constraint CK_ScoreInfo_Score  check (Score>0 and Score<100)
	alter table ScoreInfo add constraint DK_ScoreInfo_Score default('0') for Score
