use master
go
create database Students
on
(
 name='Students',
 filename='D:\Program Files\Stundents.mdf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)
log on
(
 name='Students_log',
 filename='D:\Program Files\Stundents_log.ldf',
 size=6MB,
 maxsize=100MB,
 filegrowth=10Mb
)


use Students
go

create table Class
(
	ClassID int primary key identity,
	ClassName nvarchar(20) unique not null,
)

create table Student
(
	StuID int primary key identity(1,1),
	ClassID int references Class(ClassID),
	StuName nvarchar(20) not null,
	StuSex nchar(1) default('��') check(StuSex in('��','Ů')), 
	StuBirthday date,
	StuPhone nvarchar(11) unique
)

create table Course
(
	CourseID int primary key identity,
	CourseName nvarchar(50) unique not null,
	CourseCredit int default(1) check(CourseCredit<=5 and CourseCredit>=1)
)

create table Score
(
	ScoreID int identity,
	StuID int,
	CourseID  int,
	Score decimal(5,2) unique not null
)

use Students
go

alter table Student add StuAddress nvarchar(200) not null
alter table Score add constraint PK_Score_ScoreID primary key(ScoreID)
alter table Score add constraint FK_Score_StuID foreign key(StuID) references Student(StuID)
alter table Score add constraint FK_Score_CourseID foreign key(CourseID) references Course(CourseID)