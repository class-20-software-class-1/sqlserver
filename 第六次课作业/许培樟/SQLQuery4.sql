create database house
on
(
 name='house',
 filename='D:\house.mdf',
 size=10,
 maxsize=100,
 filegrowth=10%
)
log on
(
 name='house_log',
 filename='D:\house_log.ldf',
 size=10,
 maxsize=100,
 filegrowth=10%
)

use house
go

create table tblUser
(
userId int primary key identity(1,1),
userName varchar
)
