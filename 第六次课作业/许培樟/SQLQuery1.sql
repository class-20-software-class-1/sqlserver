use master
go

create database TestDB
on
(
 name='TestDB',
 filename='D:\TestDB.mdf',
 size=10,
 maxsize=100,
 filegrowth=10
)
log on
(
 name='TestDB_log',
 filename='D:\TestDB_log.ldf',
 size=10,
 maxsize=100,
 filegrowth=10
)

use TestDB
go

create table typeInfo
(
 TyprId int primary key identity(1,1),
 typeName varchar (10) not null,
)
create table loginInfo
(
 LoginId int primary key identity(1,1),
 LoginName nvarchar (10) not null unique,
 LoginPwd nvarchar (20) not null, default(123456),
 Loginsex varchar(10),
 Loginday date,
 LoginVIPtype varchar,
)

