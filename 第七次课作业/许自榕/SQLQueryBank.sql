create database bbs
on
(
	name='bbs',
	filename='D:\bank\bbs.mdf',
	size=20,
	maxsize=200,
	filegrowth=15%
)

log on
(
	name='bbs_log',
	filename='D:\bank\bbs.ldf',
	size=20,
	maxsize=200,
	filegrowth=15%
)
go
use bbs
go

create table userInfo 
(
	customerID int identity primary key,
	customerName nvarchar(20) not null,
	PID varchar(20) check(len(PID)=18 or len(PID)=15) unique,
	telephone varchar(20) check(len(telephone)=13 or telephone like '____-________'),
	address nvarchar(30)
)

create table cardInfo
(
	cardID varchar(20) primary key check(cardID like '1010 3576 ____ ___') not null,
	curType varchar(10) default('RMB') not null,
	savingTypev nvarchar(10) check (savingTypev in('活期','定活两便','定期')),
	openDate datetime default(getdate()) not null, 
	balance money check(balance>=1) not null,
	pass varchar(6) check(len(pass)=6) default(888888) not null,
	IsReportLoss nvarchar(5) check(IsReportLoss in ('是','否')) default('否') not null,
	customerID int references userInfo(customerID)	not null
)
create table transInfo 
(
	transId int identity primary key,
	transDate datetime default(getdate())  not null,
	cardID varchar(20) references cardInfo(cardID),
	transType nvarchar(2) check(transType in('存入','支出')),
	transMoney money check(transMoney>0) not null,
	remark text 
)